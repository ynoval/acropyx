(function(){
  'use strict';
  var socketMockModule = angular.module('socketMock', []);
  function SockectMock(){
    return {
      socket: {
        connect: function() {},
        on: function() {},
        emit: function() {},
        receive: function() {}
      },

      syncUpdates: function() {},
      unsyncUpdates: function() {}
    };
  }
  socketMockModule.factory('socket', [SockectMock]);
})();
