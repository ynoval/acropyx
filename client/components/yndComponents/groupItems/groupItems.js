(function() {
  'use strict';
  var yndGroupItemModule = angular.module('yndComponents.groupItems', ['ui.bootstrap']);
  function GroupItems() {
    return {
      restrict: 'E',
      scope: {
        'items': '=',
        'selectedItems': '=',
        'name': '@',
        'id': '@'
      },
      controller: ['$scope', function ($scope) {
        $scope.getName = function (item) {
          return item[$scope.name];
        };
        $scope.getId = function (item) {
          return item[$scope.id];
        };
        $scope.removeItem = function (item) {
          $scope.items.push(item);
          _.remove($scope.selectedItems, item);
        };
        $scope.addItem = function (item) {
          if (item && item._id) {
            $scope.selectedItems.push(item);
            _.remove($scope.items, item);
            $scope.selectedItem = undefined;
          }
        };
      }],
      templateUrl: 'components/yndComponents/groupItems/groupItems.tpl.html'
    };
  }
  yndGroupItemModule.directive('ngGroupItems', [GroupItems]);
})();
