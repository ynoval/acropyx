(function(){
  'use strict';
  var yndModule = angular.module('yndComponents.radioButtonGroup', ['ui.bootstrap'] );
  function RadioButtonGroup(){
    return {
      restrict: 'E',
      scope: { model: '=', options: '=', id: '=', name: '=', suffix: '=' },
      controller: ['$scope', function($scope) {
        $scope.activate = function (option, $event) {
          $scope.model = option[$scope.id];
          // stop the click event to avoid that Bootstrap toggles the "active" class
          if ($event.stopPropagation) {
            $event.stopPropagation();
          }
          if ($event.preventDefault) {
            $event.preventDefault();
          }
          $event.cancelBubble = true;
          $event.returnValue = false;
        };

        $scope.isActive = function(option) {
          return option[$scope.id] === $scope.model;
        };

        $scope.getName = function (option) {
          return option[$scope.name];
        };
      }],
      template: '<button type="button" class="btn btn-{{suffix}}"' +
      'ng-class="{active: isActive(option)}"' +
      'ng-repeat="option in options" ' +
      'ng-click="activate(option, $event)">{{getName(option)}} ' +
      '</button>'
    };
  }
  yndModule.directive('ngRadioButtonGroup', [ RadioButtonGroup]); 
})();
