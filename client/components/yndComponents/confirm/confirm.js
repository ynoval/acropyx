(function(){
  'use strict';
  var yndConfirmAction = angular.module( 'yndComponents.confirmAction', ['ui.bootstrap']);
  function ConfirmAction($uibModal){
    var ModalInstanceCtrl = function($scope, $uibModalInstance, message) {
      $scope.message = message;
      $scope.ok = function() {
        $uibModalInstance.close();
      };
      $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel');
      };
    };
    return {
      restrict: 'A',
      scope:{
        ngReallyDo:'&'
      },
      link: function(scope, element, attrs) {
        element.bind('click', function() {
          var modalInstance = $uibModal.open({
            templateUrl: 'components/yndComponents/confirm/confirm.tpl.html',
            controller: ['$scope', '$uibModalInstance', 'message', ModalInstanceCtrl],
            windowClass: 'confirm-modal-window',
            resolve:{/* @ngInject */
              message: function(){
                return attrs.ngReallyMessage || 'Are you sure ?';
              }
            }
          });

          modalInstance.result.then(function() {
            scope.ngReallyDo();
          }, function() {
            //Modal dismissed
          });
        });
      }
    };
  }
  yndConfirmAction.directive('ngReallyDo', ['$uibModal', ConfirmAction]);
})();
