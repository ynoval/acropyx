'use strict';

angular.module('acropyxApp')
  .factory('UtilsService', [function () {
    var UtilsService = {
      //Return array with elements from targetArray no included in elementArray
      difference: function(targetArray, elementArray){
        var result = [];
        _.each(targetArray, function(target){
          var i = 0;
          while (i < elementArray.length && elementArray[i]._id !== target._id ){
            i++;
          }
          if (i === elementArray.length){
            result.push(target);
          }
        });
        return result;
      }

    };
    return UtilsService;
  }]);
