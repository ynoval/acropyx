(function(){
  'use strict';
  var acropyxApp = angular.module('acropyxApp');
  function User ($resource) {
    return $resource('/api/users/:id/:controller', {
        id: '@_id'
      },
      {
        changePassword: {
          method: 'PUT',
          params: {
            controller:'password'
          }
        },
        get: {
          method: 'GET',
          params: {
            id:'me'
          }
        }
      }
    );
  }
  acropyxApp.factory('User', ['$resource', User]);
})();


