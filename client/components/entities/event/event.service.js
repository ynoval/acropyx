(function(){
  'use strict';
  var acropyxApp = angular.module('acropyxApp');
  acropyxApp.constant('eventUrls',{baseUrl:'/api/events/'});
  function EventService($http, $uibModal, $q, eventUrls){
    return {
      query: function(){
        return $http.get(eventUrls.baseUrl).then(function(result){
          return result.data;
        });
      },
      queryPublicEvents: function(){
        return $http.get(eventUrls.baseUrl + 'publicEvents').then(function(result){
          return result.data;
        });
      },
      queryActiveEvents: function(){
        return $http.get(eventUrls.baseUrl + 'activeEvents').then(function(result){
          return result.data;
        });
      },
      queryCompetitions: function(eventId){
        return $http.get(eventUrls.baseUrl + eventId + '/competitions').then(function(result){
          return result.data;
        });
      },
      queryActiveCompetitions:function(eventId){
        return $http.get(eventUrls.baseUrl + eventId + '/activeCompetitions').then(function(result){
          return result.data;
        });
      },
      get: function(eventId){
        return $http.get(eventUrls.baseUrl + eventId).then(function(result){
          return result.data;
        });
      },
      delete: function(eventId){
        return $http['delete'](eventUrls.baseUrl + eventId);
      },
      reset: function(eventId){
        return $http.put(eventUrls.baseUrl + eventId + '/reset', {eventId: eventId})
          .then(function(result){
            return result.data;
          });
      },
      reopen: function(eventId){
        return $http.put(eventUrls.baseUrl + eventId + '/reopen', {eventId: eventId})
          .then(function(result){
            return result.data;
          });
      },
      create: function(season){
        var deferred = $q.defer();
        var AddEventCtrl = function ($scope, $uibModalInstance, season, seasons) {
          $scope.selectedSeason = season;
          $scope.newEvent = {};
          if (season){
            $scope.newEvent.season = season._id;
          }
          $scope.seasons = seasons;

          $scope.addEvent = function(){
            $uibModalInstance.close({newEvent: $scope.newEvent, action: 'Save'});
          };
          $scope.cancel = function(){
            $uibModalInstance.close({newEvent: null, action: 'Cancel'});
          };
        };
        var modalInstance = $uibModal.open({
          templateUrl: 'components/entities/event/modalAddEvent/modalAddEvent.html',
          controller: [
            '$scope', 
            '$uibModalInstance', 
            'season', 
            'seasons', 
            AddEventCtrl
          ],
          keyboard: false,
          backdrop: 'static',
          resolve:{/* @ngInject */
            seasons: function(SeasonService){
              return SeasonService.query();
            }, /* @ngInject */
            season: function(){
              return season;
            }
          }
        });
        modalInstance.result.then(function (data) {
          switch (data.action) {
            case 'Save':
            {
              console.log('adding new event:  ' + data.newEvent.name);
              $http.post(eventUrls.baseUrl, data.newEvent).then(function(result){
                deferred.resolve(result.data);
              }, function(result){
                deferred.reject(result.data);
              });
              break;
            }
            case 'Cancel':
            {
              deferred.reject('Action canceled');
              break;
            }
          }
        }, function (err) {
          deferred.reject('Error:' + err);
        });
        return deferred.promise;
      },
      save: function(event){
        return $http.put(eventUrls.baseUrl + event._id, event);
      },
      startEvent: function(eventId){
        return $http.put(eventUrls.baseUrl + eventId + '/start', {eventId: eventId});
      },
      endEvent: function(eventId){
        return $http.put(eventUrls.baseUrl + eventId + '/end', {eventId: eventId});
      },
      displayResults: function(eventId){
        return $http.put(eventUrls.baseUrl + eventId + '/displayResults/', {eventId: eventId}).then(function(result){
          return result.data;
        });
      },
      displayPaf: function(eventId, text){
        return $http.put(eventUrls.baseUrl + eventId + '/displayPaf/', {eventId: eventId, pafText: text}).then(function(result){
          return result.data;
        });
      },
      clearPaf: function(eventId){
        return $http.put(eventUrls.baseUrl + eventId + '/clearPaf/', {eventId: eventId}).then(function(result){
          return result.data;
        });
      },
      displayMessage: function(eventId, text){
        return $http.put(eventUrls.baseUrl + eventId + '/displayMessage/', {eventId: eventId, text: text}).then(function(result){
          return result.data;
        });
      },
      clearMessage: function(eventId){
        return $http.put(eventUrls.baseUrl + eventId + '/clearMessage/', {eventId: eventId}).then(function(result){
          return result.data;
        });
      }
    };
  }
  acropyxApp.factory('EventService',['$http', '$uibModal', '$q', 'eventUrls', EventService]);
})();
