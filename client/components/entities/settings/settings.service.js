(function(){
  'use strict';
  var acropyxApp = angular.module('acropyxApp');
  acropyxApp.constant('settingsUrls',{baseUrl:'/api/settings/'});
  function SettingService($http, settingsUrls){
    return {
      getDefaults: function(){
        return $http.get(settingsUrls.baseUrl + 'defaults/' ).then(function(result){
          return result.data;
        });
      },
      get: function(settingId){
        return $http.get(settingsUrls.baseUrl+ settingId).then(function(result){
          return result.data;
        });
      },
      save: function(settings){
        return $http.put(settingsUrls.baseUrl + settings._id, settings);
      }
    };
  }
  acropyxApp.factory('SettingService',['$http', 'settingsUrls', SettingService]);
})();
