(function(){
  'use strict';
  var acropyxApp = angular.module('acropyxApp');
  acropyxApp.constant('manoeuvreUrls',{baseUrl:'/api/manoeuvres/', groupsUrl:'/api/manoeuvresGroup'});
  function ManoeuvreService($http, $uibModal, $q, manoeuvreUrls){
    return {
      query: function(){
        return $http.get(manoeuvreUrls.baseUrl).then(function(result){
          return result.data;
        });
      },
      get: function(manoeuvreId){
        return $http.get(manoeuvreUrls.baseUrl + manoeuvreId).then(function(result){
          return result.data;
        });
      },
      delete: function(manoeuvreId){
        return $http['delete'](manoeuvreUrls.baseUrl+manoeuvreId);
      },
      create: function(){
        var deferred = $q.defer();
        var AddManoeuvreCtrl = function ($scope, $uibModalInstance) {
          $scope.newManoeuvre = {
            twistedBonus: 0,
            flippedBonus: 0,
            reversedBonus:0,
            hasSides: true,
            onlySynchro: false
          };

          $scope.synchroOptions =[{_id: true, name: 'Yes'},{_id: false, name: 'No'}];
          $scope.sideOptions =[{_id: true, name: 'Yes'},{_id: false, name: 'No'}];
          $scope.idProperty = '_id';
          $scope.nameProperty = 'name';
          $scope.bootstrapSuffix = 'default';

          $scope.addManoeuvre = function(){
            $uibModalInstance.close({newManoeuvre: $scope.newManoeuvre, action: 'Save'});
          };
          $scope.cancel = function(){
            $uibModalInstance.close({newManoeuvre: null, action: 'Cancel'});
          };
        };
        var modalInstance = $uibModal.open({
          templateUrl: 'components/entities/manoeuvre/modalAddManoeuvre/modalAddManoeuvre.html',
          controller: [
            '$scope',
            '$uibModalInstance',
            AddManoeuvreCtrl
          ],
          keyboard: false,
          backdrop: 'static'
        });
        modalInstance.result.then(function (data) {
          switch (data.action) {
            case 'Save':
            {
              console.log('adding new manoeuvre:  ' + data.newManoeuvre.name);
              $http.post(manoeuvreUrls.baseUrl, data.newManoeuvre).then(function(result){
                deferred.resolve(result.data);
              }, function(result){
                deferred.reject(result.data);
              });
              break;
            }
            case 'Cancel':
            {
              deferred.reject('Action canceled');
              break;
            }
          }
        }, function (err) {
          deferred.reject('Error:' + err);
        });
        return deferred.promise;
      },
      save: function(manoeuvre){
        return $http.put(manoeuvreUrls.baseUrl + manoeuvre._id, manoeuvre);
      }
    };
  }
  acropyxApp.factory('ManoeuvreService',['$http', '$uibModal', '$q', 'manoeuvreUrls', ManoeuvreService]);
})();
