(function(){
  'use strict';
  var acropyxApp = angular.module('acropyxApp');
  acropyxApp.constant('flightUrls',{baseUrl:'/api/flights/'});
  function FlightService($http, flightUrls){
    return {
      query: function(){
        return $http.get(flightUrls.baseUrl).then(function(result){
          return result.data;
        });
      },
      get: function(flightId){
        return $http.get(flightUrls.baseUrl + flightId).then(function(result){
          return result.data;
        });
      },
      queryManoeuvres: function(flightId){
        return $http.get(flightUrls + flightId + '/manoeuvres').then(function(result){
          return result.data;
        });
      },
      queryVotes: function(flightId){
        return $http.get(flightUrls + flightId + '/votes').then(function(result){
          return result.data;
        });
      },
      delete: function(flightId){
        return $http['delete'](flightUrls.baseUrl + flightId);
      },
      save: function(flight){
        return $http.put(flightUrls.baseUrl + flight._id, flight);
      },
      reset: function(flightId){
        return $http.put(flightUrls.baseUrl + flightId + '/reset/', {flightId: flightId}).then(function(result){
          return result.data;
        });
      },
      saveManoeuvres: function(flightId, manoeuvres){
        return $http.put(flightUrls.baseUrl + flightId + '/manoeuvres/', {flightId: flightId, manoeuvres: manoeuvres}).then(function(result){
          return result.data;
        });
      },
      saveVotes: function(flightId, votes){
        return $http.put(flightUrls.baseUrl + flightId + '/votes/', {flightId: flightId, votes: votes}).then(function(result){
          return result.data;
        });
      },
      addWarning: function(flightId){
        return $http.put(flightUrls.baseUrl + flightId + '/addWarning/', {flightId: flightId}).then(function(result){
          return result.data;
        });
      },
      removeWarning: function(flightId){
        return $http.put(flightUrls.baseUrl + flightId + '/removeWarning/', {flightId: flightId}).then(function(result){
          return result.data;
        });
      },
      startFlight: function(flightId, display){
        return $http.put(flightUrls.baseUrl + flightId + '/start/', {flightId: flightId, display: display}).then(function(result){
          return result.data;
        });
      },
      endFlight: function(flightId, display){
        return $http.put(flightUrls.baseUrl + flightId + '/end/', {flightId: flightId, display: display}).then(function(result){
          return result.data;
        });
      },
      displayFlight: function(flightId){
        return $http.put(flightUrls.baseUrl + flightId + '/display/', {flightId: flightId}).then(function(result){
          return result.data;
        });
      },
      displayFlightResults: function(flightId){
        return $http.put(flightUrls.baseUrl + flightId + '/displayResults/', {flightId: flightId}).then(function(result){
          return result.data;
        });
      },
      changeOrder: function(flightId, newOrder){
        return $http.post(flightUrls.baseUrl +'changeOrder', {flightId: flightId, newOrder : newOrder});
      }
    };
  }
  acropyxApp.factory('FlightService',['$http', 'flightUrls', FlightService]);
})();
