(function(){
  'use strict';
  var acropyxApp = angular.module('acropyxApp');
  acropyxApp.constant('teamUrls',{baseUrl:'/api/teams/'});
  function TeamService($http, $uibModal, $q, teamUrls){
    return {
      query: function(){
        return $http.get(teamUrls.baseUrl).then(function(result){
          return result.data;
        });
      },
      get: function(teamId){
        return $http.get(teamUrls.baseUrl + teamId).then(function(result){
          return result.data;
        });
      },
      create: function(){
        var deferred = $q.defer();
        var AddTeamCtrl = function ($scope, $uibModalInstance, pilots) {
          $scope.newTeam = {
            pilots: []
          };
          $scope.pilots = pilots;
          $scope.addTeam = function(){
            $uibModalInstance.close({newTeam: $scope.newTeam, action: 'Save'});
          };
          $scope.cancel = function(){
            $uibModalInstance.close({newTeam: null, action: 'Cancel'});
          };
        };
        var modalInstance = $uibModal.open({
          templateUrl: 'components/entities/team/modalAddTeam/modalAddTeam.html',
          controller: [
            '$scope', 
            '$uibModalInstance',
            'pilots', 
            AddTeamCtrl
          ],
          keyboard: false,
          backdrop: 'static',
          resolve:{/* @ngInject */
            pilots: function(PilotService){
              return PilotService.query();
            }
          }
        });
        modalInstance.result.then(function (data) {
          switch (data.action) {
            case 'Save':
            {
              console.log('adding new team:  ' + data.newTeam.name);
              $http.post(teamUrls.baseUrl, data.newTeam).then(function(result){
                deferred.resolve(result.data);
              }, function(result){
                deferred.reject(result.data);
              });
              break;
            }
            case 'Cancel':
            {
              deferred.reject('Action canceled');
              break;
            }
          }
        }, function (err) {
          deferred.reject('Error:' + err);
        });
        return deferred.promise;
      },
      delete: function(teamId){
        return $http['delete'](teamUrls.baseUrl + teamId);
      },
      save: function(team){
        return $http.put(teamUrls.baseUrl + team._id, team);
      }
    };
  }
  acropyxApp.factory('TeamService',['$http', '$uibModal', '$q', 'teamUrls', TeamService]);
})();
