(function(){
  'use strict';
  var acropyxApp = angular.module('acropyxApp');
  acropyxApp.constant('sponsorUrls',{baseUrl:'/api/sponsors/'});
  function SponsorService($http, $uibModal, $q, sponsorUrls){
    return {
      query: function(){
        return $http.get(sponsorUrls.baseUrl).then(function(result){
          return result.data;
        });
      },
      get: function(sponsorId){
        return $http.get(sponsorUrls.baseUrl + sponsorId).then(function(result){
          return result.data;
        });
      },
      save: function(sponsor){
        return $http.put(sponsorUrls.baseUrl+ sponsor._id, sponsor);
      },
      delete: function(sponsorId){
        return $http['delete'](sponsorUrls.baseUrl+sponsorId);
      },
      create: function(){
        var deferred = $q.defer();
        var AddSponsorCtrl = function ($scope, $uibModalInstance) {
          $scope.addSponsor = function(){
            $uibModalInstance.close({newSponsor: $scope.newSponsor, action: 'Save'});
          };
          $scope.cancel = function(){
            $uibModalInstance.close({newSponsor: null, action: 'Cancel'});
          };
        };
        var modalInstance = $uibModal.open({
          templateUrl: 'components/entities/sponsor/modalAddSponsor/modalAddSponsor.html',
          controller: [
            '$scope', 
            '$uibModalInstance', 
            AddSponsorCtrl
          ],
          keyboard: false,
          backdrop: 'static'
        });
        modalInstance.result.then(function (data) {
          switch (data.action) {
            case 'Save':
            {
              console.log('adding new sponsor:  ' + data.newSponsor.name);
              $http.post(sponsorUrls.baseUrl, data.newSponsor).then(function(result){
                deferred.resolve(result.data);
              }, function(result){
                deferred.reject(result.data);
              });
              break;
            }
            case 'Cancel':
            {
              deferred.reject('Action canceled');
              break;
            }
          }
        }, function (err) {
          deferred.reject('Error:' + err);
        });
        return deferred.promise;
      }
    };
  }
  acropyxApp.factory('SponsorService',['$http', '$uibModal', '$q', 'sponsorUrls', SponsorService]);
})();
