(function(){
  'use strict';
  var acropyxApp = angular.module('acropyxApp');
  acropyxApp.constant('seasonUrls',{baseUrl:'/api/seasons/'});
  function SeasonService($http, $uibModal, $q, seasonUrls){
    return {
      query: function(){
        return $http.get(seasonUrls.baseUrl).then(function(result){
          return result.data;
        });

      },
      queryEvents: function(seasonId){
        return $http.get(seasonUrls.baseUrl + seasonId + '/events').then(function(result){
          return result.data;
        });
      },
      queryRankingSolo: function(seasonId){
        return $http.get(seasonUrls.baseUrl + seasonId + '/rankingSolo').then(function(result){
          return result.data;
        });
      },
      queryRankingSynchro: function(seasonId){
        return $http.get(seasonUrls.baseUrl + seasonId + '/rankingSynchro').then(function(result){
          return result.data;
        });
      },
      get: function(seasonId){
        return $http.get(seasonUrls.baseUrl + seasonId).then(function(result){
          return result.data;
        });
      },
      getDisplayUrl: function(seasonId){
        return $http.get(seasonUrls.baseUrl + seasonId + '/displayUrl').then(function(result){
          return result.data;
        });
      },
      delete: function(seasonId){
        return $http['delete'](seasonUrls.baseUrl + seasonId);
      },
      create: function(){
        var deferred = $q.defer();
        var AddSeasonCtrl = function ($scope, $uibModalInstance) {
          $scope.addSeason = function(){
            $uibModalInstance.close({newSeason: $scope.newSeason, action: 'Save'});
          };
          $scope.cancel = function(){
            $uibModalInstance.close({newSeason: null, action: 'Cancel'});
          };
        };
        var modalInstance = $uibModal.open({
          templateUrl: 'components/entities/season/modalAddSeason/modalAddSeason.html',
          controller: [
            '$scope', 
            '$uibModalInstance',
            AddSeasonCtrl
          ],
          keyboard: false,
          backdrop: 'static'
        });
        modalInstance.result.then(function (data) {
          switch (data.action) {
            case 'Save':
            {
              console.log('adding new season:  ' + data.newSeason.name);
              $http.post(seasonUrls.baseUrl, data.newSeason).then(function(result){
                deferred.resolve(result.data);
              }, function(result){
                deferred.reject(result.data);
              });
              break;
            }
            case 'Cancel':
            {
              deferred.reject('Action canceled');
              break;
            }
          }
        }, function (err) {
          deferred.reject('Error:' + err);
        });
        return deferred.promise;
      },
      save: function(season){
        return $http.put(seasonUrls.baseUrl + season._id, season);
      },
      showRankings: function(seasonId){
        return $http.put(seasonUrls.baseUrl + seasonId + '/displayRankings', {seasonId: seasonId}).then(function(result){
          return result.data;
        });
      },
      generateSoloRankingReportPDF: function(seasonId){
        return $http.put(seasonUrls.baseUrl + seasonId + '/generateSoloRankingReportPDF').then(function(result){
          return result.data;
        });
      },
      generateSynchroRankingReportPDF: function(seasonId){
        return $http.put(seasonUrls.baseUrl + seasonId + '/generateSynchroRankingReportPDF').then(function(result){
          return result.data;
        });
      }
    };
  }
  acropyxApp.factory('SeasonService',['$http', '$uibModal', '$q', 'seasonUrls', SeasonService]);
})();
