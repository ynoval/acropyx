(function(){
  'use strict';
  var acropyxApp = angular.module('acropyxApp');
  acropyxApp.constant('judgeUrls',{baseUrl:'/api/judges/', typesUrl: '/api/judgeTypes/'});
  function JudgeService($http, $uibModal, $q, judgeUrls){
    return {
      query: function(){
        return $http.get(judgeUrls.baseUrl).then(function(result){
          return result.data;
        });
      },
      get: function(judgeId){
        return $http.get(judgeUrls.baseUrl + judgeId).then(function(result){
          return result.data;
        });
      },
      queryTypes: function(){
        return $http.get(judgeUrls.typesUrl).then(function(result){
          return result.data;
        });
      },
      delete: function(judgeId){
        return $http['delete'](judgeUrls.baseUrl + judgeId);
      },
      create: function(){
        var deferred = $q.defer();
        var AddJudgeCtrl = function ($scope, $uibModalInstance, judgeTypes) {
          $scope.judgeTypes = judgeTypes;
          //TODO: improve
          $scope.newJudge = {};
          $scope.newJudge.type = _.result(_.find(judgeTypes, function(judge) {
            return judge.name === 'FAI';
          }), '_id');

          $scope.idProperty = '_id';
          $scope.nameProperty = 'name';
          $scope.bootstrapSuffix = 'default';
          $scope.addJudge = function(){
            $uibModalInstance.close({newJudge: $scope.newJudge, action: 'Save'});
          };
          $scope.cancel = function(){
            $uibModalInstance.close({newJudge: null, action: 'Cancel'});
          };
        };
        var modalInstance = $uibModal.open({
          templateUrl: 'components/entities/judge/modalAddJudge/modalAddJudge.html',
          controller: [
            '$scope', 
            '$uibModalInstance', 
            'judgeTypes', 
            AddJudgeCtrl
          ],
          keyboard: false,
          backdrop: 'static',
          resolve: {/* @ngInject */
            judgeTypes : function(JudgeService){
              return JudgeService.queryTypes();
            }
          }
        });
        modalInstance.result.then(function (data) {
          switch (data.action) {
            case 'Save':
            {
              console.log('adding new judge:  ' + data.newJudge.name);
              $http.post(judgeUrls.baseUrl, data.newJudge).then(function(result){
                deferred.resolve(result.data);
              }, function(result){
                deferred.reject(result.data);
              });
              break;
            }
            case 'Cancel':
            {
              deferred.reject('Action canceled');
              break;
            }
          }
        }, function (err) {
          deferred.reject('Error:' + err);
        });
        return deferred.promise;
      },
      save: function(judge){
        return $http.put(judgeUrls.baseUrl + judge._id, judge);
      }
    };
  }
  acropyxApp.factory('JudgeService',['$http', '$uibModal', '$q', 'judgeUrls', JudgeService]);
})();
