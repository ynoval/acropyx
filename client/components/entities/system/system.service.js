(function(){
  'use strict';
  var acropyxApp = angular.module('acropyxApp');
  acropyxApp.constant('systemUrls',{countriesUrl:'/api/countries/', markTypesUrl:'/api/markTypes/'});
  function SystemService($http, systemUrls){
    return {
      queryCountries: function(){
        return $http.get(systemUrls.countriesUrl).then(function(result){
          return result.data;
        });
      },
      queryMarkTypes: function(){
        return $http.get(systemUrls.markTypesUrl).then(function(result){
          return result.data;
        });
      }
    };
  }
  acropyxApp.factory('SystemService',['$http', 'systemUrls', SystemService]);
})();
