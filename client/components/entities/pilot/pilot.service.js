(function(){
  'use strict';
  var acropyxApp = angular.module('acropyxApp');
  acropyxApp.constant('pilotUrls',{baseUrl:'/api/pilots/'});
  function PilotService($http, $uibModal, $q, pilotUrls){
    return {
      query: function(){
        return $http.get(pilotUrls.baseUrl).then(function(result){
          return result.data;
        });
      },
      get: function(pilotId){
        return $http.get(pilotUrls.baseUrl + pilotId).then(function(result){
          return result.data;
        });
      },
      save: function(pilot){
        return $http.put(pilotUrls.baseUrl + pilot._id, pilot);
      },
      delete: function(pilotId){
        return $http['delete'](pilotUrls.baseUrl + pilotId);
      },
      create: function(){
        var deferred = $q.defer();
        var AddPilotCtrl = function ($scope, $uibModalInstance, countries) {

          $scope.countries = countries;

          $scope.newPilot = {sex :'Male'};

          $scope.sexOptions = [{value: 'Female', label: 'Female'}, {value: 'Male', label: 'Male'}];
          $scope.idPropertySex = 'value';
          $scope.namePropertySex = 'label';

          $scope.addPilot = function(){
            $uibModalInstance.close({newPilot: $scope.newPilot, action: 'Save'});
          };
          $scope.cancel = function(){
            $uibModalInstance.close({newPilot: null, action: 'Cancel'});
          };
        };
        var modalInstance = $uibModal.open({
          templateUrl: 'components/entities/pilot/modalAddPilot/modalAddPilot.html',
          controller: [
            '$scope', 
            '$uibModalInstance', 
            'countries', 
            AddPilotCtrl
          ],
          keyboard: false,
          backdrop: 'static',
          resolve:{/* @ngInject */
            countries: function(SystemService){
              return SystemService.queryCountries();
            }
          }
        });
        modalInstance.result.then(function (data) {
          switch (data.action) {
            case 'Save':
            {
              console.log('adding new pilot:  ' + data.newPilot.name);
              $http.post(pilotUrls.baseUrl, data.newPilot).then(function(result){
                deferred.resolve(result.data);
              }, function(result){
                deferred.reject(result.data);
              });
              break;
            }
            case 'Cancel':
            {
              deferred.reject('Action canceled');
              break;
            }
          }
        }, function (err) {
          deferred.reject('Error:' + err);
        });
        return deferred.promise;
      }
    };
  }
  acropyxApp.factory('PilotService',['$http', '$uibModal', '$q', 'pilotUrls', PilotService]);
})();
