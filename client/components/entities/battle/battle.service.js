(function(){
  'use strict';
  var acropyxApp = angular.module('acropyxApp');
  acropyxApp.constant('battleUrls',{baseUrl:'/api/battles/'});
  function BattleService($http, battleUrls){
    return {
      display: function(firstFlightId, secondFlightId){
        return $http.put(battleUrls.baseUrl + 'display', {firstFlightId: firstFlightId, secondFlightId: secondFlightId}).then(function(result){
          return result.data;
        });
      },
      displayResults: function(firstFlightId, secondFlightId){
        return $http.put(battleUrls.baseUrl + 'displayResults', {firstFlightId: firstFlightId, secondFlightId: secondFlightId}).then(function(result){
          return result.data;
        });
      }
    };
  }
  acropyxApp.factory('BattleService',['$http', 'battleUrls', BattleService]);
})();
