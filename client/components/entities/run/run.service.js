(function(){
  'use strict';
  var acropyxApp = angular.module('acropyxApp');
  acropyxApp.constant('runUrls',{baseUrl:'/api/runs/', typesUrl:'/api/runTypes/'});
  function RunService($http, $uibModal, $q, runUrls){
    return {
      query: function(){
        return $http.get(runUrls.baseUrl).then(function(result){
          return result.data;
        });
      },
      queryTypes: function(){
        return $http.get(runUrls.typesUrl).then(function(result){
          return result.data;
        });
      },
      queryFlights: function(runId){
        return $http.get(runUrls.baseUrl + runId + '/flights').then(function(result){
          return result.data;
        });
      },
      queryResults: function(runId){
        return $http.get(runUrls.baseUrl + runId + '/results').then(function(result){
          return result.data;
        });
      },
      queryJudges: function(runId){
        return $http.get(runUrls.baseUrl + runId + '/judges').then(function(result){
          return result.data;
        });
      },
      get: function(runId){
        return $http.get(runUrls.baseUrl + runId).then(function(result){
          return result.data;
        });
      },
      delete: function(runId){
        return $http['delete'](runUrls.baseUrl + runId);
      },
      reset:function(runId){
        return $http.put(runUrls.baseUrl + runId + '/reset', {runId: runId})
          .then(function(result){
            return result.data;
          });
      },
      reopen:function(runId){
        return $http.put(runUrls.baseUrl + runId + '/reopen', {runId: runId})
          .then(function(result){
            return result.data;
          });
      },
      create: function(competition){
        var deferred = $q.defer();
        var AddRunCtrl = function ($scope, $uibModalInstance, competitions, competition, runTypes) {
          $scope.selectedCompetition = competition;

          $scope.newRun = {};
          if(competition){
            $scope.newRun.competition = competition._id;
          }

          $scope.newRun.type = _.result(_.find(runTypes, function(run) {
            return run.name === 'Standard';
          }), '_id');

          $scope.competitions = competitions;
          $scope.runTypes = runTypes;
          $scope.idProperty = '_id';
          $scope.nameProperty = 'name';
          $scope.bootstrapSuffix = 'default';

          $scope.addRun = function(){
            $uibModalInstance.close({newRun: $scope.newRun, action: 'Save'});
          };
          $scope.cancel = function(){
            $uibModalInstance.close({newRun: null, action: 'Cancel'});
          };
        };
        var modalInstance = $uibModal.open({
          templateUrl: 'components/entities/run/modalAddRun/modalAddRun.html',
          controller: [
            '$scope', 
            '$uibModalInstance', 
            'competitions',
            'competition', 
            'runTypes', 
            AddRunCtrl
          ],
          keyboard: false,
          backdrop: 'static',
          resolve: {/* @ngInject */
            competitions: function(CompetitionService){
              return CompetitionService.query();
            },/* @ngInject */
            runTypes: function(RunService){
              return RunService.queryTypes();
            }, /* @ngInject */
            competition: function(){
              return competition;
            }
          }
        });
        modalInstance.result.then(function (data) {
          switch (data.action) {
            case 'Save':
            {
              console.log('adding new run:  ' + data.newRun.name);
              $http.post(runUrls.baseUrl, data.newRun).then(function(result){
                deferred.resolve(result.data);
              }, function(result){
                deferred.reject(result.data);
              });
              break;
            }
            case 'Cancel':
            {
              deferred.reject('Action canceled');
              break;
            }
          }
        }, function (err) {
          deferred.reject('Error:' + err);
        });
        return deferred.promise;
      },
      save: function(run){
        return $http.put(runUrls.baseUrl + run._id, run);
      },
      changeOrder: function(runId, newOrder){
        return $http.post(runUrls.baseUrl +'changeOrder', {runId: runId, newOrder : newOrder});
      },
      generateStartingOrderPDF: function(runId){
        return $http.put(runUrls.baseUrl + runId + '/generateStartingOrderPDF').then(function(result){
          return result.data;
        });
      },
      generateStartingOrder: function(runId, regenerate){
        return $http.put(runUrls.baseUrl + runId + '/prepare', {id: runId, regenerate: regenerate}).then(function(result){
          return result.data;
        });
      },
      startRun: function(runId, display){
        return $http.put(runUrls.baseUrl + runId + '/start', {runId: runId, display: display})
          .then(function(result){
            return result.data;
          });
      },
      endRun: function(runId, display){
        return $http.put(runUrls.baseUrl + runId + '/end', {runId: runId, display: display})
          .then(function(result){
            return result.data;
          });
      },
      displayStartingOrder: function(runId){
        return $http.put(runUrls.baseUrl + runId + '/displayStartingOrder/', {runId: runId}).then(function(result){
          return result.data;
        });
      },
      generateResultsPDF: function(runId){
        return $http.put(runUrls.baseUrl + runId + '/generateResultsPDF').then(function(result){
          return result.data;
        });
      },
      displayResults: function(runId){
        return $http.put(runUrls.baseUrl + runId + '/displayResults/', {runId: runId}).then(function(result){
          return result.data;
        });
      },
      displayBattleResults : function(runId){
        return $http.put(runUrls.baseUrl + runId + '/displayBattlesResults/', {runId: runId}).then(function(result){
          return result.data;
        });
      },
      generateManoeuvresPDF: function(runId){
        return $http.put(runUrls.baseUrl + runId + '/generateManoeuvresPDF').then(function(result){
          return result.data;
        });
      }
    };
  }
  acropyxApp.factory('RunService',['$http', '$uibModal', '$q', 'runUrls', RunService]);
})();
