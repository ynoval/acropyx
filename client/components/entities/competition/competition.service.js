(function(){
  'use strict';
  var acropyxApp = angular.module('acropyxApp');
  acropyxApp.constant('competitionUrls',{baseUrl:'/api/competitions/', typesUrl: '/api/competitionTypes/'});
  function CompetitionService($http, $uibModal, $q, competitionUrls){
    return {
      query: function(){
        return $http.get(competitionUrls.baseUrl).then(function(result){
          return result.data;
        });
      },
      queryRuns: function(competitionId){
        return $http.get(competitionUrls.baseUrl + competitionId + '/runs').then(function(result){
          return result.data;
        });
      },
      queryTypes: function(){
        return $http.get(competitionUrls.typesUrl).then(function(result){
          return result.data;
        });
      },
      queryType: function(typeId){
        return $http.get(competitionUrls.typesUrl + typeId).then(function(result){
          return result.data;
        });
      },
      queryMarkTypes:function(competitionId){
        return $http.get(competitionUrls.baseUrl + competitionId +'/markTypes').then(function(result){
          return result.data;
        });
      },
      queryAllCompetitors: function(competitionId){
        return $http.get(competitionUrls.baseUrl + competitionId + '/allCompetitors').then(function(result){
          return result.data;
        });
      },
      queryResults: function(competitionId){
        return $http.get(competitionUrls.baseUrl + competitionId + '/results').then(function(result){
          return result.data;
        });
      },
      queryResultsFAI: function(competitionId){
        return $http.get(competitionUrls.baseUrl + competitionId + '/resultsFAI').then(function(result){
          return result.data;
        });
      },
      generateResultsReportPDF: function(competitionId){
        return $http.put(competitionUrls.baseUrl + competitionId + '/generateResultsReportPDF').then(function(result){
          return result.data;
        });
      },
      generateResultsReportCSV: function(competitionId){
        return $http.put(competitionUrls.baseUrl + competitionId + '/generateResultsReportCSV').then(function(result){
          return result.data;
        });
      },
      get: function(competitionId){
        return $http.get(competitionUrls.baseUrl + competitionId).then(function(result){
          return result.data;
        });
      },
      delete: function(competitionId){
        return $http['delete'](competitionUrls.baseUrl + competitionId);
      },
      reset:function(competitionId){
        return $http.put(competitionUrls.baseUrl + competitionId + '/reset', {competitionId: competitionId})
          .then(function(result){
            return result.data;
          });
      },
      reopen:function(competitionId){
        return $http.put(competitionUrls.baseUrl + competitionId + '/reopen', {competitionId: competitionId})
          .then(function(result){
            return result.data;
          });
      },
      create: function(event){
        var deferred = $q.defer();
        var AddCompetitionCtrl = function ($scope, $uibModalInstance, competitionTypes, events, event) {
          $scope.selectedEvent = event;
          $scope.newCompetition = {};
          if(event){
            $scope.newCompetition.event = event._id;
          }
          $scope.newCompetition.type = _.result(_.find(competitionTypes, function(competition) {
            return competition.name === 'Solo';
          }), '_id');

          $scope.events = events;

          $scope.competitionTypes = competitionTypes;
          $scope.idPropertyType = '_id';
          $scope.namePropertyType = 'name';
          $scope.bootstrapSuffix = 'default';

          $scope.rankingOptions = [{value: true, label: 'Yes'}, {value: false, label: 'No'}];
          $scope.idPropertyRanking = 'value';
          $scope.namePropertyRanking = 'label';
          $scope.bootstrapSuffix = 'default';


          $scope.addCompetition = function(){
            $uibModalInstance.close({newCompetition: $scope.newCompetition, action: 'Save'});
          };
          $scope.cancel = function(){
            $uibModalInstance.close({newCompetition: null, action: 'Cancel'});
          };
        };
        var modalInstance = $uibModal.open({
          templateUrl: 'components/entities/competition/modalAddCompetition/modalAddCompetition.html',
          controller: [
            '$scope', 
            '$uibModalInstance', 
            'competitionTypes', 
            'events',
            'event',
            AddCompetitionCtrl
          ],
          keyboard: false,
          backdrop: 'static',
          resolve:{/* @ngInject */
            competitionTypes : function(CompetitionService){
              return CompetitionService.queryTypes();
            },/* @ngInject */
            events: function(EventService){
              return EventService.query();
            },/* @ngInject */
            event: function(){
              return event;
            }
          }
        });
        modalInstance.result.then(function (data) {
          switch (data.action) {
            case 'Save':
            {
              console.log('adding new competition:  ' + data.newCompetition.name);
              $http.post(competitionUrls.baseUrl, data.newCompetition).then(function(result){
                deferred.resolve(result.data);
              }, function(result){
                deferred.reject(result.data);
              });
              break;
            }
            case 'Cancel':
            {
              deferred.reject('Action canceled');
              break;
            }
          }
        }, function (err) {
          deferred.reject('Error:' + err);
        });
        return deferred.promise;
      },
      save: function(competition){
        return $http.put(competitionUrls.baseUrl + competition._id, competition);
      },
      addCompetitor: function(competitionId, competitor){
        return $http.post(competitionUrls.baseUrl + competitionId + '/competitor', competitor);
      },
      startCompetition: function(competitionId, display){
        return $http.put(competitionUrls.baseUrl + competitionId + '/start', {competitionId: competitionId, display: display})
          .then(function(result){
            return result.data;
          });
      },
      endCompetition: function(competitionId, display){
        return $http.put(competitionUrls.baseUrl + competitionId + '/end', {competitionId: competitionId, display: display})
          .then(function(result){
            return result.data;
          });
      },
      displayResults: function(competitionId){
        return $http.put(competitionUrls.baseUrl + competitionId + '/displayResults/', {competitionId: competitionId}).then(function(result){
          return result.data;
        });
      }
    };
  }
  acropyxApp.factory('CompetitionService',[
    '$http', 
    '$uibModal', 
    '$q', 
    'competitionUrls', CompetitionService]);
})();
