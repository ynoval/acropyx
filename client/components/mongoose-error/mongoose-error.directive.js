/**
 * Removes server error when user updates input
 */
(function(){
  'use strict';
  var acropyxApp = angular.module('acropyxApp');
  function MongooseError(){
    return {
      restrict: 'A',
      require: 'ngModel',
      link: function(scope, element, attrs, ngModel) {
        element.on('keydown', function() {
          return ngModel.$setValidity('mongoose', true);
        });
      }
    };
  }
  acropyxApp.directive('mongooseError', [ MongooseError]);
})();
