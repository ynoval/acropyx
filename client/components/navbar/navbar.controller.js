(function(){
  'use strict';
  var acropyxApp = angular.module('acropyxApp');
  function NavBarCtrl($scope, $location, Auth, EventService, socket) {
    function loadActiveEvents(){
      EventService.queryActiveEvents().then(function(events){
        $scope.menu = [];
        events.forEach(function(event){
          $scope.menu.push({
            title: event.season.name + ' - ' + event.name,
            link: '/manager/event/' + event._id
          });
        });
      });
    }
    function init(){
      $scope.menu = [];
      loadActiveEvents();

      $scope.isCollapsed = true;
      $scope.isLoggedIn = Auth.isLoggedIn;
      $scope.isAdmin = Auth.isAdmin;
      $scope.isManager = Auth.isManager;
      $scope.getCurrentUser = Auth.getCurrentUser;
    }
    init();
    socket.syncUpdates('event', [], function(){
      loadActiveEvents();
    });

    $scope.logout = function() {
      Auth.logout();
      $location.path('/login');
    };
    $scope.isActive = function(route) {
      return route === $location.path();
    };
  }
  acropyxApp.controller('NavbarCtrl', [
    '$scope',
    '$location',
    'Auth',
    'EventService',
    'socket',
    NavBarCtrl
  ]);
})();
