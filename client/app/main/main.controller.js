(function(){
  'use strict';
  var acropyxApp = angular.module('acropyxApp');
  function MainCtrl($scope, $window, SeasonService, publicEvents) {
    function init() {
      $scope.publicEvents = publicEvents;
    }
    init();

    $scope.isEventActive = function(event){
      var today = new Date();
      today.setHours(0, 0, 0);
      return (today > event.startDate && today < event.endDate);
    };

    $scope.openDisplay = function(event){
      SeasonService.getDisplayUrl(event.season._id).then(function(result){
        $window.open(result.url);
      });
    };
  }
  acropyxApp.controller('MainCtrl', [
    '$scope',
    '$window',
    'SeasonService',
    'publicEvents',
    MainCtrl]);
})();

