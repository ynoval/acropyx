(function(){
  'use strict';
  var acropyxApp = angular.module('acropyxApp');
  function Config($stateProvider) {
    $stateProvider
      .state('main', {
        url: '/',
        templateUrl: 'app/main/main.html',
        controller: 'MainCtrl',
        resolve: { /* @ngInject */
          publicEvents : function(EventService){
            return EventService.queryPublicEvents();
          }
        }
      });
  }
  acropyxApp.config(['$stateProvider', Config]);
})();
