(function () {
  'use strict';
  var acropyxApp = angular.module('acropyxApp');

  function ManagerEventCtrl($rootScope, $scope, $window, eventInstance, competitions,
                            $timeout, EventService, SeasonService, $uibModal) {
    $rootScope.activeSeason = eventInstance.season._id;
    $scope.eventInstance = eventInstance;
    $scope.eventInstance.activeCompetitions = competitions;

    $scope.displayerBox = {
      isOpen: true
    };

    $scope.openDisplayer = function () {
      $scope.isOpeningDisplayer = true;
      SeasonService.getDisplayUrl(eventInstance.season._id).then(function (result) {
        $window.open(result.url);
        $timeout(function () {
          $scope.isOpeningDisplayer = false;
        }, 1000);
      });
    };

    $scope.showSeasonRankings = function () {
      $scope.isShowingRankings = true;
      SeasonService.showRankings(eventInstance.season._id).then(function () {
        $timeout(function () {
          $scope.isShowingRankings = false;
        }, 2000);
      });
    };

    //$scope.showEventSponsors = function(){
    //  $scope.isShowingSponsors = true;
    //  $timeout(function() {
    //    $scope.isShowingSponsors = false;
    //  }, 2000);
    //};
    //$scope.showEventImages = function(){
    //  $scope.isShowingImages = true;
    //  $timeout(function() {
    //    $scope.isShowingImages = false;
    //  }, 2000);
    //};

    $scope.showEventResults = function () {
      $scope.isShowingEventResults = true;
      EventService.displayResults(eventInstance._id).then(function () {
        $timeout(function () {
          $scope.isShowingEventResults = false;
        }, 2000);
      });
    };
    $scope.openEventResults = function () {
      var ShowEventResultsCtrl = function ($scope, $uibModalInstance, eventInstance, competitions, CompetitionService) {
        $scope.eventInstance = eventInstance;
        $scope.eventInstance.filledCompetitions = [];

        function loadResults() {
          competitions.forEach(function (competition) {
            CompetitionService.queryResults(competition._id).then(function (results) {
              competition.compResults = results;
              $scope.eventInstance.filledCompetitions.push(competition);
            });
          });
        }

        loadResults();


        $scope.close = function () {
          $uibModalInstance.close();
        };
      };
      $uibModal.open({
        templateUrl: 'app/manager/dashboard/modals/modalEventResults.html',
        controller: [
          '$scope',
          '$uibModalInstance',
          'eventInstance',
          'competitions',
          'CompetitionService',
          ShowEventResultsCtrl
        ],
        windowClass: 'large-modal',
        keyboard: false,
        backdrop: 'static',
        resolve: {/* @ngInject */
          eventInstance: function () {
            return $scope.eventInstance;
          },/* @ngInject */
          competitions: function (EventService) {
            return EventService.queryCompetitions($scope.eventInstance._id);
          }
        }
      });
    };

    $scope.showPaf = function (msg) {
      $scope.isShowingPaf = true;
      EventService.displayPaf(eventInstance._id, msg).then(function () {
        $timeout(function () {
          $scope.isShowingPaf = false;
        }, 2000);
      });
    };

    $scope.clearPaf = function () {
      $scope.isClearingPaf = true;
      EventService.clearPaf(eventInstance._id).then(function () {
        $timeout(function () {
          $scope.isClearingPaf = false;
        }, 2000);
      });
    };

    $scope.showMessage = function (text) {
      $scope.isShowingMessage = true;
      EventService.displayMessage(eventInstance._id, text).then(function () {
        $timeout(function () {
          $scope.isShowingMessage = false;
        }, 2000);
      });
    };

    $scope.clearMessage = function () {
      $scope.isClearingMessage = true;
      EventService.clearMessage(eventInstance._id).then(function () {
        $timeout(function () {
          $scope.isClearingMessage = false;
        }, 2000);
      });
    };
  }

  acropyxApp.controller('ManagerEventCtrl', [
    '$rootScope',
    '$scope',
    '$window',
    'eventInstance',
    'competitions',
    '$timeout',
    'EventService',
    'SeasonService',
    ManagerEventCtrl
  ]);
})();
