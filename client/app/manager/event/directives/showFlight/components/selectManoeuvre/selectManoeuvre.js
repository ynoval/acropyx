(function () {
  'use strict';
  var module = angular.module('acropyxApp');

  function ComponentController() {
    var model = this;
    model.$onInit = function () {
      model.isTwisted = false;
      model.isFlipped = false;
      model.isReversed = false;
    };

    function getBonus(){
      var result = [];
      if(model.isTwisted){
        result.push('Twisted');
      }
      if(model.isFlipped){
        result.push('Flipped');
      }
      if(model.isReversed){
        result.push('Reversed');
      }
      return result;
    }
    function getSelectedManoeuvre(){
      return {
        _id: model.manoeuvre._id,
        name: model.manoeuvre.name,
        coefficient: model.manoeuvre.coefficient
      };
    }

    model.add = function(side){
      model.onSelect(
        {
          selectedManoeuvre: {
            manoeuvre: getSelectedManoeuvre(),
            side: side,
            bonusType: getBonus()
          }
        }
      );
      model.isTwisted = false;
      model.isFlipped = false;
      model.isReversed = false;
    };
    //
    // model.addRight = function(){
    //   model.onSelect(
    //     {
    //       selectedManoeuvre: {
    //         manoeuvre: getSelectedManoeuvre(),
    //         side: 'Right'
    //       }
    //     }
    //   );
    // };
    // model.add = function(){
    //   model.onSelect(
    //     {
    //       selectedManoeuvre: {
    //         manoeuvre: getSelectedManoeuvre()
    //       }
    //     }
    //   );
    // };


    model.changeTwisted = function(){
      if(model.isTwisted){
        model.isFlipped = false;
        // model.isReversed = false;
      }
    };

    model.changeFlipped = function(){
      if(model.isFlipped){
        model.isTwisted = false;
        // model.isReversed = false;
      }
    };

    // model.changeReversed = function(){
    //   if(model.isReversed){
    //     model.isTwisted = false;
    //     model.isFlipped = false;
    //   }
    // };
  }

  module.component('selectManoeuvre', {
    templateUrl: 'app/manager/event/directives/showFlight/components/selectManoeuvre/selectManoeuvre.html',
    controllerAs: 'model',
    controller: [ComponentController],
    bindings:{
      'manoeuvre':  '<',
      'isSynchro': '<',
      'onSelect': '&'
    }
  });

}());
