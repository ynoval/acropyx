(function () {
  'use strict';
  var acropyxApp = angular.module('acropyxApp');

  function ShowFlightCtrl($rootScope, $scope, $uibModal, $timeout, RunService,
                          CompetitionService, FlightService) {
    function getVote(judgeId, markTypeId) {
      var index = 0;
      while (index < $scope.flight.votes.length) {
        if ($scope.flight.votes[index].judge === judgeId &&
          $scope.flight.votes[index].markType === markTypeId) {
          return $scope.flight.votes[index].points;
        }
        index++;
      }
      return '';
    }

    function fillVotes() {
      //Update judge votes
      _.each($scope.judges, function (judge) {
        judge.votes = [];
        _.each($scope.markTypes, function (markType) {
          judge.votes.push({points: getVote(judge._id, markType._id), markTypeName: markType.name});
        });
      });
    }

    function fillResults() {
      $scope.flightResults = {};
      if ($scope.flight) {
        _.each($scope.flight.detailResults, function (result) {
          $scope.flightResults[result.markType] = result.value;
        });
      }
    }

    if ($scope.flight) {
      RunService.get($scope.flight.run).then(function (run) {
        $scope.run = run;
        CompetitionService.queryMarkTypes($scope.run.competition._id).then(function (markTypes) {
          $scope.markTypes = markTypes;
          $scope.judges = $scope.run.judges;
          fillVotes();
          fillResults();
        });
      });
    }

    $scope.$watch('flight', function () {
      fillVotes();
      fillResults();
    });

    $scope.addWarning = function () {
      $scope.isAddingWarning = true;
      FlightService.addWarning($scope.flight._id).then(function (savedFlight) {
        $scope.flight = savedFlight;
        $scope.isAddingWarning = false;
      });
    };
    $scope.removeWarning = function () {
      $scope.isRemovingWarning = true;
      FlightService.removeWarning($scope.flight._id).then(function (savedFlight) {
        $scope.flight = savedFlight;
        $scope.isRemovingWarning = false;
      });
    };

    $scope.openFlightVotes = function () {
      var flightVotesCtrl = function ($scope, $uibModalInstance, flightVotes, competitorName, judges, markTypes) {
        $scope.judges = judges;
        $scope.markTypes = markTypes;
        $scope.competitorName = competitorName;

        $scope.votePoints = [];
        for (var n = 0; n <= 20; n++) {
          $scope.votePoints.push({_id: n, value: n / 2});
        }

        $scope.votes = [];
        for (var i = 0; i < $scope.judges.length; i++) {
          for (var j = 0; j < $scope.markTypes.length; j++) {
            var index = 0;
            var found = false;
            while (index < flightVotes.length && !found) {
              if (flightVotes[index].judge === $scope.judges[i]._id &&
                flightVotes[index].markType === $scope.markTypes[j]._id) {
                $scope.votes[j + i * $scope.markTypes.length] = $scope.votePoints[flightVotes[index].points * 2];
                found = true;
              }
              index++;
            }
            if (!found) {
              $scope.votes[j + i * $scope.markTypes.length] = $scope.votePoints[0];
            }
          }
        }
        $scope.save = function () {
          var flightVotes = [];
          for (var i = 0; i < $scope.judges.length; i++) {
            for (var j = 0; j < $scope.markTypes.length; j++) {
              flightVotes.push({
                judge: $scope.judges[i]._id,
                markType: $scope.markTypes[j]._id,
                points: $scope.votes[j + i * $scope.markTypes.length].value
              });
            }
          }
          $uibModalInstance.close({flightVotes: flightVotes, action: 'Save'});
        };
        $scope.cancel = function () {
          $uibModalInstance.close({flightVotes: null, action: 'Cancel'});
        };
      };
      var modalInstance = $uibModal.open({
        templateUrl: 'app/manager/event/directives/showFlight/modals/modalVotes.html',
        controller: ['$scope', '$uibModalInstance', 'flightVotes', 'competitorName', 'judges', 'markTypes', flightVotesCtrl],
        windowClass: 'large-modal',
        keyboard: false,
        backdrop: 'static',
        resolve: {/* @ngInject */
          judges: function () {
            return $scope.judges;
          },/* @ngInject */
          markTypes: function () {
            return $scope.markTypes;
          },/* @ngInject */
          flightVotes: function () {
            return $scope.flight.votes;
          }, /* @ngInject */
          competitorName: function () {
            return $scope.flight.competitor.name;
          }
        }
      });
      modalInstance.result.then(function (data) {
        switch (data.action) {
          case 'Save':
          {
            FlightService.saveVotes($scope.flight._id, data.flightVotes).then(function (savedFlight) {
              console.log('Flight votes saved');
              $scope.flight = savedFlight;
              fillVotes();
              if (!$scope.flight.manoeuvres || $scope.flight.manoeuvres.length === 0) {
                $scope.openFlightManoeuvres();
              }
            });

            break;
          }
          case 'Cancel':
          {
            break;
          }
        }
      });
    };
    $scope.openFlightManoeuvres = function () {
      var flightManoeuvresCtrl = function ($scope, $uibModalInstance, manoeuvres, flightManoeuvres,
                                           CompetitionService, competitorName, competitionType, settingsId, SettingService) {
        $scope.flightManoeuvres = angular.copy(flightManoeuvres);
        $scope.competitorName = competitorName;
        $scope.manoeuvres = manoeuvres;

        function isNoMalus(manoeuvre) {
          var found = false;
          var index = 0;
          while (index < $scope.noMalusManoeuvresList.length && !found) {
            if ($scope.noMalusManoeuvresList[index]._id === manoeuvre._id) {
              found = true;
            }
            index++;
          }
          return found;
        }

        function init() {
          CompetitionService.queryType(competitionType).then(function (competitionType) {
            SettingService.get(settingsId).then(function (settings) {
              $scope.noMalusManoeuvresList = settings.withoutPenaltyManoeuvres;
              $scope.isSynchro = (competitionType.name === 'Synchro');
              $scope.noMalusManoeuvres = [];
              $scope.manoeuvreList = [];
              $scope.onlySynchroManoeuvres = [];
              manoeuvres = _.sortBy(manoeuvres, 'name');
              _.each(manoeuvres, function (manoeuvre) {

                if (isNoMalus(manoeuvre)) {
                  $scope.noMalusManoeuvres.push(manoeuvre);
                } else {
                  if (manoeuvre.onlySynchro) {
                    $scope.onlySynchroManoeuvres.push(manoeuvre);
                  } else {
                    $scope.manoeuvreList.push(manoeuvre);
                  }
                }
              });
            });
          });
        }

        init();

        $scope.addManoeuvre = function (selectedManoeuvre) {
          $scope.flightManoeuvres.push(selectedManoeuvre);
        };

        $scope.removeManoeuvre = function (manoeuvre) {
          _.remove($scope.flightManoeuvres, manoeuvre);
        };

        $scope.save = function () {
          $uibModalInstance.close({flightManoeuvres: $scope.flightManoeuvres, action: 'Save'});
        };
        $scope.cancel = function () {
          $uibModalInstance.close({flightManoeuvres: null, action: 'Cancel'});
        };
      };
      var modalInstance = $uibModal.open({
        templateUrl: 'app/manager/event/directives/showFlight/modals/modalManoeuvres.html',
        controller: [
          '$scope',
          '$uibModalInstance',
          'manoeuvres',
          'flightManoeuvres',
          'CompetitionService',
          'competitorName',
          'competitionType',
          'settingsId',
          'SettingService',
          flightManoeuvresCtrl
        ],
        windowClass: 'large-modal',
        keyboard: false,
        backdrop: 'static',
        resolve: {/* @ngInject */
          manoeuvres: function (ManoeuvreService) {
            return ManoeuvreService.query();
          },/* @ngInject */
          flightManoeuvres: function () {
            return $scope.flight.manoeuvres;
          },/* @ngInject */
          competitorName: function () {
            return $scope.flight.competitor.name;
          },/* @ngInject */
          competitionType: function (RunService) {
            return RunService.get($scope.flight.run).then(function (run) {
              return run.competition.type;
            });
          },/* @ngInject */
          settingsId: function (SeasonService) {
            return SeasonService.get($rootScope.activeSeason).then(function (season) {
              return season.settings;
            });
          }
        }
      });
      modalInstance.result.then(function (data) {
        switch (data.action) {
          case 'Save':
          {
            FlightService.saveManoeuvres($scope.flight._id, data.flightManoeuvres).then(function (savedFlight) {
              console.log('Flight manoeuvres saved');
              $scope.flight = savedFlight;
              if (!$scope.flight.votes || $scope.flight.votes.length === 0) {
                $scope.openFlightVotes();
              }
            });
            break;
          }
          case 'Cancel':
          {
            break;
          }
        }
      });
    };

    $scope.startFlight = function (showDisplay) {
      $scope.isStartingFlight = true;
      FlightService.startFlight($scope.flight._id, showDisplay).then(function (startedFlight) {
        $timeout(function () {
          $scope.isStartingFlight = false;
          $scope.flight = startedFlight;
        }, 2000);
      });
    };

    $scope.displayFlight = function () {
      $scope.isShowingFlight = true;
      FlightService.displayFlight($scope.flight._id).then(function () {
        $timeout(function () {
          $scope.isShowingFlight = false;
        }, 2000);
      });
    };

    $scope.resetFlight = function () {
      FlightService.reset($scope.flight._id).then(function (resetFlight) {
        console.log('Flight manoeuvres saved');
        $scope.flight = resetFlight;
      });
    };

    $scope.displayFlightResults = function () {
      $scope.isShowingFlightResults = true;
      FlightService.displayFlightResults($scope.flight._id).then(function () {
        $timeout(function () {
          $scope.isShowingFlightResults = false;
        }, 2000);
      });
    };

    $scope.endFlight = function (showDisplay) {
      $scope.isEndingFlight = true;
      FlightService.endFlight($scope.flight._id, showDisplay).then(function (endedFlight) {
        $timeout(function () {
          $scope.flight = endedFlight;
          $scope.isEndingFlight = false;
        }, 2000);
      });
    };

    $scope.getBonusType = function(bonus){
      var result = '';
      bonus.forEach(function(b){
        result += b + ' ';
      });
      return result;
    };

    $scope.getBonusPercent = function(flightManoeuvre){
      var bonus = 0;
      flightManoeuvre.bonusType.forEach(function(b){
        switch (b) {
          case 'Twisted': {
            bonus += flightManoeuvre.manoeuvre.twistedBonus;
            break;
          }
          case 'Flipped': {
            bonus += flightManoeuvre.manoeuvre.flippedBonus;
            break;
          }
          case 'Reversed': {
            bonus += flightManoeuvre.manoeuvre.reversedBonus;
            break;
          }
        }
      });
      return bonus;
    };

  }

  function ShowFlight() {
    return {
      restrict: 'E',
      scope: {
        'flight': '='
      },
      controller: [
        '$rootScope',
        '$scope',
        '$uibModal',
        '$timeout',
        'RunService',
        'CompetitionService',
        'FlightService',
        ShowFlightCtrl
      ],
      templateUrl: 'app/manager/event/directives/showFlight/showFlight.html'
    };
  }

  acropyxApp.directive('showFlight', [ShowFlight]);
})();
