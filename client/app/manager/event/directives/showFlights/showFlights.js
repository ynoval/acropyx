(function () {
  'use strict';
  var acropyxApp = angular.module('acropyxApp');

  function ShowFlightsCtrl($scope) {
    $scope.flights = _.sortBy($scope.flights, 'order');
    if ($scope.flights && $scope.flights.length > 0) {
      $scope.currentFlight = 0;
    }

    $scope.previousFlight = function () {
      if ($scope.currentFlight > 0) {
        $scope.currentFlight--;
      }
    };

    $scope.nextFlight = function () {
      if ($scope.currentFlight < $scope.flights.length - 1) {
        $scope.currentFlight++;
      }
    };

    $scope.getPreviousFlights = function(){
      if($scope.currentFlight > 0) {
        return $scope.flights.slice(0, $scope.currentFlight);
      }
      return [];
    };
    $scope.getNextFlights = function(){
      if($scope.currentFlight < $scope.flights.length - 1) {
        return $scope.flights.slice($scope.currentFlight+1);
      }
      return [];
    };

    $scope.setFlight = function(pos){
      $scope.currentFlight = $scope.currentFlight + pos;
    };
  }

  function ShowFlights() {
    return {
      restrict: 'E',
      scope: {
        'flights': '='
      },
      controller: ['$scope', ShowFlightsCtrl],
      templateUrl: 'app/manager/event/directives/showFlights/showFlights.html'
    };
  }

  acropyxApp.directive('showFlights', [ShowFlights]);
})();


