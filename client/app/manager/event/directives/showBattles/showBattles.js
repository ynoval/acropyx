(function(){
  'use strict';
  var acropyxApp = angular.module('acropyxApp');
  function ShowBattlesCtrl($scope, $timeout, BattleService){
    $scope.flights = _.sortBy($scope.flights, 'order');
    $scope.battles = [];
    var index = 0;
    $scope.info = {};
    $scope.info.middle = $scope.flights.length / 2;
    while(index < $scope.info.middle ){
      $scope.battles.push({
        firstFlight : $scope.flights[$scope.info.middle - (index + 1)],
        secondFlight: $scope.flights[$scope.info.middle + index]
      });
      index++;
    }


    if ($scope.battles && $scope.battles.length > 0) {
      $scope.info.currentBattle = 0;
    }

    $scope.previousBattle = function () {
      if ($scope.info.currentBattle > 0) {
        $scope.info.currentBattle--;
      }
    };

    $scope.nextBattle = function () {
      if ($scope.info.currentBattle < $scope.battles.length - 1) {
        $scope.info.currentBattle++;
      }
    };

    $scope.showBattle = function(){
      $scope.info.isShowingBattle = true;
      BattleService.display($scope.battles[$scope.info.currentBattle].firstFlight._id, $scope.battles[$scope.info.currentBattle].secondFlight._id).then(function(){
        $timeout(function() {
          $scope.info.isShowingBattle = false;
        }, 2000);
      });
    };
    function showBattleResults(firstFlightId, secondFlightId){
      $scope.info.isShowingBattleResults = true;
      BattleService.displayResults(firstFlightId, secondFlightId).then(function(){
        $timeout(function() {
          $scope.info.isShowingBattleResults = false;
        }, 2000);
      });
    };
    $scope.endBattle = function(showResults){
      var firstFlight = $scope.battles[$scope.info.currentBattle].firstFlight;
      var secondFlight = $scope.battles[$scope.info.currentBattle].secondFlight;
      //update Flights
      var firstIndex = -1;
      var secondIndex = -1;
      var index = 0;
      while(firstIndex === -1 || secondIndex === -1){
        if($scope.flights[index]._id === firstFlight._id){
            firstIndex = index;
        } else {
          if ($scope.flights[index]._id === secondFlight._id) {
            secondIndex = index;
          }
        }
        index++;
      }
      $scope.flights[firstIndex] = firstFlight;
      $scope.flights[secondIndex] = secondFlight;

      if(showResults){
        showBattleResults(firstFlight._id, secondFlight._id);
      }
    };
  }
  function ShowBattles(){
    return {
      restrict: 'E',
      scope: {
        'flights': '='
      },
      controller: ['$scope', '$timeout', 'BattleService', ShowBattlesCtrl],
      templateUrl: 'app/manager/event/directives/showBattles/showBattles.html'
    };
  }
  acropyxApp.directive('showBattles',[ShowBattles]);
})();
