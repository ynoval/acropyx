(function(){
  'use strict';
  var acropyxApp = angular.module('acropyxApp');
  function ShowCompetitionCtrl($scope, $uibModal, $timeout, RunService, CompetitionService){
    $scope.flightsEnded = function(){
      if (!$scope.competition.activeRun || !$scope.competition.activeRun.flights){
        return false;
      }
      var index = 0;
      while (index < $scope.competition.activeRun.flights.length && $scope.competition.activeRun.flights[index].endTime){
        index++;
      }
      return index >= $scope.competition.activeRun.flights.length;      
    };
    $scope.startRun = function(display){
      if($scope.competition.activeRun){
        $scope.isStartingRun = true;
        RunService.startRun($scope.competition.activeRun._id, display).then(function(savedRun){
          $timeout(function() {
            $scope.competition.activeRun.startTime = savedRun.startTime;
            $scope.isStartingRun = false;
          }, 2000);
        });
      }
    };
    $scope.endRun = function(display){
      if($scope.competition.activeRun){
        $scope.isEndingRun = true;
        RunService.endRun($scope.competition.activeRun._id, display).then(function(savedRun){
          $timeout(function() {
            $scope.competition.activeRun.endTime = savedRun.endTime;
            $scope.isEndingRun = false;
          }, 2000);
        });
      }
    };
    $scope.showRunStartingOrder = function(){
      if($scope.competition.activeRun){
        $scope.isShowingStartingOrder = true;
        RunService.displayStartingOrder($scope.competition.activeRun._id).then(function(){
          $timeout(function() {
            $scope.isShowingStartingOrder = false;
          }, 2000);
        });
      }
    };
    $scope.showRunResults = function(){
      if($scope.competition.activeRun){
        if($scope.competition.activeRun.type.name === 'Battle'){
          $scope.showRunBattlesResults();
        } else {
          $scope.isShowingRunResults = true;
          RunService.displayResults($scope.competition.activeRun._id).then(function(){
            $timeout(function() {
              $scope.isShowingRunResults = false;
            }, 2000);
          });
        }
      }
    };

    $scope.showRunBattlesResults = function(){
      if($scope.competition.activeRun){
        $scope.isShowingRunResults = true;
        RunService.displayBattleResults($scope.competition.activeRun._id).then(function(){
          $timeout(function() {
            $scope.isShowingRunResults = false;
          }, 2000);
        });
      }
    };
    $scope.openRunResults = function(){
      var ShowRunResultsCtrl = function ($scope, $window, $uibModalInstance, RunService, runInstance) {
        $scope.runInstance = runInstance;

        function organizeFlights(flights){
          $scope.runInstance.flights = _.sortBy(flights, 'order');
          if($scope.runInstance.type.name === 'Battle'){
            $scope.battles = [];
            var index = 0;
            //TODO: Only accept Battles for runs with even competitors amount
            var middle = $scope.runInstance.flights.length / 2;
            while(index < middle ){
              $scope.battles.push({
                firstFlight : $scope.runInstance.flights[middle - (index + 1)],
                secondFlight: $scope.runInstance.flights[middle + index]
              });
              index++;
            }
          }
        }
        function loadFlights(){
          $scope.isLoadingFlights = true;
          RunService.queryFlights($scope.runInstance._id).then(function(flights){
            organizeFlights(flights);
            $scope.isLoadingFlights = false;
          });
        }

        $scope.runResults = function(){
          var runFlightResults = [];
          $scope.runInstance.flights.forEach(function(flight){
            if (flight.endTime){
              runFlightResults.push(flight);
            }
          });
          return _.sortBy(runFlightResults, 'computeResult').reverse();
        };


        loadFlights();

        $scope.generateRunResultsPDF = function() {
          RunService.generateResultsPDF($scope.runInstance._id).then(function(){
            var url = 'pdf/' + $scope.runInstance.code + '_Results.pdf';
            $window.open(url);
          });
        };

        $scope.close = function(){
          $uibModalInstance.close();
        };
      };
      $uibModal.open({
        templateUrl: 'app/manager/dashboard/modals/modalRunResults.html',
        controller: ['$scope', '$window', '$uibModalInstance', 'RunService', 'runInstance', ShowRunResultsCtrl],
        windowClass: 'large-modal',
        keyboard: false,
        backdrop: 'static',
        resolve:{  /* @ngInject */
          runInstance : function(){
            return $scope.competition.activeRun;//RunService.get(run._id);
          }
        }
      });
    };
    $scope.openRunStartingOrder = function(){
      var ShowRunStartingOrderCtrl = function ($scope, $window, $uibModalInstance, RunService, runInstance) {
        $scope.runInstance = runInstance;

        $scope.isLoadingFlights = true;
        RunService.queryFlights($scope.runInstance._id).then(function(flights){
          $scope.runInstance.flights = _.sortBy(flights, 'order');
          if($scope.runInstance.type.name === 'Battle'){
            $scope.battles = [];
            var index = 0;
            //TODO: Only accept Battles in runs with even competitors amount
            var middle = $scope.runInstance.flights.length / 2;
            while(index < middle ){
              $scope.battles.push({
                firstFlight : $scope.runInstance.flights[middle - (index + 1)],
                secondFlight: $scope.runInstance.flights[middle + index]
              });
              index++;
            }
          }
          $scope.isLoadingFlights = false;
        });
        $scope.generateRunStartingOrderPDF = function() {
          RunService.generateStartingOrderPDF($scope.runInstance._id).then(function(){
            var url = 'pdf/' + $scope.runInstance.code + '_StartingOrder.pdf';
            $window.open(url);
          });
        };

        $scope.close = function(){
          $uibModalInstance.close();
        };
      };
      $uibModal.open({
        templateUrl: 'app/manager/dashboard/modals/modalRunStartingOrder.html',
        controller: [
          '$scope', 
          '$window', 
          '$uibModalInstance', 
          'RunService', 
          'runInstance', 
          ShowRunStartingOrderCtrl
        ],
        keyboard: false,
        backdrop: 'static',
        resolve:{ /* @ngInject */
          runInstance : function(){
            return $scope.competition.activeRun;
          }
        }
      });
    };

    $scope.showCompetitionResults = function(){
      if($scope.competition){
        $scope.isShowingCompetitionResults = true;
        CompetitionService.displayResults($scope.competition._id).then(function(){
          $timeout(function() {
            $scope.isShowingCompetitionResults = false;
          }, 2000);
        });
      }
    };
    $scope.openCompetitionResults = function(){
      var ShowCompetitionResultsCtrl = function ($scope, $window, $uibModalInstance, competitionInstance, CompetitionService) {
        $scope.competitionInstance = competitionInstance;
        function loadResults(){
          if($scope.competitionInstance.startTime){
            $scope.isLoadingResults = true;
            CompetitionService.queryResults($scope.competitionInstance._id).then(function(results){
              $scope.competitionResults = results;
              $scope.isLoadingResults = false;
            });
          }
        }
        loadResults();

        $scope.generateCompetitionResultsPDF = function() {
          CompetitionService.generateResultsReportPDF($scope.competitionInstance._id).then(function(){
            var url = 'pdf/' + $scope.competitionInstance.code + '_Results.pdf';
            $window.open(url);
          });
        };

        $scope.close = function(){
          $uibModalInstance.close();
        };
      };
      $uibModal.open({
        templateUrl: 'app/manager/dashboard/modals/modalCompetitionResults.html',
        controller: [
          '$scope',
          '$window',
          '$uibModalInstance',
          'competitionInstance',
          'CompetitionService',
          ShowCompetitionResultsCtrl
        ],
        windowClass: 'large-modal',
        keyboard: false,
        backdrop: 'static',
        resolve:{ /* @ngInject */
          competitionInstance : function(){
            return $scope.competition;
          }
        }
      });
    };
    $scope.endCompetition = function(display){
      if($scope.competition){
        $scope.isEndingCompetition = true;
        CompetitionService.endCompetition($scope.competition._id, display).then(function(savedCompetition){
          $timeout(function() {
            $scope.competition.endTime = savedCompetition.endTime;
            $scope.isEndingCompetition = false;
          }, 2000);
        });
      }
    };
  }
  function ShowCompetition() {
    return {
      restrict: 'E',
      scope: {
        'competition': '='
      },
      controller: [
        '$scope',
        '$uibModal',
        '$timeout',
        'RunService',
        'CompetitionService',
        ShowCompetitionCtrl
      ],
      templateUrl: 'app/manager/event/directives/showCompetition/showCompetition.html'
    };
  }
  acropyxApp.directive('showCompetition',[ShowCompetition]);
})();
