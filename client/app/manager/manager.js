(function () {
  'use strict';
  var acropyxApp = angular.module('acropyxApp');

  function Config($stateProvider) {
    $stateProvider
      .state('managerDashboard', {
        url: '/manager',
        templateUrl: 'app/manager/dashboard/managerDashboard.html',
        controller: 'ManagerDashboardCtrl',
        authenticate: true,
        resolve: { /* @ngInject */
          seasons: function (SeasonService) {
            return SeasonService.query();
          }
        }
      })
      .state('managerEvent', {
        url: '/manager/event/:eventId',
        templateUrl: 'app/manager/event/managerEvent.html',
        controller: 'ManagerEventCtrl',
        authenticate: true,
        resolve: {/* @ngInject */
          eventInstance: function ($stateParams, EventService) {
            return EventService.get($stateParams.eventId);
          },/* @ngInject */
          competitions: function ($stateParams, EventService) {
            return EventService.queryActiveCompetitions($stateParams.eventId);
          }
        }
      });
  }

  acropyxApp.config(['$stateProvider', Config]);
})();
