(function(){
  'use strict';
  var acropyxApp = angular.module('acropyxApp');
  function ManagerDashboardCtrl($scope,  $state, $uibModal, $timeout, seasons, SeasonService, EventService,
                                RunService, CompetitionService){


    function init(){
      $scope.seasons = seasons;
      //    populateSeasons();
      console.log('Finish Populating seasons');
    }
    init();

    /* Season */
    $scope.openSeason = function(season){
      if(!season.populatedEvents){
        SeasonService.queryEvents(season._id).then(function(events){
          season.events = events;
          season.populatedEvents = true;
        });
      }
    };
    $scope.showSeasonRankings = function(season){
      $scope.isShowingSeasonRanking = true;
      SeasonService.showRankings(season._id).then(function(){
        $timeout(function() {
          $scope.isShowingSeasonRanking = false;
        }, 2000);
      });
    };

    $scope.openSeasonRankings = function(season){
      var ShowSeasonRankingsCtrl = function ($scope, $uibModalInstance, SeasonService, seasonInstance) {

        $scope.seasonInstance = seasonInstance;
        function loadRankings(){
          $scope.isLoadingSoloRanking = true;
          SeasonService.queryRankingSolo($scope.seasonInstance._id).then(function(ranking){
            $scope.seasonInstance.soloRanking = ranking;
            $scope.isLoadingSoloRanking = false;
          });
          $scope.isLoadingSynchroRanking = true;
          SeasonService.queryRankingSynchro($scope.seasonInstance._id).then(function(ranking){
            $scope.seasonInstance.synchroRanking = ranking;
            $scope.isLoadingSynchroRanking = false;
          });
        }
        loadRankings();
        $scope.close = function(){
          $uibModalInstance.close();
        };
      };

      $uibModal.open({
        templateUrl: 'app/manager/dashboard/modals/modalSeasonRankings.html',
        controller: ['$scope', '$uibModalInstance', 'SeasonService', 'seasonInstance', ShowSeasonRankingsCtrl],
        windowClass: 'large-modal',
        keyboard: false,
        backdrop: 'static',
        resolve:{/* @ngInject */
          seasonInstance : function(){
            return season;
          }
        }
      });
    };

    /*Event*/
    function populateCompetitions(event) {
      EventService.queryCompetitions(event._id).then(function (competitions) {
        event.competitions = [];
        competitions.forEach(function (competition) {
          CompetitionService.queryRuns(competition._id).then(function (runs) {
            competition.nextRun = 1;
            _.each(runs, function(run){
              if(run.order >= competition.nextRun && run.endTime){
                competition.nextRun = run.order + 1;
              }
            });
            competition.runs = runs;
            event.competitions.push(competition);
          });
        });
        event.populatedCompetitions = true;
      });
    }

    $scope.openEvent = function(event){
      if (!event.populatedCompetitions) {
        populateCompetitions(event);
      }
    };
    $scope.startEvent = function(event){
      event.isStarting = true;
      EventService.startEvent(event._id).then(function(){
        if (!event.populatedCompetitions) {
          populateCompetitions(event);
        }
        $timeout(function() {
          event.isStarting = false;
          event.isStarted = true;
          event.isEnded = false;
        }, 2000);
      });
    };
    $scope.endEvent = function(event){
      event.isEnding = true;
      EventService.endEvent(event._id).then(function(){
        //TODO: Notifications
        $timeout(function() {
          event.isEnding = false;
          event.isStarted = true;
          event.isEnded = true;
        }, 2000);
      });
    };

    $scope.openEventResults = function(event){
      var ShowEventResultsCtrl = function ($scope, $uibModalInstance, eventInstance, competitions, CompetitionService) {
        $scope.eventInstance = eventInstance;
        $scope.eventInstance.filledCompetitions = [];

        function loadResults(){
          competitions.forEach(function(competition){
            CompetitionService.queryResults(competition._id).then(function(results){
              competition.compResults = results;
              $scope.eventInstance.filledCompetitions.push(competition);
            });
          });
        }

        loadResults();


        $scope.close = function(){
          $uibModalInstance.close();
        };
      };
      $uibModal.open({
        templateUrl: 'app/manager/dashboard/modals/modalEventResults.html',
        controller: ['$scope', '$uibModalInstance', 'eventInstance', 'competitions', 'CompetitionService', ShowEventResultsCtrl],
        windowClass: 'large-modal',
        keyboard: false,
        backdrop: 'static',
        resolve:{/* @ngInject */
          eventInstance : function(){
            return event;
          }, /* @ngInject */
          competitions: function(EventService){
            return EventService.queryCompetitions(event._id);
          }
        }
      });
    };
    $scope.showEventResults = function(event){
      $scope.isShowingEventResults = true;
      EventService.displayResults(event._id).then(function(){
        $timeout(function() {
          $scope.isShowingEventResults = false;
        }, 2000);
      });
    };
    $scope.openEventResults = function(){
      var ShowEventResultsCtrl = function ($scope, $uibModalInstance, eventInstance, competitions, CompetitionService) {
        $scope.eventInstance = eventInstance;
        $scope.eventInstance.filledCompetitions = [];

        function loadResults(){
          competitions.forEach(function(competition){
            CompetitionService.queryResults(competition._id).then(function(results){
              competition.compResults = results;
              $scope.eventInstance.filledCompetitions.push(competition);
            });
          });
        }

        loadResults();


        $scope.close = function(){
          $uibModalInstance.close();
        };
      };
      $uibModal.open({
        templateUrl: 'app/manager/dashboard/modals/modalEventResults.html',
        controller: [
          '$scope',
          '$uibModalInstance',
          'eventInstance',
          'competitions',
          'CompetitionService',
          ShowEventResultsCtrl
        ],
        windowClass: 'large-modal',
        keyboard: false,
        backdrop: 'static',
        resolve:{/* @ngInject */
          eventInstance : function(){
            return $scope.eventInstance;
          }, /* @ngInject */
          competitions: function(EventService){
            return EventService.queryCompetitions($scope.eventInstance._id);
          }
        }
      });
    };

    /*Competition*/
    $scope.startCompetition = function(competition){
      competition.isStarting = true;
      CompetitionService.startCompetition(competition._id).then(function(startedCompetition){
        $timeout(function() {
          competition.isStarting = false;
          competition.startTime = startedCompetition.startTime;
          competition.nextRun = 1;
        }, 2000);
      });
    };
    $scope.endCompetition = function(competition){
      competition.isEnding = true;
      CompetitionService.endCompetition(competition._id).then(function(endedCompetition){
        $timeout(function() {
          competition.isEnding = false;
          competition.endTime = endedCompetition.endTime;
          competition.nextRun = -1;
        }, 2000);
      });
    };
    $scope.openCompetitionResults = function(competition){
      var ShowCompetitionResultsCtrl = function ($scope, $window, $http, $uibModalInstance, competitionInstance, CompetitionService) {

        $scope.competitionInstance = competitionInstance;
        function loadResults(){
          if($scope.competitionInstance.startTime){
            $scope.isLoadingResults = true;
            CompetitionService.queryResults($scope.competitionInstance._id).then(function(results){
              $scope.competitionResults = results;
              $scope.isLoadingResults = false;
            });
          }
        }
        loadResults();

        $scope.generateCompetitionResultsPDF = function() {
          CompetitionService.generateResultsReportPDF($scope.competitionInstance._id).then(function(){
            // var text = '';
            // if($scope.competitionInstance.endTime){
            //   text = 'Standings_Final';
            // } else{
            //   text = 'Standings_after_run_' + $scope.competitionInstance.nextRun - 1;
            // }
            // var url = 'pdf/' + $scope.competitionInstance.code + '_' + text + '.pdf';
            var url = 'pdf/' + competition.code + '_Results.pdf';
            $window.open(url);
          });
        };

        $scope.generateCompetitionResultsCSV = function() {
          CompetitionService.generateResultsReportCSV($scope.competitionInstance._id).then(function(){
            var url = 'csv/' + $scope.competitionInstance.code + '_Results.pdf';
            $window.open(url);
          });
        };

        $scope.close = function(){
          $uibModalInstance.close();
        };
      };
      $uibModal.open({
        templateUrl: 'app/manager/dashboard/modals/modalCompetitionResults.html',
        controller: [
          '$scope',
          '$window',
          '$http',
          '$uibModalInstance',
          'competitionInstance',
          'CompetitionService',
          ShowCompetitionResultsCtrl
        ],
        windowClass: 'large-modal',
        keyboard: false,
        backdrop: 'static',
        resolve:{/* @ngInject */
          competitionInstance : function(){
            return competition;
          }
        }
      });
    };


    /*Run*/
    $scope.startRun = function(run){
      var StartRunConfirmCtrl = function ($scope, $uibModalInstance, runInstance, flights) {
        $scope.runInstance = runInstance;

        function organizeFlights(flights){
          $scope.runInstance.flights = _.sortBy(flights, 'order');
          if($scope.runInstance.type.name === 'Battle'){
            $scope.battles = [];
            var index = 0;
            //TODO: Only accept Battles for runs with even competitors amount
            var middle = $scope.runInstance.flights.length / 2;
            while(index < middle ){
              $scope.battles.push({
                firstFlight : $scope.runInstance.flights[middle - (index + 1)],
                secondFlight: $scope.runInstance.flights[middle + index]
              });
              index++;
            }
          }
        }

        organizeFlights(flights);
        $scope.close = function(){
          $uibModalInstance.close({action: 'CANCEL'});
        };
        $scope.startRun = function(){
          $uibModalInstance.close({action: 'START'});
        };
        $scope.editRun = function(){
          $uibModalInstance.close({action: 'EDIT'});
        };
      };
      //Show Confirm Dialog
      var modalInstance = $uibModal.open({
        templateUrl: 'app/manager/dashboard/modals/modalStartRunConfirm.html',
        controller: [
          '$scope',
          '$uibModalInstance',
          'runInstance',
          'flights',
          StartRunConfirmCtrl
        ],
        windowClass: 'medium-modal',
        keyboard: true,
        backdrop: 'static',
        resolve:{/* @ngInject */
          runInstance : function(RunService){
            return RunService.get(run._id);
          },/* @ngInject */
          flights: function(RunService){
            return RunService.queryFlights(run._id);
          }
        }
      });

      modalInstance.result.then(function (data) {
        switch (data.action) {
          case 'START':
          {
            run.isStarting = true;
            RunService.startRun(run._id).then(function(startedRun){
              $timeout(function() {
                run.isStarting = false;
                run.startTime = startedRun.startTime;
              }, 2000);
            });
            break;
          }
          case 'EDIT':
          {
            $state.go('adminRun', {runId: run._id});
            break;
          }
        }
      });
    };
    $scope.endRun = function(competition, run){
      run.isEnding = true;
      RunService.endRun(run._id).then(function(endedRun){
        $timeout(function() {
          run.isEnding = false;
          run.endTime = endedRun.endTime;
          competition.nextRun = run.order + 1;
        }, 2000);
      });
    };
    $scope.openRunResults = function(run){
      var ShowRunResultsCtrl = function ($scope, $window, $uibModalInstance, RunService, runInstance) {

        $scope.runInstance = runInstance;

        function organizeFlights(flights){
          $scope.runInstance.flights = _.sortBy(flights, 'order');
          if($scope.runInstance.type.name === 'Battle'){
            $scope.battles = [];
            var index = 0;
            //TODO: Only accept Battles for runs with even competitors amount
            var middle = $scope.runInstance.flights.length / 2;
            while(index < middle ){
              $scope.battles.push({
                firstFlight : $scope.runInstance.flights[middle - (index + 1)],
                secondFlight: $scope.runInstance.flights[middle + index]
              });
              index++;
            }
          }
        }
        function loadFlights(){
          $scope.isLoadingFlights = true;
          RunService.queryFlights($scope.runInstance._id).then(function(flights){
            organizeFlights(flights);
            $scope.isLoadingFlights = false;
          });
        }

        $scope.runResults = function(){
          var runFlightResults = [];
          if($scope.runInstance.flights && $scope.runInstance.flights.length > 0){
            $scope.runInstance.flights.forEach(function(flight){
              if (flight.endTime){
                runFlightResults.push(flight);
              }
            });
          }
          return _.sortBy(runFlightResults, 'computeResult').reverse();
        };


        loadFlights();

        $scope.generateRunResultsPDF = function() {
          RunService.generateResultsPDF($scope.runInstance._id).then(function(){
            var url = 'pdf/' + $scope.runInstance.code + '_Results.pdf';
            $window.open(url);
          });
        };


        $scope.close = function(){
          $uibModalInstance.close();
        };
      };
      $uibModal.open({
        templateUrl: 'app/manager/dashboard/modals/modalRunResults.html',
        controller: [
          '$scope',
          '$window',
          '$uibModalInstance',
          'RunService',
          'runInstance',
          ShowRunResultsCtrl
        ],
        windowClass: 'large-modal',
        keyboard: false,
        backdrop: 'static',
        resolve:{/* @ngInject */
          runInstance : function(){
            return RunService.get(run._id);
          }
        }
      });
    };
    $scope.openRunStartingOrder = function(run){
      var ShowRunStartingOrderCtrl = function ($scope, $window, $uibModalInstance, RunService, runInstance) {
        $scope.runInstance = runInstance;

        $scope.isLoadingFlights = true;
        RunService.queryFlights($scope.runInstance._id).then(function(flights){
          $scope.runInstance.flights = _.sortBy(flights, 'order');
          if($scope.runInstance.type.name === 'Battle'){
            $scope.battles = [];
            var index = 0;
            //TODO: Only accept Battles in runs with even competitors amount
            var middle = $scope.runInstance.flights.length / 2;
            while(index < middle ){
              $scope.battles.push({
                firstFlight : $scope.runInstance.flights[middle - (index + 1)],
                secondFlight: $scope.runInstance.flights[middle + index]
              });
              index++;
            }
          }
          $scope.isLoadingFlights = false;
        });

        $scope.generateRunStartingOrderPDF = function() {
          RunService.generateStartingOrderPDF($scope.runInstance._id).then(function(){
            var url = 'pdf/' + $scope.runInstance.code + '_StartingOrder.pdf';
            $window.open(url);
          });
        };

        $scope.close = function(){
          $uibModalInstance.close();
        };
      };
      $uibModal.open({
        templateUrl: 'app/manager/dashboard/modals/modalRunStartingOrder.html',
        controller: [
          '$scope',
          '$window',
          '$uibModalInstance',
          'RunService',
          'runInstance',
          ShowRunStartingOrderCtrl
        ],
        keyboard: false,
        backdrop: 'static',
        resolve:{/* @ngInject */
          runInstance : function(){
            return run;
          }
        }
      });
    };
    $scope.openRunManoeuvres = function(run){
      var ShowRunManoeuvresCtrl = function ($scope, $window, $uibModalInstance, RunService, runInstance, flights) {
        $scope.runInstance = runInstance;
        $scope.runInstance.flights = _.sortBy(flights, 'order');

        $scope.generateRunManoeuvresPDF = function() {
          RunService.generateManoeuvresPDF($scope.runInstance._id).then(function(){
            var url = 'pdf/' + $scope.runInstance.code + '_Manoeuvres.pdf';
            $window.open(url);
          });
        };

        $scope.getBonusType = function(bonus){
          var result = '';
          bonus.forEach(function(b){
            result += b + ' ';
          });
          return result;
        };

        $scope.close = function(){
          $uibModalInstance.close();
        };
      };
      $uibModal.open({
        templateUrl: 'app/manager/dashboard/modals/modalRunManoeuvres.html',
        controller: [
          '$scope',
          '$window',
          '$uibModalInstance',
          'RunService',
          'runInstance',
          'flights',
          ShowRunManoeuvresCtrl
        ],
        windowClass: 'large-modal',
        keyboard: false,
        backdrop: 'static',
        resolve:{/* @ngInject */
          runInstance : function(){
            return RunService.get(run._id);
          }, /* @ngInject */
          flights: function(){
            return RunService.queryFlights(run._id);
          }
        }
      });
    };
    $scope.viewRun = function(event){
      $state.go('managerEvent', {eventId: event._id});
    };

    $scope.isRunReady = function(competition, run){
      if(!competition.startTime || competition.endTime) {
        return false;
      }
      //First Run in started and not ended competition
      if( run.order === 1 ){
        return true;
      }
      //All previous run are started and ended
      competition.runs.forEach(function(currentRun){
        if (currentRun.order < run.order){
          if (!currentRun.startTime || !currentRun.endTime){
            return false;
          }
        }
      });
      return true;
    };

    $scope.status = {
      isFirstOpen: true,
      isFirstDisabled: false
    };
  }
  acropyxApp.controller('ManagerDashboardCtrl', [
    '$scope',
    '$state',
    '$uibModal',
    '$timeout',
    'seasons',
    'SeasonService',
    'EventService',
    'RunService',
    'CompetitionService',
    ManagerDashboardCtrl
  ]);
})();
