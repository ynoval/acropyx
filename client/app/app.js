(function(){
  'use strict';
  var acropyxApp = angular.module('acropyxApp', [
    'ngCookies',
    'ngResource',
    'ngSanitize',
    'btford.socket-io',
    'ui.router',
    'ngFileUpload',
    'ui.bootstrap',
    'ui.select',
    'angular-ladda',
    'notifications',
    'yndComponents.confirmAction',
    'yndComponents.groupItems',
    'yndComponents.radioButtonGroup'
  ]);
  function Config($urlRouterProvider, $locationProvider, $httpProvider){
    $urlRouterProvider
      .otherwise('/');

    $locationProvider.html5Mode(true);
    $httpProvider.interceptors.push('authInterceptor');
  }
  acropyxApp.config(['$urlRouterProvider', '$locationProvider', '$httpProvider', Config]);

  function AuthInterceptor($q, $cookieStore, $location){
    return {
      // Add authorization token to headers
      request: function (config) {
        config.headers = config.headers || {};
        if ($cookieStore.get('token')) {
          config.headers.Authorization = 'Bearer ' + $cookieStore.get('token');
        }
        return config;
      },
      // Intercept 401s and redirect you to login
      responseError: function(response) {
        if(response.status === 401) {
          $location.path('/login');
          // remove any stale tokens
          $cookieStore.remove('token');
          return $q.reject(response);
        }
        else {
          return $q.reject(response);
        }
      }
    };
  }
  acropyxApp.factory('authInterceptor', ['$q', '$cookieStore', '$location', AuthInterceptor]);

  function RunApp($rootScope, $location, Auth) {
    // Redirect to login if route requires auth and you're not logged in
    $rootScope.$on('$stateChangeStart', function (event, next) {
      Auth.isLoggedInAsync(function(loggedIn) {
        if (next.authenticate && !loggedIn) {
          $location.path('/login');
        }
      });
    });
  }
  acropyxApp.run(['$rootScope', '$location', 'Auth', RunApp]);
})();

