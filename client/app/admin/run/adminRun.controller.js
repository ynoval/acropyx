(function(){
  'use strict';
  var acropyxApp = angular.module('acropyxApp');
  function AdminRunCtrl($scope, $window, $state, runInstance, RunService,
                        FlightService, runTypes, judges, UtilsService){

    $scope.runInstance = runInstance;
    $scope.runTypes = runTypes;
    var originalRunInstance = angular.copy(runInstance);

    function organizeFlights(flights){
      $scope.runInstance.flights = _.sortBy(flights, 'order');
      if($scope.runInstance.typeName === 'Battle'){
        $scope.battles = [];
        var index = 0;
        //TODO: Only accept Battles for runs with even competitors amount
        var middle = $scope.runInstance.flights.length / 2;
        while(index < middle ){
          $scope.battles.push({
            firstFlight : $scope.runInstance.flights[middle - (index + 1)],
            secondFlight: $scope.runInstance.flights[middle + index]
          });
          index++;
        }
      }
    }
    function loadFlights(){
      $scope.isLoadingFlights = true;
      RunService.queryFlights($scope.runInstance._id).then(function(flights){
        organizeFlights(flights);
        $scope.isLoadingFlights = false;
      });
    }
    function init() {
      $scope.runInstance.typeName = $scope.runInstance.type.name;
      $scope.runInstance.type = _.result(_.find(runTypes, function(run) {
        return run.name === originalRunInstance.type.name;
      }), '_id');

      $scope.idPropertyType = '_id';
      $scope.namePropertyType = 'name';
      $scope.bootstrapSuffix = 'default';

      $scope.penaltyOptions = [{value: true, label: 'Yes'}, {value: false, label: 'No'}];
      $scope.idProperty = 'value';
      $scope.nameProperty = 'label';

      $scope.medalsOptions = [{value: true, label: 'Yes'}, {value: false, label: 'No'}];

      $scope.availableJudges = UtilsService.difference(judges, $scope.runInstance.judges);

      loadFlights();

    }
    init();

    $scope.runResults = function(){
      var runFlightResults = [];
      if( $scope.runInstance.flights){
        $scope.runInstance.flights.forEach(function(flight){
          if (flight.endTime){
            runFlightResults.push(flight);
          }
        });
        return _.sortBy(runFlightResults, 'computeResult').reverse();
      }
    };
    $scope.resetRun = function(){
      RunService.reset(runInstance._id).then(function(){
        $state.go('adminDashboard');
      });
    };
    $scope.reopenRun = function(){
      RunService.reopen(runInstance._id).then(function(){
        $state.go('adminDashboard');
      });
    };
    $scope.discardChanges = function(){
      $scope.runInstance =  angular.copy(originalRunInstance );
      init();
    };
    $scope.deleteRun = function(){
      RunService.delete(runInstance._id).success(function(){
        console.log(runInstance.name + ' removed');
        $state.go('adminDashboard');
      });
    };
    $scope.changeRun = function(){
      delete runInstance.flights;
      delete runInstance.qualification;

      RunService.save(runInstance);
      $state.go('adminDashboard');
    };
    $scope.viewCompetition = function(){
      $state.go('adminCompetition', {competitionId: $scope.runInstance.competition._id});
    };
    $scope.viewRun = function(run){
      $state.go('adminRun', {runId: run._id});
    };
    $scope.generateStartingOrder = function(regenerate){
      console.log('generate starting order');
      RunService.generateStartingOrder($scope.runInstance._id, regenerate).then(function(){
        loadFlights();
      });
    };
    $scope.downFlight = function(flight) {
      if (flight.order < $scope.runInstance.flights.length) {
        $scope.isLoadingFlights = true;
        FlightService.changeOrder(flight._id, flight.order + 1).then(function () {
          //TODO: FIX!!
          loadFlights();
          $scope.isLoadingFlights = false;
        });
      }
    };
    $scope.upFlight = function(flight){
      if(flight.order > 0){
        $scope.isLoadingFlights = true;
        FlightService.changeOrder(flight._id, flight.order - 1).then(function(){
          //TODO: FIX!!
          loadFlights();
          $scope.isLoadingFlights = false;
        });
      }
    };
    $scope.downBattle = function(battle) {
      if(battle && battle.firstFlight.order > 0){
        $scope.isLoadingFlights = true;
        FlightService.changeOrder(battle.firstFlight._id, battle.firstFlight.order - 1).then(function(){
          FlightService.changeOrder(battle.secondFlight._id, battle.secondFlight.order + 1).then(function() {
            //TODO: FIX!!
            loadFlights();
            $scope.isLoadingFlights = false;
          });
        });
      }
    };
    $scope.upBattle = function(battle){
      if(battle && battle.firstFlight.order  + 1 !== battle.secondFlight.order){
        $scope.isLoadingFlights = true;
        FlightService.changeOrder(battle.firstFlight._id, battle.firstFlight.order + 1).then(function(){
          FlightService.changeOrder(battle.secondFlight._id, battle.secondFlight.order -1).then(function() {
            //TODO: FIX!!
            loadFlights();
            $scope.isLoadingFlights = false;
          });
        });
      }
    };
    $scope.generateRunResultsPDF = function() {
      RunService.generateResultsPDF($scope.runInstance._id).then(function(){
        var url = 'pdf/' + $scope.runInstance.code + '_Results.pdf';
        $window.open(url);
      });
    };
  }
  acropyxApp.controller('AdminRunCtrl',[
    '$scope',
    '$window',
    '$state',
    'runInstance',
    'RunService',
    'FlightService',
    'runTypes',
    'judges',
    'UtilsService',
    AdminRunCtrl
  ]);
})();
