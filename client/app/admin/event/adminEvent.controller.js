(function(){
  'use strict';
  var acropyxApp = angular.module('acropyxApp');
  function AdminEventCtrl($scope, $state, $timeout, $uibModal, Upload,
                          eventInstance, EventService, sponsors, CompetitionService){

    $scope.eventInstance = eventInstance;
    var originalEventInstance = angular.copy($scope.eventInstance);
    $scope.eventLogo= {};

    function loadCompetitions(){
      $scope.isLoadingCompetitions = true;
      EventService.queryCompetitions($scope.eventInstance._id).then(function(competitions){
        $scope.eventInstance.competitions = competitions;
        $scope.isLoadingCompetitions = false;
      });
    }

    function init(){

      $scope.logoUrl = '/api/events/' + $scope.eventInstance._id + '/logo/' + Math.random();

      $scope.sponsors = sponsors;
      $scope.confirmedOptions = [{value: true, label: 'Yes'}, {value: false, label: 'No'}];
      $scope.idProperty = 'value';
      $scope.nameProperty = 'label';
      $scope.bootstrapSuffix = 'default';
      loadCompetitions();



    }
    init();


    $scope.resetEvent = function(){
      EventService.reset($scope.eventInstance._id).then(function(){
        //$scope.eventInstance = resetEvent;
        //init();
        //loadCompetitions();
        console.log(eventInstance.name + ' reset');
        $state.go('adminDashboard');
      });
    };
    $scope.deleteEvent = function(){
      EventService.delete(eventInstance._id).success(function(){
        console.log(eventInstance.name + ' removed');
        $state.go('adminDashboard');
      });
    };
    $scope.reopenEvent = function(){
      EventService.reopen(eventInstance._id).then(function(){
        console.log(eventInstance.name + ' reopened');
        $state.go('adminDashboard');
      });
    };

    $scope.changeEvent =  function(){
      $scope.isSavingEvent = true;
      if($scope.eventLogo.pictures && $scope.eventLogo.pictures[0] !== null) {
        var file = $scope.eventLogo.pictures[0];
        file.upload = Upload.upload({
          url: '/api/events/' + $scope.eventInstance._id + '/updateLogo',
          method: 'PUT',
          headers: {
            'my-header': 'my-header-value'
          },
          fields: {id: $scope.eventInstance._id},
          file: file
          //fileFormDataName: 'logo-sponsor'
        });

        file.upload.then(function (response) {
          $timeout(function () {
            file.result = response.data;
            delete $scope.eventInstance.logo;
            EventService.save($scope.eventInstance).then(function(){
              $timeout(function() {
                $scope.isSavingEvent = false;
                $state.go('adminDashboard');
              }, 2000);
            });
          });
        }, function (response) {
          if (response.status > 0){
            $scope.errorMsg = response.status + ': ' + response.data;
          }
        });
      } else {
        delete $scope.eventInstance.logo;
        EventService.save($scope.eventInstance).then(function(){
          $timeout(function() {
            $scope.isSavingEvent = false;
            $state.go('adminDashboard');
          }, 2000);
        });
      }
    };

    $scope.openCreateCompetition = function(){
      CompetitionService.create(eventInstance).then(function(newCompetition){
        //$scope.eventInstance.competitions.push(newCompetition);
        $state.go('adminCompetition', {competitionId: newCompetition._id});
      },function(err){
        console.log(err);
      });
    };
    $scope.deleteCompetition = function(competition){
      CompetitionService.delete(competition._id).success(function(){
        console.log(competition.name + ' removed');
        _.remove($scope.eventInstance.competitions, competition);
      });
    };

    $scope.openAddLocation = function(){
      var AddLocationCtrl = function ($scope, $uibModalInstance) {
        $scope.addLocation = function(){
          $uibModalInstance.close({newLocation: $scope.newLocation, action: 'Save'});
        };
        $scope.cancel = function(){
          $uibModalInstance.close({newLocation: null, action: 'Cancel'});
        };
      };
      var modalInstance = $uibModal.open({
        templateUrl: 'app/admin/event/modalAddLocation/modalAddLocation.html',
        controller: ['$scope', '$uibModalInstance', AddLocationCtrl],
        keyboard: false,
        backdrop: 'static'
      });
      modalInstance.result.then(function (data) {
        switch (data.action) {
          case 'Save':
          {
            console.log('adding new location:  ' + data.newLocation.name);
            $scope.eventInstance.locations.push(data.newLocation);
            break;
          }
          case 'Cancel':
          {
            console.log('Action canceled');
            break;
          }
        }
      });
    };
    $scope.removeLocation = function(location){
      _.remove($scope.eventInstance.locations, location);
    };

    $scope.viewSeason = function(){
      $state.go('adminSeason', {seasonId: $scope.eventInstance.season._id});
    };

    $scope.discardChanges = function(){
      delete $scope.eventLogo;
      $scope.eventInstance = angular.copy(originalEventInstance);
      init();
    };
  }
  acropyxApp.controller('AdminEventCtrl', [
    '$scope',
    '$state',
    '$timeout',
    '$uibModal',
    'Upload',
    'eventInstance',
    'EventService',
    'sponsors',
    'CompetitionService',
    AdminEventCtrl
  ]);
})();
