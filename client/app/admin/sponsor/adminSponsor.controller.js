(function(){
  'use strict';
  var acropyxApp = angular.module('acropyxApp');
  function AdminSponsorCtrl($scope, $state, $timeout, sponsorInstance, SponsorService, Upload){

    var originalSponsorInstance = angular.copy(sponsorInstance);
    $scope.sponsorInstance = sponsorInstance;
    $scope.eventLogo = [];
    $scope.logoUrl = '/api/sponsors/' + $scope.sponsorInstance._id + '/logo/' + Math.random();


    $scope.changeSponsor = function(){
      $scope.isSavingSponsor = true;
      if($scope.sponsorLogo && $scope.sponsorLogo[0] !== null) {
        var file = $scope.sponsorLogo[0];
        file.upload = Upload.upload({
          url: '/api/sponsors/' + $scope.sponsorInstance._id + '/updateLogo',
          method: 'PUT',
          headers: {
            'my-header': 'my-header-value'
          },
          fields: {id: $scope.sponsorInstance._id},
          file: file
          //fileFormDataName: 'logo-sponsor'
        });

        file.upload.then(function (response) {
          $timeout(function () {
            file.result = response.data;
            delete $scope.sponsorInstance.logo;
            SponsorService.save($scope.sponsorInstance).then(function(){
              $timeout(function() {
                $scope.isSavingSponsor = false;
                $state.go('adminDashboard');
              }, 2000);
            });
          });
        }, function (response) {
          if (response.status > 0){
            $scope.errorMsg = response.status + ': ' + response.data;
          }
        });
      } else {
        delete $scope.sponsorInstance.logo;
        SponsorService.save($scope.sponsorInstance).then(function(){
          $timeout(function() {
            $scope.isSavingSponsor = false;
            $state.go('adminDashboard');
          }, 2000);
        });
      }
    };

    $scope.deleteSponsor = function(){
      SponsorService.delete(sponsorInstance._id).success(function(){
        console.log(sponsorInstance.name + ' removed');
        $state.go('adminDashboard');
      });
    };

    $scope.discardChanges = function(){
      $scope.sponsorLogo = null;
      $scope.sponsorInstance = angular.copy(originalSponsorInstance);
    };


    //$scope.uploadSponsorLogo = function(selectedFiles) {
    //  var file = selectedFiles[0];
    //  file.upload = Upload.upload({
    //    url: '/api/sponsors/' + $scope.sponsorInstance._id + '/updateLogo',
    //    method: 'PUT',
    //    headers: {
    //      'my-header': 'my-header-value'
    //    },
    //    fields: {id: $scope.sponsorInstance._id},
    //    file: file
    //    //fileFormDataName: 'logo-sponsor'
    //  });
    //
    //  file.upload.then(function (response) {
    //    $timeout(function () {
    //      file.result = response.data;
    //    });
    //  }, function (response) {
    //    if (response.status > 0)
    //      $scope.errorMsg = response.status + ': ' + response.data;
    //  });
    //
    //  file.upload.progress(function (evt) {
    //    // Math.min is to fix IE which reports 200% sometimes
    //    file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
    //  });
    //
    //  file.upload.xhr(function (xhr) {
    //    // xhr.upload.addEventListener('abort', function(){console.log('abort complete')}, false);
    //  });
    //}
  }
  acropyxApp.controller('AdminSponsorCtrl', [
    '$scope',
    '$state',
    '$timeout',
    'sponsorInstance',
    'SponsorService',
    'Upload',
    AdminSponsorCtrl
  ]);
})();
