(function() {
  'use strict';
  var acropyxApp = angular.module('acropyxApp');
  function AdminDocumentsCtrl($scope,  documents){
    $scope.documents = documents;   
  }
  acropyxApp.controller('AdminDocumentsCtrl', [
    '$scope',    
    'documents',
    AdminDocumentsCtrl
  ]);
})();
