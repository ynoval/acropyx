(function () {
  'use strict';
  var acropyxApp = angular.module('acropyxApp');

  function AdminTeamCtrl($scope, $state, teamInstance, pilots, TeamService) {

    var originalJudgeInstance = angular.copy(teamInstance);
    $scope.teamInstance = teamInstance;
    $scope.pilots = pilots;

    $scope.changeJudge= function(){
      $scope.isSavingJudge = true;
      JudgeService.save(judgeInstance).success(function(){
        $scope.isSavingJudge = false;
        $state.go('adminDashboard');
      });
    };

    $scope.deleteJudge = function(){
      $scope.isDeletingJudge = true;
      JudgeService.delete(judgeInstance._id).success(function(){
        $scope.isDeletingJudge = false;
        $state.go('adminDashboard');
      });
    };

    $scope.changeTeam = function () {
      $scope.isSavingTeam = true;
      TeamService.save(teamInstance).success(function(){
        $scope.isSavingTeam = false;
        $state.go('adminDashboard');
      });
    };

    $scope.deleteTeam = function () {
      $scope.isDeletingTeam = true;
      TeamService.delete(teamInstance._id).success(function () {
        $scope.isDeletingTeam = false;
        $state.go('adminDashboard');
      });
    };

    $scope.discardChanges = function(){
      $scope.judgeInstance = angular.copy(originalJudgeInstance);
    };
  }

  acropyxApp.controller('AdminTeamCtrl', [
    '$scope',
    '$state',
    'teamInstance',
    'pilots',
    'TeamService',
    AdminTeamCtrl
  ]);
})();
