(function() {
  'use strict';
  var acropyxApp = angular.module('acropyxApp');
  function AdminCompetitionCtrl($scope, $state, $window, $uibModal,
                                competitionInstance, CompetitionService,
                                RunService, allCompetitors, UtilsService){

    var originalCompetitionInstance = angular.copy(competitionInstance);
    $scope.competitorList = {};
    $scope.competitionInstance = competitionInstance;
    $scope.availableCompetitors = UtilsService.difference(allCompetitors, $scope.competitionInstance.competitors);
    /*Results*/
    function loadResults(){
      if($scope.competitionInstance.startTime){
        $scope.isLoadingResults = true;
        CompetitionService.queryResults($scope.competitionInstance._id).then(function(results){
          $scope.competitionResults = results;
          $scope.isLoadingResults = false;
        });
      }
    }
    $scope.generateCompetitionResultsPDF = function() {
      CompetitionService.generateResultsReportPDF($scope.competitionInstance._id).then(function(){
        var url = 'pdf/' + $scope.competitionInstance.code + '_Results.pdf';
        $window.open(url);
      });
    };
    $scope.generateCompetitionResultsCSV = function() {
      CompetitionService.generateResultsReportCSV($scope.competitionInstance._id).then(function(){
        var url = 'csv/' + $scope.competitionInstance.code + '_Results.csv';
        $window.open(url);
      });
    };

    /*
     * Results FAI
     */
    function loadResultsFAI(){
      if($scope.competitionInstance.startTime && $scope.competitionInstance.endTime){
        $scope.isLoadingResultsFAI = true;
        CompetitionService.queryResultsFAI($scope.competitionInstance._id).then(function(results){
          $scope.competitionResultsFAI = results;
          $scope.isLoadingResultsFAI = false;
        });
      }
    }

    //Runs
    function loadRuns(){
      $scope.isLoadingRuns = true;
      CompetitionService.queryRuns($scope.competitionInstance._id).then(function(runs){
        $scope.competitionInstance.runs = runs;
        $scope.isLoadingRuns = false;
      });
    }

    function init(){
      loadRuns();
      loadResults();
      loadResultsFAI();
    }
    init();

    $scope.viewEvent = function(){
      $state.go('adminEvent', {eventId: $scope.competitionInstance.event._id});
    };

    $scope.deleteCompetition = function(){
      CompetitionService.delete(competitionInstance._id).success(function(){
        console.log(competitionInstance.name + ' removed');
        $state.go('adminDashboard');
      });
    };
    $scope.changeCompetition = function(){
      $scope.isSavingCompetition = true;
      CompetitionService.save($scope.competitionInstance).success(function(){
        $scope.isSavingCompetition = false;
        $state.go('adminDashboard');  
      });

    };
    $scope.resetCompetition = function(){
      CompetitionService.reset($scope.competitionInstance._id).then(function(){
        CompetitionService.get($scope.competitionInstance._id).then(function(competition){
          $scope.competitionInstance = competition;
          loadRuns();
        });
      });
    };
    $scope.reopenCompetition = function(){
      CompetitionService.reopen($scope.competitionInstance._id).then(function(){
        $state.go('adminDashboard');
      });
    };

    //Competitors
    $scope.addCompetitor = function(){
      $scope.competitionInstance.competitors.push($scope.competitorList.selectedCompetitor);
      _.remove($scope.availableCompetitors, $scope.competitorList.selectedCompetitor);
      $scope.competitorList = {};
      $scope.competitorAdded = true;
    };
    $scope.removeCompetitor = function(competitor){
      $scope.availableCompetitors.push(competitor);
      _.remove($scope.competitionInstance.competitors, competitor);
    };
    $scope.openAddSubscription = function(){
      var AddSubscriptionsCtrl = function ($scope, $uibModalInstance, availableCompetitors) {
        $scope.availableCompetitors = _.sortBy(availableCompetitors, 'name');
        $scope.selectedCompetitors = [];

        $scope.addCompetitor = function(competitor){
          $scope.selectedCompetitors.push(competitor);
          _.remove($scope.availableCompetitors, competitor);
        };
        $scope.removeCompetitor = function(competitor){
          _.remove($scope.selectedCompetitors, competitor);
          $scope.availableCompetitors.push(competitor);
          $scope.availableCompetitors = _.sortBy($scope.availableCompetitors, 'name');
        };

        $scope.addSubscriptions = function(){
          $uibModalInstance.close({selectedCompetitors: $scope.selectedCompetitors, action: 'Save'});
        };
        $scope.cancel = function(){
          $uibModalInstance.close({selectedCompetitors: null, action: 'Cancel'});
        };
      };
      var modalInstance = $uibModal.open({
        templateUrl: 'app/admin/competition/modalAddSubscriptions/addSubscriptions.html',
        controller: ['$scope', '$uibModalInstance', 'availableCompetitors', AddSubscriptionsCtrl],
        windowClass: 'competitors-modal',
        keyboard: false,
        backdrop: 'static',
        resolve:{/* @ngInject */
          availableCompetitors : function(){
            return $scope.availableCompetitors;
          }
        }
      });

      modalInstance.result.then(function (data) {
        switch (data.action) {
          case 'Save':
          {
            data.selectedCompetitors.forEach(function(competitor){
              $scope.competitionInstance.competitors.push(competitor);
              _.remove($scope.availableCompetitors, competitor);
            });
            break;
          }
          case 'Cancel':
          {
            console.log('Action canceled');
            break;
          }
        }
      });
    };

    $scope.openCreateRun = function(){
      RunService.create(competitionInstance).then(function(newRun){
        //$scope.competitionInstance.runs.push(newRun);
        $state.go('adminRun', {runId: newRun._id});
      },function(err){
        console.log(err);
      });
    };
    $scope.deleteRun = function(run){
      RunService.delete(run._id).success(function(){
        console.log(run.name + ' removed');
        //TODO: FIX!!
        loadRuns();
        //_.remove($scope.competitionInstance.runs, run);
      });
    };
    $scope.downRun = function(run) {
      if (run.order < $scope.competitionInstance.runs.length) {
        $scope.isLoadingRuns = true;
        RunService.changeOrder(run._id, run.order + 1).then(function () {
          //TODO: FIX!!
          loadRuns();
          $scope.isLoadingRuns = false;
        });
      }
    };
    $scope.upRun = function(run){
      if(run.order > 1){
        $scope.isLoadingRuns = true;
        RunService.changeOrder(run._id, run.order - 1).then(function(){
          //TODO: FIX!!
          loadRuns();
          $scope.isLoadingRuns = false;
        });
      }
    };

    $scope.discardChanges = function(){
      $scope.competitionInstance = angular.copy(originalCompetitionInstance);
      init();
    };

  }
  acropyxApp.controller('AdminCompetitionCtrl', [
    '$scope',
    '$state',
    '$window',
    '$uibModal',
    'competitionInstance',
    'CompetitionService',
    'RunService',
    'allCompetitors',
    'UtilsService',
    AdminCompetitionCtrl
  ]);
})();
