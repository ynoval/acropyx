(function(){
  'use strict';
  var acropyxApp = angular.module('acropyxApp');
  function AdminDashboardCtrl($scope, $state, $notification, SeasonService, SponsorService,
            EventService, CompetitionService, RunService, JudgeService,
            ManoeuvreService, PilotService, TeamService) {

    //Manoeuvres
    function loadManoeuvres(){
      ManoeuvreService.query().then(function(manoeuvres){
        $scope.manoeuvres = manoeuvres;
      });
    }
    $scope.openAddManoeuvre = function(){
      ManoeuvreService.create().then(function(newManoeuvre){
        $scope.manoeuvres.push(newManoeuvre);
      },function(err){
        console.log(err);
      });
    };
    $scope.deleteManoeuvre = function(manoeuvre){
      ManoeuvreService.delete(manoeuvre._id).success(function(){
        console.log(manoeuvre.name + ' removed');
        _.remove($scope.manoeuvres, manoeuvre);
      });
    };

    //Sponsor
    function loadSponsors(){
      SponsorService.query().then(function(sponsors){
        $scope.sponsors = sponsors;
      });
    }
    $scope.openAddSponsor = function(){
      SponsorService.create().then(function(newSponsor){
        //$scope.sponsors.push(newSponsor);
        $state.go('adminSponsor', {sponsorId: newSponsor._id});
      },function(err){
        console.log(err);
      });
    };
    $scope.deleteSponsor = function(sponsor){
      SponsorService.delete(sponsor._id).success(function(){
        console.log(sponsor.name + ' removed');
        _.remove($scope.sponsors, sponsor);
      });
    };

    //Judge
    function loadJudges(){
      JudgeService.query().then(function(judges){
        $scope.judges = judges;
      });
    }
    $scope.openAddJudge = function(){
      JudgeService.create().then(function(newJudge){
        $scope.judges.push(newJudge);
      },function(err){
        console.log(err);
      });
    };
    $scope.deleteJudge = function(judge){
      JudgeService.delete(judge._id).success(function(){
        console.log(judge.name + ' removed');
        _.remove($scope.judges, judge);
      });
    };

    //Team
    function loadTeams(){
      TeamService.query().then(function(teams){
        $scope.teams = teams;
      });
    }
    $scope.openAddTeam = function(){
      TeamService.create().then(function(newTeam){
        //$scope.teams.push(newTeam);
        $state.go('adminTeam', {teamId: newTeam._id});
      },function(err){
        console.log(err);
      });
    };
    $scope.deleteTeam = function(team){
      TeamService.delete(team._id).success(function(){
        console.log(team.name + ' removed');
        _.remove($scope.teams, team);
      });
    };

    //Pilot
    function loadPilots(){
      PilotService.query().then(function(pilots){
        $scope.pilots = pilots;
      });
    }
    $scope.openAddPilot = function(){
      PilotService.create().then(function(newPilot){
        //$scope.pilots.push(newPilot);
        $state.go('adminPilot', {pilotId: newPilot._id});
      },function(err){
        console.log(err);
      });
    };
    $scope.deletePilot = function(pilot){
      PilotService.delete(pilot._id).success(function(){
        console.log(pilot.name + ' removed');
        _.remove($scope.pilots, pilot);
      });
    };

    //Run
    function loadRuns(){
      RunService.query().then(function(runs){
        $scope.runs = _.sortBy(runs, function (obj) {
          return obj.competition.name;
        });
        //$scope.runs = _.sortBy(_.pluck(runs, 'competition'), ['name']);
      });
    }
    $scope.openAddRun = function(){
      RunService.create().then(function(newRun){
        //$scope.runs.push(newRun);
        $state.go('adminRun', {runId: newRun._id});
      },function(err){
        console.log(err);
      });
    };
    $scope.deleteRun = function(run){
      RunService.delete(run._id).success(function(){
        console.log(run.name + ' removed');
        _.remove($scope.runs, run);
      });
    };

    //Competition
    function loadCompetitions(){
      CompetitionService.query().then(function(competitions){
        $scope.competitions = competitions;
      });
    }
    $scope.openAddCompetition = function(){
      CompetitionService.create().then(function(newCompetition){
        //$scope.competitions.push(newCompetition);
        $state.go('adminCompetition', {competitionId: newCompetition._id});
      },function(err){
        console.log(err);
      });
    };
    $scope.deleteCompetition = function(competition){
      CompetitionService.delete(competition._id).success(function(){
        console.log(competition.name + ' removed');
        _.remove($scope.competitions, competition);
        loadRuns();
      });
    };

    //Event
    function loadEvents(){
      EventService.query().then(function(events){
        $scope.events = events;
      });
    }
    $scope.openAddEvent = function(){
      EventService.create().then(function(newEvent){
        //$scope.events.push(newEvent);
        $state.go('adminEvent', {eventId: newEvent._id});
      },function(err){
        console.log(err);
      });
    };
    $scope.deleteEvent = function(event){
      EventService.delete(event._id).success(function(){
        console.log(event.name + ' removed');
        _.remove($scope.events, event);
        loadCompetitions();
        loadRuns();
      });
    };

    //Season
    function loadSeasons(){
      SeasonService.query().then(function(seasons){
        $scope.seasons = seasons;
        $notification.info('Season', 'Seasons Loaded');
      });
    }
    $scope.openAddSeason = function(){
      SeasonService.create().then(function(newSeason){
        //$scope.seasons.push(newSeason);
        $state.go('adminSeason', {seasonId: newSeason._id});
      },function(err){
        console.log(err);
      });
    };
    $scope.deleteSeason = function(season){
      SeasonService.delete(season._id).success(function(){
        console.log(season.name + ' removed');
        _.remove($scope.seasons, season);
        loadEvents();
        loadCompetitions();
        loadRuns();
      });
    };

    function init(){
      loadSeasons();
      loadEvents();
      loadCompetitions();
      loadRuns();
      loadPilots();
      loadTeams();
      loadJudges();
      loadSponsors();
      loadManoeuvres();
    }
    init();
  }
  acropyxApp.controller('AdminDashboardCtrl', [
    '$scope',
    '$state',
    '$notification',
    'SeasonService',
    'SponsorService',
    'EventService',
    'CompetitionService',
    'RunService',
    'JudgeService',
    'ManoeuvreService',
    'PilotService',
    'TeamService',
    AdminDashboardCtrl
  ]);
})();
