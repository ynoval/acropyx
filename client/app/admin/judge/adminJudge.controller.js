(function(){
  'use strict';
  var acropyxApp = angular.module('acropyxApp');
  function AdminJudgeCtrl($scope, $state, judgeInstance, judgeTypes, JudgeService){
    var originalJudgeInstance = angular.copy(judgeInstance);
    $scope.judgeInstance = judgeInstance;
    $scope.judgeTypes = judgeTypes;
    $scope.idProperty = '_id';
    $scope.nameProperty = 'name';
    $scope.bootstrapSuffix = 'default';

    $scope.changeJudge= function(){
      $scope.isSavingJudge = true;
      JudgeService.save(judgeInstance).success(function(){
        $scope.isSavingJudge = false;
        $state.go('adminDashboard');
      });
    };

    $scope.deleteJudge = function(){
      $scope.isDeletingJudge = true;
      JudgeService.delete(judgeInstance._id).success(function(){
        $scope.isDeletingJudge = false;
        $state.go('adminDashboard');
      });
    };

    $scope.discardChanges = function(){      
      $scope.judgeInstance = angular.copy(originalJudgeInstance);
    };
  }
  acropyxApp.controller('AdminJudgeCtrl', [
    '$scope',
    '$state',
    'judgeInstance',
    'judgeTypes',
    'JudgeService',
    AdminJudgeCtrl
  ]);
})();
