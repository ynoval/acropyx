(function () {
  'use strict';
  var acropyxApp = angular.module('acropyxApp');
  function Config($stateProvider) {
    $stateProvider
      .state('adminDashboard', {
        url: '/admin',
        templateUrl: 'app/admin/dashboard/adminDashboard.html',
        controller: 'AdminDashboardCtrl',
        authenticate: true
      })
      .state('adminSeason', {
        url: '/admin/season/:seasonId',
        templateUrl: 'app/admin/season/adminSeason.html',
        controller: 'AdminSeasonCtrl',
        authenticate: true,
        resolve: {/* @ngInject */
          seasonInstance: function ($stateParams, SeasonService) {
            return SeasonService.get($stateParams.seasonId);
          },/* @ngInject */
          sponsors: function (SponsorService) {
            return SponsorService.query();
          }
        }
      })
      .state('adminEvent', {
        url: '/admin/event/:eventId',
        templateUrl: 'app/admin/event/adminEvent.html',
        controller: 'AdminEventCtrl',
        authenticate: true,
        resolve: {/* @ngInject */
          eventInstance: function ($stateParams, EventService) {
            return EventService.get($stateParams.eventId);
          },/* @ngInject */
          sponsors: function (SponsorService) {
            return SponsorService.query();
          }
          //TODO: Add in future versions
          //judges: function(JudgeService){
          //  return JudgeService.query();
          //}
        }
      })
      .state('adminCompetition', {
        url: '/admin/competition/:competitionId',
        templateUrl: 'app/admin/competition/adminCompetition.html',
        controller: 'AdminCompetitionCtrl',
        authenticate: true,
        resolve: {/* @ngInject */
          competitionInstance: function ($stateParams, CompetitionService) {
            return CompetitionService.get($stateParams.competitionId);
          },/* @ngInject */
          competitionTypes: function (CompetitionService) {
            return CompetitionService.queryTypes();
          }, /* @ngInject */
          allCompetitors: function ($stateParams, CompetitionService) {
            return CompetitionService.queryAllCompetitors($stateParams.competitionId);
          }
        }
      })
      .state('adminRun', {
        url: '/admin/run/:runId',
        templateUrl: 'app/admin/run/adminRun.html',
        controller: 'AdminRunCtrl',
        authenticate: true,
        resolve: {/* @ngInject */
          runInstance: function ($stateParams, RunService) {
            return RunService.get($stateParams.runId);
          },/* @ngInject */
          runTypes: function (RunService) {
            return RunService.queryTypes();
          },/* @ngInject */
          judges: function (JudgeService) {
            return JudgeService.query();
          }
        }
      })
      .state('adminPilot', {
        url: '/admin/pilot/:pilotId',
        templateUrl: 'app/admin/pilot/adminPilot.html',
        controller: 'AdminPilotCtrl',
        authenticate: true,
        resolve: {/* @ngInject */
          pilotInstance: function ($stateParams, PilotService) {
            return PilotService.get($stateParams.pilotId);
          }, /* @ngInject */
          sponsors: function (SponsorService) {
            return SponsorService.query();
          }, /* @ngInject */
          countries: function (SystemService) {
            return SystemService.queryCountries();
          }
        }
      })
      .state('adminTeam', {
        url: '/admin/team/:teamId',
        templateUrl: 'app/admin/team/adminTeam.html',
        controller: 'AdminTeamCtrl',
        authenticate: true,
        resolve: {/* @ngInject */
          teamInstance: function ($stateParams, TeamService) {
            return TeamService.get($stateParams.teamId);
          },/* @ngInject */
          pilots: function (PilotService) {
            return PilotService.query();
          }
        }
      })
      .state('adminJudge', {
        url: '/admin/judge/:judgeId',
        templateUrl: 'app/admin/judge/adminJudge.html',
        controller: 'AdminJudgeCtrl',
        authenticate: true,
        resolve: {/* @ngInject */
          judgeInstance: function ($stateParams, JudgeService) {
            return JudgeService.get($stateParams.judgeId);
          }, /* @ngInject */
          judgeTypes: function (JudgeService) {
            return JudgeService.queryTypes();
          }
        }
      })
      .state('adminSponsor', {
        url: '/admin/sponsor/:sponsorId',
        templateUrl: 'app/admin/sponsor/adminSponsor.html',
        controller: 'AdminSponsorCtrl',
        authenticate: true,
        resolve: {/* @ngInject */
          sponsorInstance: function ($stateParams, SponsorService) {
            return SponsorService.get($stateParams.sponsorId);
          }
        }
      })
      .state('adminManoeuvre', {
        url: '/admin/manoeuvre/:manoeuvreId',
        templateUrl: 'app/admin/manoeuvre/adminManoeuvre.html',
        controller: 'AdminManoeuvreCtrl',
        authenticate: true,
        resolve: {/* @ngInject */
          manoeuvreInstance: function ($stateParams, ManoeuvreService) {
            return ManoeuvreService.get($stateParams.manoeuvreId);
          }
        }
      })
      .state('adminSetting', {
        url: '/admin/setting',
        templateUrl: 'app/admin/setting/adminSetting.html',
        controller: 'AdminSettingCtrl',
        authenticate: true
      })
      .state('adminDocuments', {
        url: '/admin/documents',
        templateUrl: 'app/admin/documents/adminDocuments.html',
        controller: 'AdminDocumentsCtrl',
        authenticate: true,
        resolve: {/* @ngInject */
          documents: function(){
            return [ ];
          }
        }
      })
    ;
  }
  acropyxApp.config(['$stateProvider', Config]);
})();
