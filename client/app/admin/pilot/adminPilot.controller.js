(function () {
  'use strict';
  var acropyxApp = angular.module('acropyxApp');

  function AdminPilotCtrl($scope, $state, $timeout, pilotInstance, sponsors,
                          Upload, PilotService, countries) {

    $scope.pilotInstance = pilotInstance;
    var originalPilotInstance = angular.copy($scope.pilotInstance);

    function init() {
      $scope.pictureUrl = '/api/pilots/' + $scope.pilotInstance._id + '/picture/' + Math.random();
      $scope.sponsors = sponsors;
      $scope.countries = countries;

      $scope.sexOptions = [{value: 'Female', label: 'Female'}, {value: 'Male', label: 'Male'}];
      $scope.idPropertySex = 'value';
      $scope.namePropertySex = 'label';


      $scope.faiLicenceOptions = [{value: true, label: 'Yes'}, {value: false, label: 'No'}];
      $scope.idPropertyLicence = 'value';
      $scope.namePropertyLicence = 'label';

      $scope.pilotInstance.dateOfBirth = new Date($scope.pilotInstance.dateOfBirth);

      $scope.open = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();

        $scope.opened = true;
      };
      $scope.dateOptions = {
        formatYear: 'yy',
        'min-mode': 'year',
        startingDay: 1
      };

      $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
      $scope.format = $scope.formats[0];

      $scope.format = 'dd.MM.yyyy';
    }

    init();

    $scope.changePilot = function () {
      $scope.isSavingPilot = true;
      if ($scope.pilotPicture && $scope.pilotPicture[0] !== null) {
        var file = $scope.pilotPicture[0];
        file.upload = Upload.upload({
          url: '/api/pilots/' + $scope.pilotInstance._id + '/updatePicture',
          method: 'PUT',
          headers: {
            'my-header': 'my-header-value'
          },
          fields: {id: $scope.pilotInstance._id},
          file: file
          //fileFormDataName: 'logo-sponsor'
        });

        file.upload.then(function (response) {
          $timeout(function () {
            file.result = response.data;
            delete $scope.pilotInstance.picture;
            PilotService.save($scope.pilotInstance).then(function () {
              $timeout(function () {
                $scope.isSavingPilot = false;
                $state.go('adminDashboard');
              }, 2000);
            });
          });
        }, function (response) {
          if (response.status > 0) {
            $scope.errorMsg = response.status + ': ' + response.data;
          }
        });
      } else {
        delete $scope.pilotInstance.picture;
        PilotService.save($scope.pilotInstance).then(function () {
          $timeout(function () {
            $scope.isSavingSponsor = false;
            $state.go('adminDashboard');
          }, 2000);
        });
      }
    };
    $scope.deletePilot = function () {
      PilotService.delete(pilotInstance._id).success(function () {
        console.log(pilotInstance.name + ' removed');
        $state.go('adminDashboard');
      });
    };
    $scope.discardChanges = function () {
      delete $scope.pilotPicture;
      $scope.pilotInstance = angular.copy(originalPilotInstance);
      init();
    };
  }

  acropyxApp.controller('AdminPilotCtrl', [
    '$scope',
    '$state',
    '$timeout',
    'pilotInstance',
    'sponsors',
    'Upload',
    'PilotService',
    'countries',
    AdminPilotCtrl
  ]);
})();
