(function(){
  'use strict';
  var acropyxApp = angular.module('acropyxApp');
  function AdminSettingsCtrl($scope, $timeout, SettingService, UtilsService, ManoeuvreService){
    //Incremental warning options
    $scope.incrementalOptions = [{value: true, label: 'Yes'}, {value: false, label: 'No'}];
    $scope.idProperty = 'value';
    $scope.nameProperty = 'label';
    $scope.bootstrapSuffix = 'default';
    var originalSettings;

    if ($scope.settingId !== 0) {
      SettingService.get($scope.settingId).then(function (settings) {
        originalSettings = angular.copy(settings);
        $scope.settingInstance = settings;
        ManoeuvreService.query().then(function (manoeuvres) {
          $scope.manoeuvres = UtilsService.difference(manoeuvres, $scope.settingInstance.withoutPenaltyManoeuvres);
        });
      });
    }
    else {
      SettingService.getDefaults().then(function (settings) {
        originalSettings = angular.copy(settings);
        $scope.settingInstance = settings;
        ManoeuvreService.query().then(function (manoeuvres) {
          $scope.manoeuvres = UtilsService.difference(manoeuvres, $scope.settingInstance.withoutPenaltyManoeuvres);
        });
      });
    }

    originalSettings = angular.copy($scope.settingInstance);

    $scope.changeSettings = function () {
      $scope.isSaving = true;
      $timeout(function(){
        $scope.isSaving = false;
        SettingService.save($scope.settingInstance);
      }, 100);
    };

    $scope.discardChanges = function () {
      $scope.settingInstance = angular.copy(originalSettings);
    };
  }
  function AdminSettings(){
    return {
      restrict: 'E',
      scope: {
        settingId: '<'
      },
      controller: ['$scope', '$timeout', 'SettingService', 'UtilsService', 'ManoeuvreService', AdminSettingsCtrl],
      templateUrl: 'app/admin/common/settings/directives/settings.html'
    };
  }
  acropyxApp.directive('ngAdminSettings', [AdminSettings]);
})();

