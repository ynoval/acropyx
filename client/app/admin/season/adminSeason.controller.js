(function(){
  'use strict';
  var acropyxApp = angular.module('acropyxApp');
  function _AdminSeasonCtrl($scope, $state, $window, $timeout, seasonInstance,  SeasonService,
                           sponsors, EventService, SettingService){
    function computeFemaleSynchroRanking(){
      // $scope.seasonInstance.femaleSynchroRanking = [];
      // $scope.seasonInstance.synchroRanking.forEach(function(competitor){
      //   if (competitor.sex === 'Female'){
      //     $scope.seasonInstance.femaleSynchroRanking.push(competitor);
      //   }
      // });
    }
    function loadSynchroRanking(){
      $scope.isLoadingSynchroRanking = true;
      SeasonService.queryRankingSynchro($scope.seasonInstance._id).then(function(ranking){
        $scope.seasonInstance.synchroRanking = ranking;
        $scope.isLoadingSynchroRanking = false;
        computeFemaleSynchroRanking();
      });
    }
    function computeFemaleSoloRanking(){
      // $scope.seasonInstance.femaleSoloRanking = [];
      // $scope.seasonInstance.soloRanking.forEach(function(competitor){
      //   if (competitor.sex === 'Female'){
      //     $scope.seasonInstance.femaleSoloRanking.push(competitor);
      //   }
      // });
    }
    function loadSoloRanking(){
      $scope.isLoadingSoloRanking = true;
      SeasonService.queryRankingSolo($scope.seasonInstance._id).then(function(ranking){
        $scope.seasonInstance.soloRanking = ranking;
        $scope.isLoadingSoloRanking = false;
        computeFemaleSoloRanking();
      });
    }
    function loadRankings(){
      loadSoloRanking();
      loadSynchroRanking();
    }
    function loadEvents(){
      $scope.isLoadingEvents = true;
      SeasonService.queryEvents($scope.seasonInstance._id).then(function(events){
        //TODO: Sort By startTime
        $scope.seasonInstance.events = events;
        $scope.isLoadingEvents = false;
      });
    }
    function init(){
      $scope.seasonInstance = seasonInstance;
      $scope.sponsors = sponsors;
      $scope.pubicOptions = [{value: true, label: 'Yes'}, {value: false, label: 'No'}];
      $scope.idProperty = 'value';
      $scope.nameProperty = 'label';
      $scope.bootstrapSuffix = 'default';
      loadEvents();
      loadRankings();
    }
    init();

    $scope.getSettings = function(){
      return  SettingService.get($scope.seasonInstance.settings);
    };

    $scope.changeSeason = function(){
      $scope.isSavingSeason = true;
      seasonInstance.events.forEach(function(event){
        delete event.logo;
      });
      SeasonService.save(seasonInstance).then(function() {
        $timeout(function () {
          $scope.isSavingSeason = false;
          $state.go('adminDashboard');
        }, 2000);
      });
    };

    $scope.deleteSeason = function(){
      var season = $scope.seasonInstance;
      SeasonService.delete(seasonInstance._id).success(function(){
        console.log(season.name + ' removed');
        $state.go('adminDashboard');
      });
    };

    $scope.openCreateEvent = function(){
      EventService.create($scope.seasonInstance).then(function(newEvent){
        //$scope.seasonInstance.events.push(newEvent);
        $state.go('adminEvent', {eventId: newEvent._id});
      },function(err){
        console.log(err);
      });
    };
    $scope.deleteEvent = function(event){
      EventService.delete(event._id).success(function(){
        console.log(event.name + ' removed');
        _.remove($scope.seasonInstance.events, event);
      });
    };

    $scope.generateSoloRankingPDF = function() {
      SeasonService.generateSoloRankingReportPDF($scope.seasonInstance._id).then(function(){
        var url = 'pdf/' + $scope.seasonInstance.code + '_StandingsSolo.pdf';
        $window.open(url);
      });
    };
    $scope.generateSynchroRankingPDF = function() {
      SeasonService.generateSynchroRankingReportPDF($scope.seasonInstance._id).then(function(){
        var url = 'pdf/' + $scope.seasonInstance.code + '_StandingsSynchro.pdf';
        $window.open(url);
      });
    };
  }
  acropyxApp.controller('AdminSeasonCtrl',[
    '$scope',
    '$state',
    '$window',
    '$timeout',
    'seasonInstance',
    'SeasonService',
    'sponsors',
    'EventService',
    'SettingService',
    _AdminSeasonCtrl
  ]);
})();
