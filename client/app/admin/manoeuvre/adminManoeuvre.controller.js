(function(){
  'use strict';
  var acropyxApp = angular.module('acropyxApp');
  function AdminManoeuvreCtrl($scope, $state, $timeout, manoeuvreInstance, ManoeuvreService){
    var originalManoeuvreInstance = angular.copy(manoeuvreInstance);
    $scope.manoeuvreInstance = manoeuvreInstance;
    $scope.synchroOptions =[{_id: true, name: 'Yes'},{_id: false, name: 'No'}];
    $scope.sideOptions =[{_id: true, name: 'Yes'},{_id: false, name: 'No'}];
    $scope.idProperty = '_id';
    $scope.nameProperty = 'name';
    $scope.bootstrapSuffix = 'default';

    $scope.changeManoeuvre = function(){
      $scope.isSavingManoeuvre = true;
      ManoeuvreService.save(manoeuvreInstance).then(function() {
        $timeout(function () {
          $scope.isSavingManoeuvre = false;
          $state.go('adminDashboard');
        }, 500);
      });
    };

    $scope.deleteManoeuvre = function(){
      $scope.isDeletingManoeuvre = true;
      ManoeuvreService.delete(manoeuvreInstance._id).then(function() {
        $timeout(function () {
          $scope.isDeletingManoeuvre = false;
          $state.go('adminDashboard');
        }, 500);
      });
      // ManoeuvreService.delete(manoeuvreInstance._id).success(function(){
      //   console.log(manoeuvreInstance.name + ' removed');
      //   $state.go('adminDashboard');
      // });
    };

    $scope.discardChanges = function(){
      $scope.manoeuvreInstance = angular.copy(originalManoeuvreInstance);
    };
  }
  acropyxApp.controller('AdminManoeuvreCtrl', [
    '$scope',
    '$state',
    '$timeout',
    'manoeuvreInstance',
    'ManoeuvreService',
    AdminManoeuvreCtrl
  ]);
})();
