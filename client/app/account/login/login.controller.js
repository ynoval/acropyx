(function(){
  'use strict';
  var acropyxApp = angular.module('acropyxApp');
  function LoginCtrl($scope, Auth, $state, $window) {
    $scope.user = {};
    $scope.errors = {};
    $scope.login = function (form) {
      $scope.submitted = true;
      if (form.$valid) {
        Auth.login({
            email: $scope.user.email,
            password: $scope.user.password
          })
          .then(function () {
            // Logged in
            Auth.isLoggedInAsync(function () {
              if (Auth.isAdmin()) {
                $state.go('adminDashboard');
                return;
              }
              if (Auth.isManager()) {
                $state.go('managerDashboard');
                return;
              }
            });
          })
          .catch(function (err) {
            $scope.errors.other = err.message;
          });
      }
    };
    $scope.loginOauth = function (provider) {
      $window.location.href = '/auth/' + provider;
    };
  }
  acropyxApp.controller('LoginCtrl', ['$scope', 'Auth', '$state', '$window', LoginCtrl]);
})();
