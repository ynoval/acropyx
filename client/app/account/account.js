(function(){
  'use strict';
  var acropyxApp = angular.module('acropyxApp');
  function Config($stateProvider){
    $stateProvider
      .state('login', {
        url: '/login',
        templateUrl: 'app/account/login/login.html',
        controller: 'LoginCtrl'
      })
      .state('settings', {
        url: '/settings',
        templateUrl: 'app/account/settings/settings.html',
        controller: 'SettingsCtrl',
        authenticate: true
      });
  }
  acropyxApp.config(['$stateProvider', Config]);
})();


