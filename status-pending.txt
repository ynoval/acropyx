DASHBOARD
 Components
  - poner un scroll lindo en cada componente (5)
  - adicionar loading icon a cada componente (2)
  - adicionar otros campos en cada componente

 Add Manoeuvre
  - Validar no hay nombre repetido
  - Validar Bonus (integer  [0 ... 20])
  - Validar coefficient (real, step 0.05 ((0 .. 3])
  - Notificar que se adiciono correctamente

 Add Season
  - Validar no hay season repetidas
  - Notificar que se adiciono correctamente
  - Validar code solo tenga letras y numeros

 Add Event
  - Validar nombre no puede estar repetido
  - Desactivar el add event si no hay seasons
  - Notificar que se adiciono correctamente

 Add Competition
  - Validar que el nombre y el code no esten repetidos
  - Desactivar el add competition si no hay events
  - Codigo validar solo letras o numeros
  - Notificar que se adiciono correctamente

 Add Run
  - Validar que el nombre y el code no esten repetidos
  - Desactivar el add run si no hay competitions
  - Adicionar para poner orden de la run (first, second)
  - Validar codigo solo tenga letras y numeros

 Add Pilot
   - validar todo
   - al adicionar cosultar FAI service y update pilot info

 Add Team
 - Validar Todo
 - Nombre no puede estar repetido
 - Adicionar Pilotos (al menos dos en las validaciones)

 Add Judge
  - Validar todo

 Add Sponsor
   - Validar Todo

DISPLAYER
No mostrar el ranking si es 1000 o superior (significa que no tiene ranking)

SETTINGS
 - Validar las sumas (2)
 - Validar los requeridos (2)
 - poner los inputs como number (poner reglas mayor que 1 en varios) (3)
 - mejorar los help-text debajo de cada campo (5)
 - implementar el discard changes (solo este activo si algo se cambio)
 - Poner un load Defaults (si estamos en los settings de una season)


GENERAL
- poner un icon de loading...(5)
- incluir ngMessages y ngAnimate
- desabilitar el add (en la directiva cuando no hay elementos o se llego a un maximo (util para los teams))
- format para todas las fechas




EVENT:
 - Poner foto de no-image en el logo(3)

 Competition Pending:

 Run Pending:

 Pilot pending:
 - Add Pilot dashboard
 - Poner foto de no-image(3)
 - Validar
 - Paises
 - Borrar Pilot



 Team Pending:
 - Poner foto de no image para cada piloto(3)



 Jueces Pending:
 - Poner foto de no image (3)

 Sponsor Pending:
 - Poner foto de no-image en el logo(3)

 Users:
 - Crear user manager (1)


MANAGER
 Displayer Pending:
  - hablarle al displayer (1)
 UI Pending
  - Disennar UI (1)

HOME
  Login Pending:
   - redisennar pagina (1)
   - redirigir a la pagina adecuada (1)

  Displayer Pending:
  - cargar los eventos activos (1)
  - Abrir el displayer que quieras (1)

  UI Pending:
  - Disennar la pagina


- poner imagen no-image (ver si se pone una femenina y otra masculina para los pilots y jueces)
- los tabs tengan una url #tabname y se puda ir directo a ellos(poner links en el dashboard
ex: para una season: links a la derecha para view, settings, ranking, delete)

- Notificar cuando se termino una accion y que se vea que se esta mandando a hacer algo al server (boton pase a processing...)


Security
- las paginas de admin no se vean por otro usuario
- si el Id no es correcto redirigir a dashboard
- eliminar los delete de las entidades imprescindibles
- mandar encriptada la info
- poner sufijo a los json


Mejoras graficas
- Add Team con problemas la directiva

Eficiencia
- Al terminar una season
    - borrar los settings
    - solo dejar los reportes en PDF y los rankings

IMPORT
    PILOTOS con problema en el IMPORT
     - Gill Schneider  (chequear enl glider)
     - PROBLEMA SPONSORS: Red Bull / DR Zip / Gin gliders / Salewa
     - PROBLEMA SPONSORS: Air-G / Mundaka Optics / Drift inovation / NEO / APO
     - PROBLEMA SPONSORS: Glorify / Rainyday / U-Turn / Xdreamfly
     - PROBLEMA SPONSORS: Icaro / Freegun / Drift innovation / Supair / Five Ten
     - PROBLEMA SPONSORS: AIRG Products / Supair / RR Acrowings
     - PROBLEMA SPONSORS: Turkish Aeronautical Association - Dragonfly



