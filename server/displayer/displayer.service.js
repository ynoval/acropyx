
//ADD REST service
var displayerRest = require('restler');

var config = require('../config/environment');

// var restUrl = 'http://localhost:3000/';
var restUrl = 'http://' + config.displayDomain + ':' + config.displayPort+'/';

var Season = require('../api/season/season.model');
var SeasonCtrl = require('../api/season/season.controller');
var Event = require('../api/event/event.model');

var Competition = require('../api/competition/competition.model');
var CompetitionCtrl = require('../api/competition/competition.controller');

var Run = require('../api/run/run.model');
var RunCtrl = require('../api/run/run.controller');

var Flight = require('../api/flight/flight.model');
var Pilot = require('../api/pilot/pilot.model');
var Team = require('../api/team/team.model');
var Country = require('../api/country/country.model');


var _this = this;

exports.lastActions = {};
exports.messageText = {};

//Tenant
function getTenantBySeason(seasonId, callback){
  Season.findById(seasonId, function(err, season){
    if (err) {  return callback(err, null);}
    return callback(null, {season: season.code.toLowerCase()});
  });
}
function getTenantByEvent(eventId, callback){
  Event.findById(eventId, function(err, event){
    if (err) { return callback(err, null);}
    getTenantBySeason(event.season, function(err, tenant){
      if(err){
        return callback(err, null);
      }
      tenant.event = event._id;
      return callback(null, tenant);
    });
  });
}
function getTenantByCompetition(competitionId, callback){
  Competition.findById(competitionId, function(err, competition){
    if (err) { return callback(err, null);}
    getTenantByEvent(competition.event, function(err, tenant){
      if(err){
        return callback(err, null);
      }
      tenant.competition = competition._id;
      return callback(null, tenant);
    });
  });
}
function getTenantByRun(runId, callback){
  Run.findById(runId, function(err, run){
    if (err) { return callback(err, null);}
    getTenantByCompetition(run.competition, function(err, tenant){
      if(err){
        return callback(err, null);
      }
      tenant.run = run._id;
      return callback(null, tenant);
    });
  });
}
function getTenantByFlight(flightId, callback){
  Flight.findById(flightId, function(err, flight){
    if (err) { return callback(err, null);}
    getTenantByRun(flight.run, function(err, tenant){
      if(err){
        return callback(err, null);
      }
      tenant.flight = flight._id;
      return callback(null, tenant);
    });
  });
}


function fillLogoUrls(tenant, callback){
  var logoInfo = {"leftLogos": [], "rightLogos": []};
  Season.findOne({code: tenant.season}, function(err, season){
    if (err){
      return callback(err, null);
    }
    if(season) {
      // if(season.isFAI) {
      //   //Add  FAI Logo
      //   logoInfo.leftLogos.push({'imageSrc': 'http://' + config.displayDomain + ':' + config.port + '/images/faiLogo.jpg'});
      // }
      //Set season sponsors logos
      season.sponsors.forEach(function (sponsor) {
        logoInfo.leftLogos.push({'imageSrc': 'http://' + config.displayDomain +':' + config.port + '/api/sponsors/' + sponsor + '/logo'});
      });
    }

    if(tenant.event){
      Event.findById(tenant.event, function(err, event){
        if(err){
          return callback(err, null)
        }
        //Add Event logo
        logoInfo.rightLogos.push({'imageSrc' : 'http://'+ config.displayDomain +':' + config.port + '/api/events/' + event._id + '/logo'});
        //Add event sponsors logos
        event.sponsors.forEach(function(sponsor){
          logoInfo.rightLogos.push({'imageSrc': 'http://' + config.displayDomain + ':' + config.port + '/api/sponsors/' + sponsor + '/logo'});
        });

        return callback(null, logoInfo);
      });
    } else {
      return callback(null, logoInfo);
    }
  });
}
function sendToDisplayer(tenant, url, json, callback){
  fillLogoUrls(tenant, function(err, logoInfo){
    if(err){
      return callback(err, false);
    }
    json.extraInfo = logoInfo;
    if (_this.messageText[tenant.season]){
      json.text = _this.messageText[tenant.season].text;
    }

    displayerRest.postJson(restUrl + url, json, {headers: {"Tenant": tenant.season}})
      .on('complete', function (result) {
        if (result instanceof Error) {
          return callback(result, false);
        }
        return callback(null, true);
      });
  });
}

function retransmitToDisplayer(tenant, callback){
  var action = _this.lastActions[tenant.season];
  if(action){
    switch (action.name){
      case 'SEASON_RANKING':{
        _this.showSeasonRankings(action.entities[0], callback);
        break;
      }

      case 'START_EVENT': {
        _this.startEvent(action.entities[0], callback);
        break;
      }
      case 'EVENT_RESULTS': {
        _this.showEventResults(action.entities[0], callback);
        break;
      }

      case 'START_COMPETITION': {
        _this.startCompetition(action.entities[0], callback);
        break;
      }
      case 'COMPETITION_RESULTS': {
        _this.showCompetitionResults(action.entities[0], callback);
        break;
      }

      case 'START_RUN': {
        _this.startRun(action.entities[0], callback);
        break;
      }
      case 'STARTING_ORDER': {
        _this.startingOrder(action.entities[0], callback);
        break;
      }
      case 'STARTING_ORDER_BATTLE': {
        _this.startingOrderBattle(action.entities[0], callback);
        break;
      }
      case 'RUN_RESULTS': {
        _this.showRunResults(action.entities[0], callback);
        break;
      }
      case 'RUN_BATTLE_RESULTS': {
        _this.showRunBattleResults(action.entities[0], callback);
        break;
      }

      case 'BATTLE': {
        _this.showBattle(action.entities[0], action.entities[1], callback);
        break;
      }
      case 'BATTLE_RESULTS': {
        _this.showBattleResults(action.entities[0], action.entities[1], callback);
        break;
      }

      case 'FLIGHT': {
        _this.showFlight(action.entities[0], callback);
        break;
      }
      case 'FLIGHT_RESULTS': {
        _this.showFlightResults(action.entities[0], callback);
        break;
      }
    }
  }
  // return callback(null, true);
}

//Competitor
function calculateAge (dateOfBirth) {
  var now = new Date();
  var current_year = now.getFullYear();
  var year_diff = current_year - dateOfBirth.getFullYear();
  var birthday_this_year = new Date(current_year, dateOfBirth.getMonth(), dateOfBirth.getDate());
  var has_had_birthday_this_year = (now >= birthday_this_year);

  return has_had_birthday_this_year? year_diff: year_diff - 1;
}

function getPilotBasicInformation(pilotId, callback){
  var result = {};
  Pilot.findById(pilotId)
    .populate('country')
    .exec(function(err, pilot){
      result.name = pilot.name;
      if(pilot.country){
        result.country = pilot.country.ioc;
      }
      return callback(null, result);
    });
}
function getPilotInformation(pilotId, callback){
  var result = {};
  var separator = '      ';
  Pilot.findById(pilotId)
    .populate('country')
    .populate('sponsors', 'name')
    .exec(function(err, pilot){
      result.name = pilot.name;
      if(pilot.country){
        result.country = pilot.country.ioc;
      }

      if(pilot.dateOfBirth){
        result.birthDate  =  'Age: ' + calculateAge(pilot.dateOfBirth);
      }
      //TODO: FIX!!
      result.imageSrc =  'http://'+ config.displayDomain + ':' + config.port + '/api/pilots/' + pilot._id + '/picture';

      if(pilot.flyingSinceYear && pilot.flyingSinceYear.getFullYear() > 1950){
        result.flyingSince = 'Flying since: ' + pilot.flyingSinceYear.getFullYear();
      }

      if(pilot.glide){
        result.glider = 'Glider: ' + pilot.glide; //TODO: FIX database
      }

      if(pilot.sponsors && pilot.sponsors.length > 0){
        result.sponsor = 'Sponsors: ';
        pilot.sponsors.forEach(function(sponsor){
          result.sponsor += sponsor.name + ' / ';
        });
        result.sponsor = result.sponsor.slice(0, result.sponsor.length - 3);
      }

      if (pilot.civlRank && pilot.civlRank < 1000){
        result.ranking = 'CIVL world ranking: ' + pilot.civlRank;
      }

      if(pilot.civlId){
        result.civlId = 'CIVL ID: ' + pilot.civlId;
      }

      result.ticker = 'Pilot: ' + pilot.name;
      if (pilot.job){
        result.ticker += separator + 'Job: ' + pilot.job;
      }
      if(pilot.civlRank && pilot.civlRank < 1000){
        result.ticker += separator + 'CIVL World Ranking: ' + pilot.civlRank;
      }
      if(pilot.glide){
        result.ticker += separator + 'Glider: ' + pilot.glide;
      }

      if (result.sponsor && result.sponsor.length > 0){
        result.ticker += separator +  result.sponsor;
      }

      if(pilot.bestResults){
        result.ticker += separator + 'Best result: '  + pilot.bestResults;
      }

      if(pilot.flyingSinceYear && pilot.flyingSinceYear.getFullYear() > 1950){
        result.ticker += separator + 'Flying since: ' + pilot.flyingSinceYear.getFullYear();
      }

      return callback(null, result);
    });
}
function getTeamBasicInformation(teamId, callback){
  var result = {};
  var step = require('step');
  step(
    function(){
      Team.findById(teamId)
        .populate('pilots', '-picture')
        .exec(this);
    },
    function(err, team){
      result.name = team.name;
      getPilotBasicInformation(team.pilots[0], this.parallel());
      getPilotBasicInformation(team.pilots[1], this.parallel());
    },
    function (err, firstPilot, secondPilot){
      result.pilot1 = firstPilot.name;
      result.country1 = firstPilot.country;
      result.pilot2 = secondPilot.name;
      result.country2 = secondPilot.country;
      return callback(null, result);
    }
  );
}
function getTeamInformation(teamId, callback){
  var result = {
    pilots:[]
  };
  var step = require('step');
  step(
    function(){
      Team.findById(teamId, this);
    },
    function (err, team){
      result.name = team.name;
      if(team.bestResults){
        result.bestResult = 'Best results: ' + team.bestResults;
      }

      getPilotInformation(team.pilots[0], this.parallel());
      getPilotInformation(team.pilots[1], this.parallel());
    },
    function(err, pilot1, pilot2){
      result.pilots.push(pilot1);
      result.pilots.push(pilot2);
      return callback(null, result);
    }
  );
}


//SEASON
function getRankingSoloCompetitors(rankCompetitors, callback){
  var rankingCompetitors = [];
  var step = require('step');
  step(
    function (){
      var group = this.group();
      rankCompetitors.forEach(function (rankCompetitor) {
        getPilotBasicInformation(rankCompetitor.competitor._id, group());
      });
    },
    function(err, competitorsInfo) {
      for(var i = 0; i < rankCompetitors.length; i++) {
        var competitorResult = {
          name: competitorsInfo[i].name,
          country: competitorsInfo[i].country,
          mark: rankCompetitors[i].points.toFixed(3)
        };
        rankingCompetitors.push(competitorResult);
      }
      return callback(null,rankingCompetitors );
    }
  );
}
function getRankingSynchroCompetitors(rankCompetitors, callback){
  var rankingCompetitors = [];
  var step = require('step');
  step(
    function (){
      var group = this.group();
      rankCompetitors.forEach(function (rankCompetitor) {
        getTeamBasicInformation(rankCompetitor.competitor._id, group());
      });
    },
    function(err, competitorsInfo) {
      for(var i = 0; i < rankCompetitors.length; i++) {
        var competitorResult = {
          name: competitorsInfo[i].name,
          mark: rankCompetitors[i].points.toFixed(3)
        };
        if (competitorsInfo[i].pilot1.length > competitorsInfo[i].pilot2.length) {
          competitorResult.pilot1 = competitorsInfo[i].pilot1;
          competitorResult.country1 = competitorsInfo[i].country1;
          competitorResult.pilot2 = competitorsInfo[i].pilot2;
          competitorResult.country2 = competitorsInfo[i].country2;
        } else {
          competitorResult.pilot1 = competitorsInfo[i].pilot2;
          competitorResult.country1 = competitorsInfo[i].country2;
          competitorResult.pilot2 = competitorsInfo[i].pilot1;
          competitorResult.country2 = competitorsInfo[i].country1;
        }
        rankingCompetitors.push(competitorResult);
      }
      return callback(null, rankingCompetitors);
    }
  );
}
exports.showSeasonRankings = function(seasonId, callback){
  getTenantBySeason(seasonId, function(err, tenant) {
    if (err) {
      return callback(err, false);
    }
    var json = {};

    var step = require('step');
    step(
      function () {

        Season.findById(seasonId, this.parallel());

        SeasonCtrl.getRankings(seasonId, this.parallel());

      },
      function (err, season, rankings) {
        if (err) {
          return callback(err, false);
        }

        if (rankings && rankings.solo) {
          if(rankings.solo.runsAmount === 0){
            json.ranking1 = {
              name: ''
            };
          } else {
            json.ranking1 = {
              name: season.name + '<br> Solo '
            };

            if (season.isEnded) {
              json.ranking1.name += ' Final Ranking '
            } else {
              var runsSoloText  = (rankings.solo.runsAmount === 1)? '(after one run)': '(after ' + rankings.solo.runsAmount + ' runs)';
              json.ranking1.name += ' Intermediate Ranking ' + runsSoloText;
            }
            getRankingSoloCompetitors(rankings.solo.competitors, this.parallel());
          }
        }

        if (rankings && rankings.synchro) {
          if(rankings.synchro.runsAmount === 0){
            json.ranking2 = {
              name: ''
            };
          } else {
            json.ranking2 = {
              //TODO: Check season ended
              name: season.name + '<br> Synchro'
            };


            if (season.isEnded) {
              json.ranking2.name += ' Final Ranking'
            } else {
              var runsSynchroText  = (rankings.synchro.runsAmount === 1)? '(after one run)': '(after ' + rankings.synchro.runsAmount + ' runs)';
              json.ranking2.name += ' Intermediate Ranking ' + runsSynchroText;
            }

            getRankingSynchroCompetitors(rankings.synchro.competitors, this.parallel());
          }
        }
      }, function(err, competitorsSolo, competitorsSynchro){
        json.ranking1.competitors = competitorsSolo;
        json.ranking2.competitors = competitorsSynchro;

        var pageUrl = 'rotateRanking';

        sendToDisplayer(tenant, pageUrl, json, function (err, result) {
          _this.lastActions[tenant] = {name: 'SEASON_RANKING', entities: [seasonId]};
          return callback(err, result);
        });
      }
    );
  });
};
//EVENT
exports.startEvent = function(eventId, callback){
  getTenantByEvent(eventId, function(err, tenant){
    if (err) { return callback(err, false);}
    Event.findById(eventId, function(err, event) {
      if (err) {
        return callback(err, false);
      }
      var json = {
        name : event.name,
        text : 'The ' + event.name + ' event is about to start !'
      } ;

      sendToDisplayer(tenant, 'waiting', json, function(err, result) {
        _this.lastActions[tenant.season] = { name: 'START_EVENT', entities: [eventId] };
        return callback(err, result);
      });
    });
  });
};
//Rotate results
exports.showEventResults = function(eventId, callback){
  Competition.find({event: eventId})
    .populate('event', '-logo')
    .populate('type')
    .exec(function (err, competitions){
        if (err){
          return callback(err, false);
        }
        if (!competitions || competitions.length === 0){
          return callback(null, false);
        }
        if(competitions.length === 1){
          _this.showCompetitionResults(competitions[0]._id, callback);
        } else {
          getTenantByEvent(eventId, function (err, tenant) {
            if (err) {
              return callback(err, false);
            }

            var pageUrl = 'rotateResults';

            var resultType1 = 'Intermediate standings';
            if (competitions[0].endTime){
              resultType1 = 'Final overall standings';
            }

            var resultType2 = 'Intermediate standings';
            if (competitions[1].endTime){
              resultType2 = 'Final overall standings';
            }

            var json = {
              competition1: {
                "message": "Show " + competitions[0].name + " results in ",
                "name": competitions[0].name + '<br>' + resultType1,
                "competitors": []
              },
              competition2: {
                "message": "Show " + competitions[1].name + " results in ",
                "name": competitions[1].name + '<br>' + resultType2,
                "competitors": []
              }
            };

            var step = require('step');
            step(
              function(){
                getCompetitionResults(competitions[0], this.parallel());
                getCompetitionResults(competitions[1], this.parallel());
              }, function(err, results1, results2){
                json.competition1.competitors = results1;
                json.competition2.competitors = results2;
                sendToDisplayer(tenant, pageUrl, json,
                  function (err, result) {
                    _this.lastActions[tenant.season] = {name: 'EVENT_RESULTS', entities: [eventId]};
                    return callback(err, result);
                }
                );
              }
            );
          });
        }
      }
    );
};
exports.showEventResultsERROR = function(eventId, callback){
  Competition.find({event: eventId})
    .populate('event', '-logo')
    .populate('type')
    .exec(function (err, competitions){
      if (err){
        return callback(err, false);
      }
      if (!competitions || competitions.length === 0){
        return callback(null, false);
      }
      if(competitions.length === 1){
        _this.showCompetitionResults(competitions[0]._id, callback);
      } else {
        getTenantByEvent(eventId, function(err, tenant) {
          if (err) {
            return callback(err, false);
          }

          var pageUrl = 'rotateResults';

          var json = {
            "competitions":[]
          };

          var step = require('step');
          step(
            function(){
              var group = this.group();
              competitions.forEach(function(competition){
                var resultType = 'Intermediate standings';
                if (competition.endTime){
                  resultType = 'Final overall standings';
                }
                json.competitions.push({
                  "message": "Show " + competition.name + " results in ",
                  "name": competition.name + '<br>' + resultType,
                  "competitors": []
                });
                getCompetitionResults(competition, group());
              });
            },
            function(err, results){
              for (var i = 0; i < results.length; i++){
                json.competitions[i].competitors = results[i];
              }

              sendToDisplayer(tenant, pageUrl, json, function(err, result) {
                _this.lastActions[tenant.season] = { name: 'EVENT_RESULTS', entities: [eventId] };
                return callback(err, result);
              });
            }
          );
        });
      }
    }
  );
};
//COMPETITION
exports.startCompetition = function(competitionId, callback){
  getTenantByCompetition(competitionId, function(err, tenant){
    if (err) { return callback(err, false);}
    Competition.findById(competitionId, function(err, competition) {
      if (err) {
        return callback(err, false);
      }
      var json = {
        name: competition.name,
        text: 'The ' + competition.name + ' competition is about to start !'
      };
      sendToDisplayer(tenant, 'waiting', json, function(err, result) {
        _this.lastActions[tenant.season] = { name: 'START_COMPETITION', entities: [competitionId] };
        return callback(err, result);
      });
    });
  });
};
exports.showCompetitionResults = function(competitionId, callback){
  var displayTenant= '';
  var competitionTypeName = '';
  var json = {
    competitors: []
  };
  var pageUrl = 'resultRun';
  var step = require('step');
  step(
    function(){
      getTenantByCompetition(competitionId, this.parallel());
      Competition.findById(competitionId)
        .populate('event', '-logo')
        .populate('type')
        .exec(this.parallel());
    },
    function(err, tenant, competition){
      displayTenant = tenant;
      competitionTypeName = competition.type.name;
      var resultType = 'Intermediate standings';
      if (competition.endTime){
        resultType = 'Final overall standings';
      }
      json.name = competition.name + '<br>' + resultType;
      getCompetitionResults(competition, this);
    },
    function (err, competitorResults) {
      json.competitors = competitorResults;
      sendToDisplayer(displayTenant, pageUrl, json, function(err, result){
        _this.lastActions[displayTenant.season] = { name: 'COMPETITION_RESULTS', entities: [competitionId] };
        return callback(err, result);
      });
    }
  );
};

function getCompetitionResults(competition, callback){
  var displayResults = [];
  var competitionTypeName = competition.type.name;

  var step = require('step');
  step(
    function(){
      CompetitionCtrl.getResults(competition._id, this);
    },
    function (err, competitionResults) {
      displayResults = competitionResults;
      var group = this.group();
      displayResults.forEach(function (result) {
        if (competitionTypeName === "Solo") {
          getPilotBasicInformation(result.competitor._id, group());
        } else {
          getTeamBasicInformation(result.competitor._id, group());
        }
      });
    },
    function(err, competitors) {
      var competitionResults = [];
      for(var i = 0; i < displayResults.length; i++) {
        var competitorResult = {
          name: competitors[i].name,
          warnings: displayResults[i].warnings,
          nbRuns: displayResults[i].runResults.length,
          mark: displayResults[i].overall.toFixed(3)
        };
        if (competitionTypeName === 'Solo') {
          competitorResult.country = competitors[i].country;
        } else {
          if (competitors[i].pilot1.length > competitors[i].pilot2.length) {
            competitorResult.pilot1 = competitors[i].pilot1;
            competitorResult.country1 = competitors[i].country1;
            competitorResult.pilot2 = competitors[i].pilot2;
            competitorResult.country2 = competitors[i].country2;
          } else {
            competitorResult.pilot1 = competitors[i].pilot2;
            competitorResult.country1 = competitors[i].country2;
            competitorResult.pilot2 = competitors[i].pilot1;
            competitorResult.country2 = competitors[i].country1;
          }
        }

        competitionResults.push(competitorResult);
      }

      return callback(null, competitionResults);
    }
  );
}

//RUN
exports.startRun = function(runId, callback){
  getTenantByRun(runId, function(err, tenant){
    if (err) { return callback(err, false);}
    Run.findById(runId)
      .populate('competition', 'name')
      .exec(function(err, run) {
      if (err) {
        return callback(err, false);
      }

      var json = {
        name : run.competition.name + '<br>' + 'Run ' + run.name,
        text : 'The ' + run.name + ' run is about to start !'
      } ;

      sendToDisplayer(tenant, 'waiting', json, function(err, result) {
        _this.lastActions[tenant.season] = { name: 'START_RUN', entities: [runId] };
        return callback(err, result);
      });
    });
  });
};
exports.startingOrder = function(runId, callback){
  var displayTenant = '';
  var displayRun = {};
  var json = {competitors:[]};
  var pageUrl = 'startOrderRun';
  var competitionTypeName = '';
  var step = require('step');
  step(
    function(){
      getTenantByRun(runId,this.parallel());
      Run.findById(runId, this.parallel());
    },
    function(err, tenant, run){
      displayTenant = tenant;
      displayRun = run;
      Competition.findById(displayRun.competition)
        .populate('type')
        .exec(this.parallel());
      Flight.find({run: displayRun._id})
        .sort('order')
        .exec(this.parallel());
    },
    function(err, competition, flights){
      json.name = competition.name + '<br> Run ' + displayRun.name + ' - Starting Order';
      competitionTypeName = competition.type.name;
      var group = this.group();
      flights.forEach(function(flight){
        if( competitionTypeName === "Solo"){
          getPilotBasicInformation(flight.competitor._id, group());
        } else{
          getTeamBasicInformation(flight.competitor._id, group());
        }
      });
    },
    function(err, competitors){
      json.competitors = competitors;
      sendToDisplayer(displayTenant, pageUrl, json, function(err, result) {
        _this.lastActions[displayTenant.season] = { name: 'STARTING_ORDER', entities: [runId] };
        return callback(err, result);
      });
    }
  );
};
exports.startingOrderBattle = function(runId, callback){
  var displayTenant = '';
  var displayRun = {};
  var json = {competitors:[]};
  var pageUrl = 'startOrderRun';
  var competitionTypeName = '';
  var step = require('step');
  step(
    function(){
      getTenantByRun(runId,this.parallel());
      Run.findById(runId, this.parallel());
    },
    function(err, tenant, run){
      displayTenant = tenant;
      displayRun = run;
      Competition.findById(displayRun.competition)
        .populate('type')
        .exec(this.parallel());
      Flight.find({run: displayRun._id})
        .sort('order')
        .exec(this.parallel());
    },
    function(err, competition, flights){
      json.name = competition.name + '<br> Run ' + displayRun.name + ' - Starting Order';
      competitionTypeName = competition.type.name;
      var group = this.group();
      flights.forEach(function(flight){
        if( competitionTypeName === "Solo"){
          getPilotBasicInformation(flight.competitor._id, group());
        } else{
          getTeamBasicInformation(flight.competitor._id, group());
        }
      });
    },
    function(err, competitors){
      var battle = {};
      var index = 0;
      //TODO: Only accept Battles in runs with even competitors amount
      var middle = competitors.length / 2;
      while(index < middle ){
        json.competitors.push(
          {
            competitor1: competitors[middle - (index + 1)],
            competitor2: competitors[middle + index]
          }
        );
        index++;
      }

      sendToDisplayer(displayTenant, pageUrl, json, function(err, result) {
        _this.lastActions[displayTenant.season] = { name: 'STARTING_ORDER_BATTLE', entities: [runId] };
        return callback(err, result);
      });
    }
  );

  // getTenantByRun(runId, function(err, tenant){
  //   if (err) { return callback(err, false);}
  //   Run.findById(runId, function(err, run) {
  //     if (err) {
  //       return callback(err, false);
  //     }
  //     Competition.findById(run.competition)
  //       .populate('type')
  //       .exec(function(err, competition){
  //         var json = {
  //           name: competition.name + '<br> Run ' + run.name + ' - Starting Order',
  //           competitors: []
  //         };
  //
  //         sendToDisplayer(tenant, 'startOrderRun',json, function(err, result) {
  //           return callback(err, result);
  //         });
  //       });
  //   });
  // });
};
function setQualifyStatus(run, competitorsResults, callback){
  var qualifyStatusList = [];
  RunCtrl.getQualifyCompetitors(run._id, function(err, qualifyCompetitors){
     for(var i = 0; i< competitorsResults.length; i++){
       var index = 0;
       var found = false;
       while( index < qualifyCompetitors.length && !found){
         if(String(qualifyCompetitors[index].competitor._id) === String(competitorsResults[i].competitor._id)){
           found = true;
         }
         index++;
       }
       if(found){
         qualifyStatusList.push(0);
       } else {
         qualifyStatusList.push(-1);
       }
     }
     return callback(null, qualifyStatusList);
  });
}

exports.showRunResults = function(runId, callback){
  var displayTenant= '';
  var competitionTypeName = '';
  var displayRun = {};
  var displayFlightResults = [];
  var json = {
    competitors: []
  };
  var pageUrl = 'resultRun';
  var step = require('step');
  step(
    function(){
      getTenantByRun(runId, this.parallel());
      Run.findById(runId)
        .populate('competition')
        .populate('type')
        .exec(this.parallel());
    },
    function(err, tenant, run){
      displayTenant = tenant;
      displayRun = run;
      Competition.findById(displayRun.competition._id)
        .populate('type')
        .exec(this.parallel());
      RunCtrl.getResults(displayRun._id, this.parallel());
      Run.count({competition: displayRun.competition._id}, this.parallel());

    },
    function(err, competition, flightResults, runsAmount){
      var resultType = 'Intermediate results';
      if (displayRun.endTime){
        resultType = 'Final standings';
      }
      json.name = displayRun.competition.name + '<br> Run ' + displayRun.name + ' - ' + resultType;
      competitionTypeName = competition.type.name;
      displayFlightResults = flightResults;
      if(!displayRun.endTime || displayRun.order  === runsAmount || displayRun.qualifiedForNextRun === 0){
        this(null,[]);
      } else {
        setQualifyStatus(displayRun, flightResults, this);
      }
    } ,
    function (err, qualifyStatusList) {
      for(var i = 0; i < qualifyStatusList.length; i++){
        displayFlightResults[i].qualifyStatus = qualifyStatusList[i];
      }

      var group = this.group();
      displayFlightResults.forEach(function (flight) {
        if ( competitionTypeName === 'Solo') {
          getPilotBasicInformation(flight.competitor._id, group());
        } else {
          getTeamBasicInformation(flight.competitor._id, group());
        }
      });
    },
    function(err, competitors) {
      for (var i = 0; i < displayFlightResults.length; i++) {
        var competitorResult = {
          name: competitors[i].name,
          warnings: displayFlightResults[i].warnings,
          mark: displayFlightResults[i].computeResult.toFixed(3)
        };

        if(displayFlightResults[i].qualifyStatus !== undefined){
          competitorResult.qualifyStatus =  displayFlightResults[i].qualifyStatus;
        }
        if (competitionTypeName === 'Solo') {
          competitorResult.country = competitors[i].country;
        } else {
          if (competitors[i].pilot1.length > competitors[i].pilot2.length) {
            competitorResult.pilot1 = competitors[i].pilot1;
            competitorResult.country1 = competitors[i].country1;
            competitorResult.pilot2 = competitors[i].pilot2;
            competitorResult.country2 = competitors[i].country2;
          } else {
            competitorResult.pilot1 = competitors[i].pilot2;
            competitorResult.country1 = competitors[i].country2;
            competitorResult.pilot2 = competitors[i].pilot1;
            competitorResult.country2 = competitors[i].country1;
          }
        }
        json.competitors.push(competitorResult);
      }
      sendToDisplayer(displayTenant, pageUrl, json, function(err, result){
        _this.lastActions[displayTenant.season] = { name: 'RUN_RESULTS', entities: [runId] };
        return callback(err, result);
      });
    }
  );
};
exports.showRunBattleResults = function(runId, callback){
  var displayTenant= '';
  var competitionTypeName = '';
  var displayRun = {};
  var displayFlightResults = [];
  var totalFlights = 0;
  var json = {
    competitors: []
  };
  var pageUrl = 'resultRun';
  var step = require('step');
  step(
    function(){
      getTenantByRun(runId, this.parallel());
      Run.findById(runId)
        .populate('competition')
        .populate('type')
        .exec(this.parallel());
    },
    function(err, tenant, run){
      displayTenant = tenant;
      displayRun = run;
      Competition.findById(displayRun.competition._id)
        .populate('type')
        .exec(this.parallel());
      RunCtrl.getResults(displayRun._id, this.parallel());
      Flight.count({run: displayRun._id}, this.parallel());
    },
    function(err, competition, flightResults, flightsAmount){
      totalFlights = flightsAmount;
      var resultType = 'Intermediate results';
      json.name = displayRun.competition.name + '<br> Run  ' + displayRun.name + ' - ' + resultType;
      competitionTypeName = competition.type.name;
      displayFlightResults = flightResults;
      var group = this.group();
      displayFlightResults.forEach(function (flight) {
        if ( competitionTypeName === "Solo") {
          getPilotBasicInformation(flight.competitor._id, group());
        } else {
          getTeamBasicInformation(flight.competitor._id, group());
        }
      });
    },
    function(err, competitors) {
      //var sortedFlights = _.sortBy(displayFlightResults, 'order');
      //TODO: Check even length
      var middle =  totalFlights/2;
      for (var i = 0; i < middle; i++) {
        var firstCompetitorIndex = -1;
        var secondCompetitorIndex = -1;
        var found = 0;
        var j = 0;
        while (j < displayFlightResults.length && found < 2) {
          if (displayFlightResults[j].order === middle - i -1) {
            firstCompetitorIndex = j;
            found++;
          } else {
            if (displayFlightResults[j].order === middle + i) {
              secondCompetitorIndex = j;
              found++;
            }
          }
          j++;
        }
        if (found === 2) {
          var battleResult = {
            competitor1: {
              name: competitors[firstCompetitorIndex].name,
              warnings: displayFlightResults[firstCompetitorIndex].warnings,
              mark: displayFlightResults[firstCompetitorIndex].computeResult.toFixed(3),
              country: competitors[firstCompetitorIndex].country,
              classified: displayFlightResults[firstCompetitorIndex].computeResult >  displayFlightResults[secondCompetitorIndex].computeResult ? 1 : 0
            },
            competitor2: {
              name: competitors[secondCompetitorIndex].name,
              warnings: displayFlightResults[secondCompetitorIndex].warnings,
              mark: displayFlightResults[secondCompetitorIndex].computeResult.toFixed(3),
              country: competitors[secondCompetitorIndex].country,
              classified: displayFlightResults[secondCompetitorIndex].computeResult >  displayFlightResults[firstCompetitorIndex].computeResult ? 1 : 0
            }
          };
          json.competitors.push(battleResult);
        }
      }
      sendToDisplayer(displayTenant, pageUrl, json, function(err, result){
        _this.lastActions[displayTenant.season] = { name: 'RUN_BATTLE_RESULTS', entities: [runId] };
        return callback(err, result);
      });
    }
  );
};

//BATTLE
exports.showBattle = function(firstFlightId, secondFlightId, callback){
  var displayTenant= '';
  var competitionTypeName = '';
  var displayFirstFlight = {};
  var displaySecondFlight = {};
  var json = {};
  var pageUrl = '';
  var step = require('step');
  step(
    function(){
      getTenantByFlight(firstFlightId, this.parallel());
      Flight.findById(firstFlightId)
        .populate('run')
        .exec(this.parallel());
      Flight.findById(secondFlightId)
        .populate('run')
        .exec(this.parallel());
    },
    function(err, tenant, firstFlight, secondFlight){
      displayTenant = tenant;
      displayFirstFlight = firstFlight;
      displaySecondFlight = secondFlight;
      Competition.findById(firstFlight.run.competition)
        .populate('event', '-logo')
        .populate('type')
        .exec(this);
    },
    function (err, competition){
      competitionTypeName = competition.type.name;
      json.name = competition.name + "<br> Run " +  displayFirstFlight.run.name;
      if( competitionTypeName === "Solo"){
        pageUrl = 'startBattleSolo';
        getPilotInformation(displayFirstFlight.competitor._id, this.parallel());
        getPilotInformation(displaySecondFlight.competitor._id, this.parallel());
      } else{
        pageUrl = 'startBattleTeam';
        getTeamInformation(displayFirstFlight.competitor._id, this.parallel());
        getTeamInformation(displaySecondFlight.competitor._id, this.parallel());
      }
    },
    function(err, firstCompetitor, secondCompetitor){
      json.competitor1 = firstCompetitor;
      json.competitor2 = secondCompetitor;

      sendToDisplayer(displayTenant, pageUrl, json, function(err, result){
        _this.lastActions[displayTenant.season] = { name: 'BATTLE', entities: [firstFlightId, secondFlightId] };
        return callback(err, result);
      });
    }
  );
};
exports.showBattleResults = function(firstFlightId, secondFlightId, callback){
  var displayTenant= '';
  var competitionTypeName = '';
  var displayFirstFlight = {};
  var displaySecondFlight = {};
  var json = {};
  var pageUrl = '';
  var step = require('step');
  step(
    function(){
      getTenantByFlight(firstFlightId, this.parallel());
      Flight.findById(firstFlightId)
        .populate('run')
        .exec(this.parallel());
      Flight.findById(secondFlightId)
        .populate('run')
        .exec(this.parallel());
    },
    function(err, tenant, firstFlight, secondFlight){
      displayTenant = tenant;
      displayFirstFlight = firstFlight;
      displaySecondFlight = secondFlight;
      Competition.findById(firstFlight.run.competition)
        .populate('event', '-logo')
        .populate('type')
        .exec(this);
    },
    function (err, competition){
      competitionTypeName = competition.type.name;
      json.name = competition.name + "<br> Run " +  displayFirstFlight.run.name;
      if( competitionTypeName === "Solo"){
        pageUrl = 'resultBattleSolo';
        getPilotInformation(displayFirstFlight.competitor._id, this.parallel());
        getPilotInformation(displaySecondFlight.competitor._id, this.parallel());
      } else{
        pageUrl = 'resultBattleTeam';
        getTeamInformation(displayFirstFlight.competitor._id, this.parallel());
        getTeamInformation(displaySecondFlight.competitor._id, this.parallel());
      }
    },
    function(err, firstCompetitor, secondCompetitor){
      json.competitor1 = firstCompetitor;
      json.competitor2 = secondCompetitor;

      json.results= {
        "result1": displayFirstFlight.computeResult.toFixed(3),
        "result2": displaySecondFlight.computeResult.toFixed(3)
      };

      if(displayFirstFlight.computeResult > displaySecondFlight.computeResult) {
        json.results.classified1 = 1;
        json.results.classified2 = 0;
      }

      if(displayFirstFlight.computeResult < displaySecondFlight.computeResult) {
        json.results.classified1 = 0;
        json.results.classified2 = 1;
      }

      sendToDisplayer(displayTenant, pageUrl, json, function(err, result){
        _this.lastActions[displayTenant.season] = { name: 'BATTLE_RESULTS', entities: [firstFlightId, secondFlightId] };
        return callback(err, result);
      });
    }
  );
};

//FLIGHT
exports.showFlight = function(flightId, callback){
  var displayTenant= '';
  var competitionTypeName = '';
  var displayFlight = {};
  var json = {};
  var pageUrl = '';
  var step = require('step');
  step(
    function(){
      getTenantByFlight(flightId, this.parallel());
      Flight.findById(flightId)
        .populate('run')
        .exec(this.parallel());
    },
    function(err, tenant, flight){
      displayTenant = tenant;
      displayFlight = flight;
      Competition.findById(displayFlight.run.competition)
        .populate('event', '-logo')
        .populate('type')
        .exec(this);
    },
    function (err, competition){
      competitionTypeName = competition.type.name;
      json.name = competition.name + "<br> Run " +  displayFlight.run.name;
      if( competitionTypeName === "Solo"){
        pageUrl = 'currentFlightSolo';
        getPilotInformation(displayFlight.competitor._id, this);
      } else{
        pageUrl = 'currentFlightTeam';
        getTeamInformation(displayFlight.competitor._id, this);
      }
    },
    function(err, competitor){
      if( competitionTypeName === "Solo"){
        json.competitor = competitor;
      } else{
        json.team = competitor;
      }
      sendToDisplayer(displayTenant, pageUrl, json, function(err, result){
        _this.lastActions[displayTenant.season] = { name: 'FLIGHT', entities: [flightId] };
        return callback(err, result);
      });
    }
  );
};
exports.showFlightResults = function(flightId, callback){

  var displayTenant= '';
  var competitionTypeName = '';
  var displayFlight = {};
  var json = {};
  var pageUrl = '';
  var step = require('step');
  step(
    function(){
      getTenantByFlight(flightId, this.parallel());
      Flight.findById(flightId)
        .populate('run')
        .populate('detailResults.markType', 'name')
        .exec(this.parallel());

    },
    function(err, tenant, flight){
      displayTenant = tenant;
      displayFlight = flight;
      Competition.findById(displayFlight.run.competition)
        .populate('event', '-logo')
        .populate('type')
        .exec(this);

    },
    function (err, competition){
      competitionTypeName = competition.type.name;
      json.name = competition.name + "<br> Run " +  displayFlight.run.name;
      if( competitionTypeName === "Solo"){
        pageUrl = 'resultFlightSolo';
        getPilotInformation(displayFlight.competitor._id, this);
      } else{
        pageUrl = 'resultFlightTeam';
        getTeamInformation(displayFlight.competitor._id, this);
      }
    },
    function(err, competitor){
      if( competitionTypeName === "Solo"){
        json.competitor = competitor;
      } else{
        json.team = competitor;
      }
      json.overall = { "kind" : "TOTAL POINTS", "value" : displayFlight.computeResult.toFixed(3)};
      json.warnings = displayFlight.warnings;
      var countBonus = 0;
      var countMalus = 0;
      displayFlight.manoeuvres.forEach(function(manoeuvre){
        if(manoeuvre.applyBonus){
          countBonus++;
        }
        if(manoeuvre.applyMalus){
          countMalus++;
        }
      });
      json.bonus = countBonus;
      json.malus = countMalus;
      json.marks = [];
      displayFlight.detailResults.forEach(function(result){
        json.marks.push({"kind" : result.markType.name, "value" : result.value.toFixed(3)});
      });


      Flight.count({run: displayFlight.run._id, computeResult: {$gte: displayFlight.computeResult}}, function(err, rank){
        json.rank = rank;
        sendToDisplayer(displayTenant, pageUrl, json, function(err, result){
          _this.lastActions[displayTenant.season] = { name: 'FLIGHT_RESULTS', entities: [flightId] };
          return callback(err, result);
        });
      });
    }
  );
};

//PAF
exports.showPaf = function(eventId, pafText, callback){
  getTenantByEvent(eventId, function(err, tenant){
    if (err) { return callback(err, false);}
    //TODO: Add different message types
    var decay = 10;  // 7
//            if (params.sticky) {
//                decay = -1
//            }
    var json = {
      "text": pafText,
      "style": "decay: -1"
    };

    displayerRest.postJson(restUrl + 'paf', json, {headers: {"Tenant": tenant.season}})
      .on('complete', function (result) {
        if (result instanceof Error) {
          return callback(result, false);
        }
        return callback(null, true);
      });
  });
};
exports.clearPaf = function(eventId, callback){
  getTenantByEvent(eventId, function(err, tenant){
    if (err) { return callback(err, false);}
    sendToDisplayer(tenant, 'pafclean', {}, function(err, result){
      return callback(err, result);
    });
  });
};

//MESSAGE
exports.showMessage = function(eventId, text, callback){
 // _this.startEvent(eventId, callback);
  getTenantByEvent(eventId, function(err, tenant){
    _this.messageText[tenant.season] = {text: text};
    retransmitToDisplayer(tenant, function(err, result){
     callback(err, result);
    });
  });
};
exports.clearMessage = function(eventId, callback){
//  _this.startEvent(eventId, callback);
  getTenantByEvent(eventId, function(err, tenant){
    _this.messageText[tenant.season] = { text: ' '};
    retransmitToDisplayer(tenant, function(err, result){
      callback(err, result);
    });
  });
};


