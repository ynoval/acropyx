'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var LocationSchema = new Schema({
  name: String,
  latitude: Number,
  longitude: Number,
  comment: String
});

var EventSchema = new Schema({
  //Event name
  name: {type: String, unique:true},
  logo: {data: Buffer, contentType: String},
  gallery: [{data: Buffer, contentType: String, order: Number}],
  season: {type: mongoose.Schema.Types.ObjectId, ref: 'Season'},
  startDate: Date,
  endDate: Date,
  webPage: String,
  //TODO: Add in future versions
  //judges:[{type:mongoose.Schema.Types.ObjectId, ref: 'Judge'}],
  sponsors: [{type:mongoose.Schema.Types.ObjectId, ref: 'Sponsor'}],
  locations:[LocationSchema],
  isConfirmed: {type: Boolean, default: true},
  isStarted: { type: Boolean, default: false},
  isEnded: { type: Boolean, default: false}
});

module.exports = mongoose.model('Event', EventSchema);
