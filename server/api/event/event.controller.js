/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /events              ->  index
 * POST    /events              ->  create
 * GET     /events/:id          ->  show
 * PUT     /events/:id          ->  update
 * DELETE  /events/:id          ->  destroy
 */

'use strict';

var _ = require('lodash');
var Event = require('./event.model');
var Season = require('../season/season.model');
var Competition = require('../competition/competition.model');
var CompetitionCtrl = require('../competition/competition.controller');
var Displayer = require('../../displayer/displayer.service');
var Run = require('../run/run.model');
var Flight = require('../flight/flight.model');

var _this = this;

// Get list of events
exports.index = function(req, res) {
  Event.find({}, {logo : 0})
    .sort('name')
    .populate('season', 'name')
    .exec(function (err, events) {
    if(err) { return handleError(res, err); }
    return res.json(200, events);
  });
};

function findActiveRun(competitionId, callback){
  Run.findOne({competition: competitionId, startTime:{$ne: null}, endTime: null})
    .populate('type')
    .exec(function(err, run){
    if (err){
      return callback(err, null);
    }

    if(run){
      return callback(null, run);
    }
    //No activeRun - find next Run
    Run
      .find({competition: competitionId, startTime: null})
      .populate('type')
      .sort({'order': 1})
      .limit(1)
      .exec(function(err, runs) {
        if (err){
          return callback(err, null);
        }
        if(!runs || runs.length === 0){
          return callback(null, null);
        }
        return callback(null, runs[0]);
      });
  });
}

function fillActiveRun(competition, callback){

  findActiveRun(competition._id, function(err, run){
    if (err){
      return callback(err,null);
    }
    if (!run){
      return callback(err, competition);
    }

    //Fill Flights
    Flight.find({run: run._id})
      .populate('manoeuvres.manoeuvre')
      .exec(function(err, flights){
      run._doc.flights = flights;
      competition._doc.activeRun = run;
      return callback(null, competition);
    });

  });
}

// Get list of active competitions (include current Run information)
exports.activeCompetitions = function(req, res) {
  Competition.find({event: req.params.id, startTime:{$ne: null}, endTime: null}, function (err, competitions) {
    if(err) { return handleError(res, err); }
    //Fill ActiveRun
    var Step = require('step');
    Step(
      function(){
        var group = this.group();
        _.each(competitions, function(competition){
          fillActiveRun(competition, group());
        });
      },
      function(err, competitions){
        if(err) { return handleError(res, err); }
        return res.json(200, competitions);
      }
    );
  });
};

// Get list of competitions
exports.competitions = function(req, res) {
  Competition.find({event: req.params.id}, function (err, competitions) {
    if(err) { return handleError(res, err); }
    return res.json(200, competitions);
  });
};

// Get a single event
exports.show = function(req, res) {
  Event
    .findById(req.params.id, {logo: 0})
    .populate('season','name code')
    //Todo: Add in future versions
    //.populate('judges', 'name')
    .populate('sponsors', 'name')
    .exec(function (err, event) {
    if(err) { return handleError(res, err); }
    if(!event) { return res.send(404); }
    return res.json(event);
  });
};

// Creates a new event in the DB.
exports.create = function(req, res) {
  Event.create(req.body, function(err, event) {
    if(err) { return handleError(res, err); }
    return res.json(201, event);
  });
};

// Updates an existing event in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  Event.findById(req.params.id, {logo: 0})
    .exec(function (err, event) {
    if (err) { return handleError(res, err); }
    if(!event) { return res.send(404); }
    //TODO: FIX!!
    //Saving sponsors
    req.body.sponsors = req.body.sponsors.map(function(option) { return option._id; });
    event.sponsors = req.body.sponsors;
    req.body.sponsors = [];
    //TODO: Add in future versions (compare judge Lists to add or remove in all Runs)
    //Saving judges
    //req.body.judges = req.body.judges.map(function(option) { return option._id; });
    //event.judges = req.body.judges;
    //req.body.judges = [];

    event.locations = req.body.locations;
    delete req.body.locations;
    //unable to change the season
    delete  req.body.season;

    var updated = _.merge(event, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.json(200, event);
    });
  });
};

//Reset an existing event
exports.reset = function(req, res){
  _this.resetEvent(req.params.id, function(err, event){
    if (err) { return handleError(res, err); }
    if(!event) { return res.send(404); }
    return res.json(200, event);
  });
};
exports.resetEvent = function(eventId, callback){
  Event.findById(eventId, function (err, event) {
    if (err) { return handleError(res, err); }
    if(!event) { return res.send(404); }

    CompetitionCtrl.resetCompetitions(eventId, function(err){
      if(err){
        return callback (err, null);
      }
      event.isStarted = false;
      event.isEnded = false;
      event.save(function (err) {
        if (err) { return handleError(res, err); }
        return callback(null, event);
      });
    });
  });
};
//Start an existing event
exports.start = function(req, res){
  Event.findById(req.params.id, function (err, event) {
    if (err) { return handleError(res, err); }
    if(!event) { return res.send(404); }
    //TODO: Check all events in the same season are ended or not started
    event.isStarted = true;
    event.isConfirmed = true;
    //TODO: Adjust the event startDate
    event.isEnded = false;
    event.save(function (err) {
      if (err) { return handleError(res, err); }
      CompetitionCtrl.prepareCompetitions(event._id, function(err, result){
        if(err){
          return handleError(res, err);
        }
        Displayer.startEvent(event._id, function(err, result){
          if (err){
            console.log(err);
          }
        });
        return res.json(200, event);
      });
    });
  });
};
//End an existing and started event
exports.end = function(req, res){
  Event.findById(req.params.id, function (err, event) {
    if (err) { return handleError(res, err); }
    if(!event) { return res.send(404); }
    if(!event.isStarted) { return res.send(404); }
    event.isEnded = true;
    event.save(function (err) {
      if (err) { return handleError(res, err); }
      Displayer.showEventResults(event._id, function(err, result){
        if (err){
          console.log(err)
        }
      });
      return res.json(200, event);
    });
  });
};

exports.reopen = function(req, res){
  Event.findById(req.params.id, function (err, event) {
    if (err) { return handleError(res, err); }
    if(!event) { return res.send(404); }
    if(!event.isEnded) { return res.send(404); }
    event.isEnded = false;
    event.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.json(200, event);
    });
  });
};

// Deletes a event from the DB.
exports.destroy = function(req, res) {
  deleteEvent(req.params.id, function(err, result){
    if(err){
      if (err === 404){
        return  res.send(404);
      }
      return handleError(res, err);
    }
    return res.send(204);
  });
};

exports.delete = function(eventId, callback){
  deleteEvent(eventId, function(err, result){
    return callback(err, result);
  });
};

function deleteEvent(eventId, callback){
  Event.findById(eventId, function (err, event) {
    if(err) { return callback(err, false); }
    if(!event) { return callback(404, false); }
    //Remove Competitions
    Competition.find({event: eventId}, '_id', function(err, competitionsId){
      competitionsId.forEach(function(competitionId){
        CompetitionCtrl.delete(competitionId, function(err, result){
          if(err){
            return callback(err, false);
          }
        });
      });
      event.remove(function(err) {
        if(err) { return callback(err, false); }
        return callback(null, true);
      });
    });

  });
}

exports.displayResults = function(req, res) {
  Event.findById(req.params.id, function (err, event) {
    if (err) {
      return handleError(res, err);
    }
    if (!event) {
      return res.send(404);
    }
    Displayer.showEventResults(req.params.id, function (err, result) {
      if (err) {
        return handleError(res, err);
      }
      return res.json(200, result);
    });
  })
};

exports.displayPaf = function(req, res) {
  Event.findById(req.params.id, function (err, event) {
    Event.findById(req.params.id, function (err, event) {
      if (err) {
        return handleError(res, err);
      }
      if (!event) {
        return res.send(404);
      }
      Displayer.showPaf(req.params.id, req.body.pafText, function (err, result) {
        if (err) {
          return handleError(res, err);
        }
        return res.json(200, result);
      });
    })
  });
};

exports.clearPaf = function(req, res) {
  Event.findById(req.params.id, function (err, event) {
    Event.findById(req.params.id, function (err, event) {
      if (err) {
        return handleError(res, err);
      }
      if (!event) {
        return res.send(404);
      }
      Displayer.clearPaf(req.params.id, function (err, result) {
        if (err) {
          return handleError(res, err);
        }
        return res.json(200, result);
      });
    })
  });
};

exports.displayMessage = function(req, res) {
  Event.findById(req.params.id, function (err, event) {
    Event.findById(req.params.id, function (err, event) {
      if (err) {
        return handleError(res, err);
      }
      if (!event) {
        return res.send(404);
      }
      Displayer.showMessage(req.params.id, req.body.text, function (err, result) {
        if (err) {
          return handleError(res, err);
        }
        return res.json(200, result);
      });
    })
  });
};

exports.clearMessage = function(req, res) {
  Event.findById(req.params.id, function (err, event) {
    Event.findById(req.params.id, function (err, event) {
      if (err) {
        return handleError(res, err);
      }
      if (!event) {
        return res.send(404);
      }
      Displayer.clearMessage(req.params.id, function (err, result) {
        if (err) {
          return handleError(res, err);
        }
        return res.json(200, result);
      });
    })
  });
};

// Get active events
exports.activeEvents = function(req, res) {
  Event.find({isStarted: true, isEnded: false},{logo:0})
    .populate('season', 'name code')
    .exec(function (err, events) {
    if(err) { return handleError(res, err); }
    return res.json(200, events);
  });
};

// Get public events
exports.publicEvents = function(req, res) {
  var Step = require('step');
  Step(
    function findPublicSeasons(){
      Season.find({isPublic: true}, this);
    },
    function findEvents(err, seasons){
      if (err) {
        throw err;
      }
      var group = this.group();
      seasons.forEach(function(season){
         Event.find({season: season._id, isStarted: true, isEnded: false}, {logo: 0})
           .populate('season', 'name')
           .populate('sponsors', 'name website')
           .exec(group());
      });
    }, function (err, results){
      if(err) { return handleError(res, err); }

      var publicEvents = [];
      results.forEach(function(events){
        events.forEach(function(event){
          publicEvents.push(event);
        })
      });
      return res.json(200, publicEvents);
    }
  );
};
function getSeasonEvents(season, callback){
  return callback(null, events);
}

exports.updateLogo = function(req, res){
  Event.findById(req.params.id, function(err, event){
    if (err) { return handleError(res, err); }
    if(!event) { return res.send(404); }
    var fs = require('node-fs');
    var file = req.files.file;
    fs.readFile(req.files.file.path, function (dataErr, data) {
      if(data) {
        event.logo = {};
        event.logo.data = data;  // Assigns the image to the path.
        event.logo.contentType = 'image/png';
        event.save(function (err, saveEvent) {
          if (err) {
            console.log('error updating the logo image: ' + err);
          }
          return res.json(200, saveEvent);
        });
      }
    });
  });
};
exports.getLogo = function(req, res){
  Event.findById(req.params.id)
    .exec(function (err, event) {
      if(err) { return handleError(res, err); }
      if(!event) { return res.send(404); }
      // var base64 = (sponsor.logo.data.toString('base64'));
      if(event.logo && event.logo.contentType) {
        res.contentType(event.logo.contentType);
        return res.send(event.logo.data);
      }else{
        var fs = require('node-fs');
        var defaultNoImage = fs.readFileSync(__dirname +  '/../../public/event-no-image.jpg');
        //var defaultNoImage = fs.readFileSync('/tmp/Acro2015/sponsor-no-image.jpg');
        res.contentType('image/jpg');
        res.send(defaultNoImage);
      }
    });
};
function handleError(res, err) {
  return res.send(500, err);
}
