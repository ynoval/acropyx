'use strict';

var express = require('express');
var controller = require('./event.controller');
var multiparty = require('connect-multiparty');
var multipartyMiddleware = multiparty();

var router = express.Router();


router.get('/', controller.index);
router.get('/:id/logo', controller.getLogo);
router.get('/:id/logo/:cache', controller.getLogo);
router.get('/activeEvents', controller.activeEvents);
router.get('/publicEvents', controller.publicEvents);
router.get('/:id/activeCompetitions', controller.activeCompetitions);
router.get('/:id/competitions', controller.competitions);
router.get('/:id', controller.show);
router.post('/', controller.create);
router.put('/:id/updateLogo', multipartyMiddleware, controller.updateLogo);
router.put('/:id/displayPaf', controller.displayPaf);
router.put('/:id/clearPaf', controller.clearPaf);
router.put('/:id/displayMessage', controller.displayMessage);
router.put('/:id/clearMessage', controller.clearMessage);
router.put('/:id/displayResults', controller.displayResults);
router.put('/:id/reset', controller.reset);
router.put('/:id/start', controller.start);
router.put('/:id/end', controller.end);
router.put('/:id/reopen', controller.reopen);
router.put('/:id', controller.update);
router.patch('/:id', controller.update);
router.delete('/:id', controller.destroy);

module.exports = router;
