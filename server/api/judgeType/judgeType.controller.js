/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /judgeTypes              ->  index
 * POST    /judgeTypes              ->  create
 * GET     /judgeTypes/:id          ->  show
 * PUT     /judgeTypes/:id          ->  update
 * DELETE  /judgeTypes/:id          ->  destroy
 */

'use strict';

var _ = require('lodash');
var JudgeType = require('./judgeType.model');

// Get list of judgeTypes
exports.index = function(req, res) {
  JudgeType.find(function (err, judgeTypes) {
    if(err) { return handleError(res, err); }
    return res.json(200, judgeTypes);
  });
};

// Get a single judgeType
exports.show = function(req, res) {
  JudgeType.findById(req.params.id, function (err, judgeType) {
    if(err) { return handleError(res, err); }
    if(!judgeType) { return res.send(404); }
    return res.json(judgeType);
  });
};

// Creates a new judgeType in the DB.
exports.create = function(req, res) {
  JudgeType.create(req.body, function(err, judgeType) {
    if(err) { return handleError(res, err); }
    return res.json(201, judgeType);
  });
};

// Updates an existing judgeType in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  JudgeType.findById(req.params.id, function (err, judgeType) {
    if (err) { return handleError(res, err); }
    if(!judgeType) { return res.send(404); }
    var updated = _.merge(judgeType, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.json(200, judgeType);
    });
  });
};

// Deletes a judgeType from the DB.
exports.destroy = function(req, res) {
  JudgeType.findById(req.params.id, function (err, judgeType) {
    if(err) { return handleError(res, err); }
    if(!judgeType) { return res.send(404); }
    judgeType.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.send(204);
    });
  });
};

function handleError(res, err) {
  return res.send(500, err);
}
