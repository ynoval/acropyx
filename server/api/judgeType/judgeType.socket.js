/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var JudgeType = require('./judgeType.model');

exports.register = function(socket) {
  JudgeType.schema.post('save', function (doc) {
    onSave(socket, doc);
  });
  JudgeType.schema.post('remove', function (doc) {
    onRemove(socket, doc);
  });
};

function onSave(socket, doc, cb) {
  socket.emit('JudgeType:save', doc);
}

function onRemove(socket, doc, cb) {
  socket.emit('JudgeType:remove', doc);
}
