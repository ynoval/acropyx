'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var RunTypeSchema = new Schema({
  //Run Type name
  name: String
});

module.exports = mongoose.model('RunType', RunTypeSchema);
