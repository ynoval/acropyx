/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /runTypes              ->  index
 * POST    /runTypes              ->  create
 * GET     /runTypes/:id          ->  show
 * PUT     /runTypes/:id          ->  update
 * DELETE  /runTypes/:id          ->  destroy
 */

'use strict';

var _ = require('lodash');
var RunType = require('./runType.model');

// Get list of runTypes
exports.index = function(req, res) {
  RunType.find(function (err, runTypes) {
    if(err) { return handleError(res, err); }
    return res.json(200, runTypes);
  });
};

// Get a single runType
exports.show = function(req, res) {
  RunType.findById(req.params.id, function (err, runType) {
    if(err) { return handleError(res, err); }
    if(!runType) { return res.send(404); }
    return res.json(runType);
  });
};

// Creates a new runType in the DB.
exports.create = function(req, res) {
  RunType.create(req.body, function(err, runType) {
    if(err) { return handleError(res, err); }
    return res.json(201, runType);
  });
};

// Updates an existing runType in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  RunType.findById(req.params.id, function (err, runType) {
    if (err) { return handleError(res, err); }
    if(!runType) { return res.send(404); }
    var updated = _.merge(runType, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.json(200, runType);
    });
  });
};

// Deletes a runType from the DB.
exports.destroy = function(req, res) {
  RunType.findById(req.params.id, function (err, runType) {
    if(err) { return handleError(res, err); }
    if(!runType) { return res.send(404); }
    runType.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.send(204);
    });
  });
};

function handleError(res, err) {
  return res.send(500, err);
}
