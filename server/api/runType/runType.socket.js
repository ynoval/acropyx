/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var RunType = require('./runType.model');

exports.register = function(socket) {
  RunType.schema.post('save', function (doc) {
    onSave(socket, doc);
  });
  RunType.schema.post('remove', function (doc) {
    onRemove(socket, doc);
  });
};

function onSave(socket, doc, cb) {
  socket.emit('RunType:save', doc);
}

function onRemove(socket, doc, cb) {
  socket.emit('RunType:remove', doc);
}
