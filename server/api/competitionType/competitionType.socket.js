/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var CompetitionType = require('./competitionType.model');

exports.register = function(socket) {
  CompetitionType.schema.post('save', function (doc) {
    onSave(socket, doc);
  });
  CompetitionType.schema.post('remove', function (doc) {
    onRemove(socket, doc);
  });
};

function onSave(socket, doc, cb) {
  socket.emit('CompetitionType:save', doc);
}

function onRemove(socket, doc, cb) {
  socket.emit('CompetitionType:remove', doc);
}
