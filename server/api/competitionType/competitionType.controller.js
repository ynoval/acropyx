/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /competitionTypes              ->  index
 * POST    /competitionTypes              ->  create
 * GET     /competitionTypes/:id          ->  show
 * PUT     /competitionTypes/:id          ->  update
 * DELETE  /competitionTypes/:id          ->  destroy
 */

'use strict';

var _ = require('lodash');
var CompetitionType = require('./competitionType.model');

// Get list of competitionTypes
exports.index = function(req, res) {
  CompetitionType.find(function (err, competitionTypes) {
    if(err) { return handleError(res, err); }
    return res.json(200, competitionTypes);
  });
};

// Get a single competitionType
exports.show = function(req, res) {
  CompetitionType.findById(req.params.id, function (err, competitionType) {
    if(err) { return handleError(res, err); }
    if(!competitionType) { return res.send(404); }
    return res.json(competitionType);
  });
};

// Creates a new competitionType in the DB.
exports.create = function(req, res) {
  CompetitionType.create(req.body, function(err, competitionType) {
    if(err) { return handleError(res, err); }
    return res.json(201, competitionType);
  });
};

// Updates an existing competitionType in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  CompetitionType.findById(req.params.id, function (err, competitionType) {
    if (err) { return handleError(res, err); }
    if(!competitionType) { return res.send(404); }
    var updated = _.merge(competitionType, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.json(200, competitionType);
    });
  });
};

// Deletes a competitionType from the DB.
exports.destroy = function(req, res) {
  CompetitionType.findById(req.params.id, function (err, competitionType) {
    if(err) { return handleError(res, err); }
    if(!competitionType) { return res.send(404); }
    competitionType.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.send(204);
    });
  });
};

function handleError(res, err) {
  return res.send(500, err);
}
