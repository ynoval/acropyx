'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var CompetitionTypeSchema = new Schema({
  //Competition Type name
  name: String,
  //Mark types to apply in this competition type
  markTypes:[{type:mongoose.Schema.Types.ObjectId, ref:'MarkType'}]
});

module.exports = mongoose.model('CompetitionType', CompetitionTypeSchema);
