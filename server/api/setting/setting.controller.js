/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /settings              ->  index
 * POST    /settings              ->  create
 * GET     /settings/:id          ->  show
 * PUT     /settings/:id          ->  update
 * DELETE  /settings/:id          ->  destroy
 */

'use strict';

var _this = this;

var _ = require('lodash');
var Setting = require('./setting.model');
var Season = require('../season/season.model');
var Event = require('../event/event.model');
var Competition = require('../competition/competition.model');
var Run = require('../run/run.model');
var Flight = require('../flight/flight.model');
var Manoeuvre = require('../manoeuvre/manoeuvre.model');

// Get list of settings
exports.index = function (req, res) {
  Setting.find(function (err, settings) {
    if (err) {
      return handleError(res, err);
    }
    return res.json(200, settings);
  });
};

// Get a single setting
exports.show = function (req, res) {
  Setting
    .findById(req.params.id)
    .populate('competitionMarkCoefficients.competitionType', 'name')
    .populate('competitionMarkCoefficients.markCoefficients.markType', 'name')
    .populate('judgesMean.judgeType', 'name')
    .populate('withoutPenaltyManoeuvres', 'name')
    .exec(function (err, setting) {
      if (err) {
        return handleError(res, err);
      }
      if (!setting) {
        return res.send(404);
      }

      return res.json(setting);
    });
};

exports.defaults = function (req, res) {
  Setting
    .findOne({name: 'Defaults'})
    .populate('competitionMarkCoefficients.competitionType', 'name')
    .populate('competitionMarkCoefficients.markCoefficients.markType', 'name')
    .populate('judgesMean.judgeType', 'name')
    .populate('withoutPenaltyManoeuvres')
    .exec(function (err, setting) {
      if (err) {
        return handleError(res, err);
      }
      if (!setting) {
        return res.send(404);
      }
      return res.json(setting);
    });
};

// Creates a new setting in the DB.
exports.create = function (req, res) {
  Setting.create(req.body, function (err, setting) {
    if (err) {
      return handleError(res, err);
    }
    return res.json(201, setting);
  });
};

// Updates an existing setting in the DB.
exports.update = function (req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  Setting.findById(req.params.id, function (err, setting) {
    if (err) {
      return handleError(res, err);
    }
    if (!setting) {
      return res.send(404);
    }
    //TODO: Ask stackoverflow about save complex schema  populated
    //Saving withoutPenaltyManoeuvres
    req.body.withoutPenaltyManoeuvres = req.body.withoutPenaltyManoeuvres.map(function (option) {
      return option._id;
    });
    setting.withoutPenaltyManoeuvres = req.body.withoutPenaltyManoeuvres;

    //Saving judges mean
    req.body.judgesMean = req.body.judgesMean.map(function (option) {
      return {_id: option._id, judgeType: option.judgeType._id, mean: option.mean};
    });
    setting.judgesMean = req.body.judgesMean;
    req.body.judgesMean = [];

    //Saving markCoefficients
    req.body.competitionMarkCoefficients = req.body.competitionMarkCoefficients.map(function (option) {
      var result = {_id: option._id, competitionType: option.competitionType._id, markCoefficients: []};
      result.markCoefficients = option.markCoefficients.map(function (markOption) {
        return {_id: markOption._id, coefficient: markOption.coefficient, markType: markOption.markType._id};
      });

      return result;
    });

    setting.competitionMarkCoefficients = req.body.competitionMarkCoefficients;

    req.body.competitionMarkCoefficients = [];


    var updated = _.merge(setting, req.body);
    updated.save(function (err, setting) {
      if (err) {
        return handleError(res, err);
      }
      return res.json(200, setting);
    });
  });
};

// Deletes a setting from the DB.
exports.destroy = function (req, res) {
  deleteSettings(req.params.id, function (err) {
    if (err) {
      return handleError(res, err);
    }
    return res.send(204);
  });
};

exports.delete = function (id, callback) {
  return deleteSettings(id, callback);
};

function deleteSettings(id, callback) {
  Setting.findById(id, function (err, setting) {
    if (err) {
      return callback(err);
    }
    if (!setting) {
      return callback(404)
    }

    setting.remove(function (err) {
      if (err) {
        return callback(err);
      }
      return callback(null, true);
    });
  });
}

exports.createSeasonSettings = function (seasonName, callback) {
  Setting.findOne({name: 'Defaults'}).exec(function (err, setting) {
    delete setting._id;
    Setting.create(
      {
        name: seasonName,
        warningPoints: setting.warningPoints,
        warningIncremental: setting.warningIncremental,
        warningsToDSQ: setting.warningsToDSQ,
        penalty: setting.penalty,
        bonusManoeuvres: setting.bonusManoeuvres,
        meanManoeuvres: setting.meanManoeuvres,
        judgesMean: setting.judgesMean,
        competitionMarkCoefficients: setting.competitionMarkCoefficients,
        withoutPenaltyManoeuvres: setting.withoutPenaltyManoeuvres
      }, function (err, setting) {
        if (err) {
          callback(err, null);
        }
        callback(null, setting);
      });
  });
};


exports.seasonSettingsByFlight = function (req, res) {
  _this.getSeasonSettingsByFlight(req.params.flightId, function (err, settings) {
    if (err) {
      return handleError(res, err);
    }
    if (!settings) {
      return res.send(404);
    }
    return res.json(200, settings);
  });
};
exports.getSeasonSettingsByFlight = function (flightId, callback) {
  Flight.findById(flightId, function (err, flight) {
    _this.getSeasonSettingsByRun(flight.run, function (err, settings) {
      return callback(err, settings);
    });
  });
};

exports.seasonSettingsByRun = function (req, res) {
  _this.getSeasonSettingsByRun(req.params.runId, function (err, settings) {
    if (err) {
      return handleError(res, err);
    }
    if (!settings) {
      return res.send(404);
    }
    return res.json(200, settings);
  });
};
exports.getSeasonSettingsByRun = function (runId, callback) {
  Run.findById(runId, function (err, run) {
    _this.getSeasonSettingsByCompetition(run.competition, function (err, settings) {
      return callback(err, settings);
    });
  });
};

exports.seasonSettingsByCompetition = function (req, res) {
  _this.getSeasonSettingsByCompetition(req.params.competitionId, function (err, settings) {
    if (err) {
      return handleError(res, err);
    }
    if (!settings) {
      return res.send(404);
    }
    return res.json(200, settings);
  });
};
exports.getSeasonSettingsByCompetition = function (competitionId, callback) {
  Competition.findById(competitionId, function (err, competition) {
    _this.getSeasonSettingsByEvent(competition.event, function (err, settings) {
      return callback(err, settings);
    });
  });
};

exports.seasonSettingsByEvent = function (req, res) {
  _this.getSeasonSettingsByEvent(req.params.eventId, function (err, settings) {
    if (err) {
      return handleError(res, err);
    }
    if (!settings) {
      return res.send(404);
    }
    return res.json(200, settings);
  });
};
exports.getSeasonSettingsByEvent = function (eventId, callback) {
  Event.findById(eventId, function (err, event) {
    _this.getSeasonSettings(event.season, function (err, settings) {
      return callback(err, settings);
    })
  });
};

exports.seasonSettings = function (req, res) {
  _this.getSeasonSettings(req.params.seasonId, function (err, settings) {
    if (err) {
      return handleError(res, err);
    }
    if (!settings) {
      return res.send(404);
    }
    return res.json(200, settings);
  });
};
exports.getSeasonSettings = function (seasonId, callback) {
  Season.findById(seasonId, function (err, season) {
    Setting.findById(season.settings, function (err, settings) {
      return callback(err, settings);
    })
  });
};


function handleError(res, err) {
  return res.send(500, err);
}
