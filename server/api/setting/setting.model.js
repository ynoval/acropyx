'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var MarkCoefficientSchema = new Schema({
  markType: {type: mongoose.Schema.Types.ObjectId, ref: 'MarkType' },
  coefficient: Number
});
//MarkCoefficientSchema.pre('save', function(next){
//  this.markType = this.markType.map(function(option) { return option._id; });
//  next();
//});
//module.exports = mongoose.model('MarkCoefficient', MarkCoefficientSchema);

var CompetitionMarkCoefficientSchema = new Schema({
  competitionType: {type: mongoose.Schema.Types.ObjectId, ref: 'CompetitionType'},
  markCoefficients: [MarkCoefficientSchema]
});
//module.exports = mongoose.model('CompetitionMarkCoefficient', CompetitionMarkCoefficientSchema);

var JudgeMeanSchema = new Schema({
  judgeType: {type: mongoose.Schema.Types.ObjectId, ref: 'JudgeType'},
  mean: Number
});
//module.exports = mongoose.model('JudgeMean', JudgeMeanSchema);

var SettingSchema = new Schema({
  //Setting name (future use- different settings)
  name: String,
  // amount of warning points to decrees (default 0.5)
  warningPoints: Number,
  // indicate if the warning decrees point are incremental
  //ex: first warn -> decrees .5 points, second -> decrees 1 points ...
  warningIncremental: Boolean,
  // amount of warning to DSQ a competitor (default 3)
  warningsToDSQ: Number,
  // percent to apply when repeat a manoeuvre (default 13)
  penalty: Number,
  // amount od bonus manoeuvres allowed
  bonusManoeuvres: Number,
  //amount of manoeuvre to compute flight result
  meanManoeuvres: Number,
  //judge type influence (FAI: 0.8, VIP: 0.2)
  judgesMean: [JudgeMeanSchema],
  // coefficient markType peer competitionType
  competitionMarkCoefficients:[CompetitionMarkCoefficientSchema],
  // manoeuvres without penalty
  withoutPenaltyManoeuvres: [{type: mongoose.Schema.Types.ObjectId, ref: 'Manoeuvre'}]
});

//SettingSchema.pre('save', function(next){
//  this.withoutPenaltyManoeuvres = this.withoutPenaltyManoeuvres.map(function(option) { return option._id; });
//  next();
//});

module.exports = mongoose.model('Setting', SettingSchema);
