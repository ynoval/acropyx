'use strict';

var express = require('express');
var controller = require('./setting.controller.js');

var router = express.Router();

router.get('/', controller.index);
router.get('/defaults/', controller.defaults);
router.get('/getBySeason/:seasonId', controller.seasonSettings);
router.get('/getByEvent/:eventId', controller.seasonSettingsByEvent);
router.get('/getByCompetition/:competitionId', controller.seasonSettingsByCompetition);
router.get('/getByRun/:runId', controller.seasonSettingsByRun);
router.get('/getByFlight/:flightId', controller.seasonSettingsByFlight);

router.get('/:id', controller.show);

router.post('/', controller.create);
router.put('/:id', controller.update);
router.patch('/:id', controller.update);
router.delete('/:id', controller.destroy);

module.exports = router;
