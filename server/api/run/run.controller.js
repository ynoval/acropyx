/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /runs              ->  index
 * POST    /runs              ->  create
 * GET     /runs/:id          ->  show
 * PUT     /runs/:id          ->  update
 * DELETE  /runs/:id          ->  destroy
 */

'use strict';

var _ = require('lodash');
var Run = require('./run.model');
var Competition = require('../competition/competition.model');
var Flight = require('../flight/flight.model');
var Displayer = require('../../displayer/displayer.service');
var ReportCtrl = require('../report/report.controller');

var _this = this;

// Get list of runs
exports.index = function (req, res) {
  Run.find()
    .populate('type', 'name')
    .populate('competition', 'name')
    .sort('competition.name')
    .exec(function (err, runs) {
      if (err) {
        return handleError(res, err);
      }
      return res.json(200, runs);
    });
};

// Get a single run
exports.show = function (req, res) {
  Run.findById(req.params.id)
    .populate('type', 'name')
    .populate('competition', 'name type')
    .populate('competition.type', 'name')
    .populate('judges', 'name')
    .exec(function (err, run) {
      if (err) {
        return handleError(res, err);
      }
      if (!run) {
        return res.send(404);
      }
      var step = require('step');
      step(
        function () {
          Run.findOne({order: run.order - 1, competition: run.competition._id}, 'name', this.parallel());
          Run.findOne({order: run.order + 1, competition: run.competition._id}, 'name', this.parallel());
        }, function (err, previousRun, nextRun) {
          if (err) {
            return handleError(res, err);
          }

          run._doc.previousRun = previousRun;
          run._doc.nextRun = nextRun;
          return res.json(run);
        }
      );
    });
};
// Create a new Run
exports.create = function (req, res) {
  Run.count({competition: req.body.competition}, function (err, count) {
    //Find previous Run
    Run.findOne({competition: req.body.competition, order: count}, function (err, prevRun) {
      req.body.order = count + 1;
      //TODO: FIX: take the value from season settings
      req.body.applyPenalty = true;
      req.body.judges = [];
      if (prevRun) {
        req.body.judges = prevRun.judges;
      }
      Run.create(req.body, function (err, run) {
        if (err) {
          return handleError(res, err);
        }
        return res.json(201, run);
      });
    });
  });
};

// Updates an existing run.
exports.update = function (req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  Run.findById(req.params.id, function (err, run) {
    if (err) {
      return handleError(res, err);
    }
    if (!run) {
      return res.send(404);
    }

    delete req.body.competition;

    req.body.judges = req.body.judges.map(function (option) {
      return option._id;
    });
    run.judges = req.body.judges;
    req.body.judges = [];
    req.body.qualification = [];

    var updated = _.merge(run, req.body);
    updated.save(function (err) {
      if (err) {
        return handleError(res, err);
      }
      return res.json(200, run);
    });
  });
};

//Reset an existing run
exports.reset = function (req, res) {
  _this.resetRun(req.params.id, function (err, run) {
    if (err) {
      return handleError(res, err);
    }
    if (!run) {
      return res.send(404);
    }
    return res.json(200, run);
  });
};
// Reset all competition runs
exports.resetRuns = function (competitionId, callback) {
  Run.find({competition: competitionId}, function (err, runs) {
    if (err) {
      return callback(err, null);
    }

    if (!runs || runs.length === 0) {
      return callback(null, true);
    }

    var Step = require('step');
    Step(
      function () {
        // Create a new group
        var group = this.group();
        runs.forEach(function (run) {
          _this.resetRun(run._id, group());
        });
      },
      function showAll(err, results) {
        if (err) {
          throw err;
        }
        callback(null, true);
      }
    );
  });
};
//Reset an existing run
exports.resetRun = function (runId, callback) {
  Run.findById(runId, function (err, run) {
    if (err) {
      return handleError(res, err);
    }
    if (!run) {
      return res.send(404);
    }
    //Delete flights
    Flight.remove({run: run._id}, function (err) {
      if (err) {
        return handleError(res, err);
      }
      run.startTime = null;
      run.endTime = null;
      run.qualification = [];
      run.save(function (err) {
        if (err) {
          return handleError(res, err);
        }
        // CompetitionCtrl.runResetNotification(run._id);
        return callback(null, run);
      });
    });
  });
};


function getInitialQualifyStatus(run, competitor) {
  if (run.qualifiedForNextRun === 0) {
    return 1; //Qualify directly
  }
  if ((run.qualifiedByCivlRank > 0 && competitor.civlRank < run.qualifiedByCivlRank) ||
    (run.qualifiedByApwcRank > 0 && competitor.apwcRank < run.qualifiedByApwcRank)) {
    return 3; //luckyByRank
  }
  return 0;//Pending
}
//Return
function getRunResults(run, callback) {
  var pointsResults = [];
  run.qualification.forEach(function (compResults) {
    pointsResults.push(
      {
        competitor: {
          _id: compResults.competitor._id,
          name: compResults.competitor.name,
          civlRank: compResults.competitor.civlRank,
          apwcRank: compResults.competitor.apwcRank
        },
        points: compResults.previousPoints + compResults.currentPoints
      }
    );
  });
  var runResults = _.sortBy(pointsResults, 'points');//TODO: return in reverse order
  return callback(null, runResults);
}

function computeCompetitorWarnings(run, competitorId, callback) {
  var warnings = 0;
  Flight.find({'competitor._id': competitorId})
    .populate('run', 'competition order')
    .exec(function (err, flights) {
      flights.forEach(function (flight) {
        if (String(flight.run.competition) === String(run.competition) && flight.run.order <= run.order) {
          warnings += flight.warnings;
        }
      });
      return callback(null, warnings);
    });
}

function removeDSQResults(run, runResults, callback) {
  var step = require('step');
  step(
    function () {
      var group = this.group();
      runResults.forEach(function (result) {
        computeCompetitorWarnings(run, result.competitor._id, group());
      });
    }, function (err, warnings) {
      var results = [];
      for (var i = 0; i < runResults.length; i++) {
        if (warnings[i] < 3) {
          results.push(runResults[i]);
        }
      }
      callback(null, results);
    }
  );
}


function getStandardQualification(run, runResults, callback) {
  if (run.qualifiedForNextRun >= runResults.length) {
    return callback(null, runResults);
  }
  var qualifyIndex = runResults.length - run.qualifiedForNextRun;

  if (run.qualifiedByCivlRank === 0 && run.qualifiedByApwcRank === 0) {
    return callback(null, runResults.slice(qualifyIndex, runResults.length));
  }

  var results = [];
  var index = 0;
  while (index < runResults.length) {
    if (index >= qualifyIndex) {
      results.push(runResults[index]);
    } else {
      if (runResults[index].competitor.civlRank <= run.qualifiedByCivlRank ||
        runResults[index].competitor.apwcRank <= run.qualifiedByApwcRank) {
        results.push(runResults[index]);
        qualifyIndex++;
      }
    }
    index++;
  }
  return callback(null, results);
}
function getBattleQualification(run, runResults, callback) {
  if (run.qualifiedForNextRun >= runResults.length) {
    return callback(null, runResults);
  }

  var results = [];
  var pendingResults = [];

  Flight.find({run: run._id})
    .sort('order')
    .exec(function (err, flights) {
      var middle = flights.length / 2;
      for (var i = 0; i < flights.length / 2; i++) {
        var firstCompetitor = flights[middle - i - 1].competitor._id;
        var secondCompetitor = flights[middle + i].competitor._id;
        var battleResult = 0; //-1: win first competitor, 0: same results, 1: win second competitor
        if (flights[middle - i - 1].computeResult > flights[middle + i].computeResult) {
          battleResult = -1;
        }
        if (flights[middle - i - 1].computeResult < flights[middle + i].computeResult) {
          battleResult = 1;
        }

        var index = 0;
        var founded = 0;
        while (founded < 2) {
          if (String(runResults[index].competitor._id) === String(firstCompetitor)) {
            switch (battleResult) {
              case 0:
              {
                pendingResults.push(runResults[index]);
                break;
              }
              case -1:
              {
                results.push(runResults[index]);
                break;
              }
              case 1:
              {
                pendingResults.push(runResults[index]);
                break;
              }
            }
            founded++;
          }
          if (String(runResults[index].competitor._id) === String(secondCompetitor)) {
            switch (battleResult) {
              case 0:
              {
                pendingResults.push(runResults[index]);
                break;
              }
              case -1:
              {
                pendingResults.push(runResults[index]);
                break;
              }
              case 1:
              {
                results.push(runResults[index]);
                break;
              }
            }
            founded++;
          }
          index++;
        }
      }

      //Add ranking and points lucky
      var qualifyIndex = pendingResults.length - (run.qualifiedForNextRun - results.length);

      if (qualifyIndex > 0) {
        var index = 0;
        pendingResults = _.sortBy(pendingResults, 'points');
        while (index < pendingResults.length) {
          if (index >= qualifyIndex) {
            results.push(pendingResults[index]);
          } else {
            if (pendingResults[index].competitor.civlRank <= run.qualifiedByCivlRank ||
              pendingResults[index].competitor.apwcRank <= run.qualifiedByApwcRank) {
              results.push(pendingResults[index]);
              qualifyIndex++;
            }
          }
          index++;
        }
      }
      return callback(null, _.sortBy(results, 'points'));
    });
}

exports.getQualifyCompetitors = function (runId, callback) {
  Run.findById(runId)
    .populate('type')
    .exec(function (err, run) {
      getQualifyCompetitors(run, function (err, results) {
        if (err) {
          return callback(null, []);
        }
        return callback(err, results);
      });
    });
};
function getQualifyCompetitors(run, callback) {
  if (!run.endTime) {
    return callback('run is not ended', null);
  }
  getRunResults(run, function (err, runResults) {
    removeDSQResults(run, runResults, function (err, realRunResults) {
      var qualifyCompetitors = [];
      if (run.qualifiedForNextRun === 0) {
        return callback(null, runResults);
      } else {
        if (run.type.name === 'Standard') {
          getStandardQualification(run, realRunResults, function (err, results) {
            return callback(err, results);
          });
        } else {
          getBattleQualification(run, realRunResults, function (err, results) {
            return callback(err, results);
          });
        }
      }
    });
  });
}

//Prepare an existing run
exports.prepare = function (req, res) {
  console.log('Prepare1:' + req.body.regenerate);
  prepareRun(req.params.id, req.body.regenerate, function (err, result) {
    if (err) {
      return handleError(res, err);
    }
    return res.json(200, true);
  });
};
exports.prepareRun = function (runId, regenerate, callback) {
  prepareRun(runId, regenerate, callback);
};

function prepareRun(runId, regenerate, callback) {
  Flight.count({run: runId}, function (err, count) {
    if (count === 0 || regenerate) {
      initializeRun(runId, callback);
    } else {
      return callback(null, true);
    }
  });
}

function computeCompetitionCompetitors(competitionId, callback) {

  Competition.findById(competitionId, function (err, competition) {
    var computeCompetitors = [];
    competition.competitors = _.sortBy(competition.competitors, 'civlRank').reverse();
    competition.competitors.forEach(function (competitor) {
      computeCompetitors.push(
        {
          competitor: {
            _id: competitor._id,
            name: competitor.name,
            civlRank: competitor.civlRank,
            apwcRank: competitor.apwcRank
          },
          points: 0
        }
      )
    });
    return callback(null, computeCompetitors);
  });
}

function computeRunCompetitors(run, callback) {
  if (run.order === 1) {
    computeCompetitionCompetitors(run.competition, callback);
  } else {
    Run.findOne({competition: run.competition, order: run.order - 1})
      .populate('type')
      .exec(function (err, previousRun) {
        getQualifyCompetitors(previousRun, callback);
      });
  }
}

function initializeRun(runId, callback) {
  Run.findById(runId, function (err, run) {
    if (err) {
      return callback(err, false);
    }
    if (!run) {
      return callback(404, false);
    }
    var step = require('step');
    step(
      function () {
        computeRunCompetitors(run, this);
      },
      function (err, competitors) {
        if (err) {
          return callback(err);
        }
        if (!competitors) {
          return callback('error: no run competitors');
        }
        //Final Medals RUN
        if (run.finalMedals) {
          //The previous run qualify only 4 competitors
          if (competitors.length !== 4) {
            //ERROR!!
          }
          //Find Previous Run
          Run.findOne({competition: run.competition, order: run.order - 1})
            .populate('type')
            .exec(function (err, previousRun) {
              //Check: Previous Run has to be a Battle (change in future versions)
              if (previousRun.type.name !== 'Battle') {
                //ERROR!!!!
              } else {
                //Find Previous Run Flights
                Flight.find({'run': previousRun._id})
                  .sort('order')
                  .exec(function (err, previousFlights) {
                    if (previousFlights.length !== 4) {
                      //ERROR!!
                    } else {
                      //Battles
                      var goldBattle = [];
                      var bronzeBattle = [];
                      if (previousFlights[0].computeResult > previousFlights[3].computeResult) {
                        bronzeBattle.push(previousFlights[3].competitor);
                        goldBattle.push(previousFlights[0].competitor);
                      } else {
                        bronzeBattle.push(previousFlights[0].competitor);
                        goldBattle.push(previousFlights[3].competitor);
                      }
                      //Second Battle
                      if (previousFlights[1].computeResult > previousFlights[2].computeResult) {
                        bronzeBattle.push(previousFlights[2].competitor);
                        goldBattle.push(previousFlights[1].competitor);
                      } else {
                        bronzeBattle.push(previousFlights[1].competitor);
                        goldBattle.push(previousFlights[2].competitor);
                      }

                      //Generate Flights
                      var flights = [];
                      var index1 = 0;
                      while (String(competitors[index1].competitor._id) !== String(bronzeBattle[0]._id)) {
                        index1++;
                      }
                      var index2 = 0;
                      while (String(competitors[index2].competitor._id) !== String(bronzeBattle[1]._id)) {
                        index2++;
                      }
                      flights.push({
                        competitor: competitors[index1].competitor,
                        run: run._id,
                        order: (index1 < index2) ? 2 : 3 //1:2
                      });
                      flights.push({
                        competitor: competitors[index2].competitor,
                        run: run._id,
                        order: (index1 < index2) ? 3 : 2 // 2:1
                      });

                      var index1 = 0;
                      while (String(competitors[index1].competitor._id) !== String(goldBattle[0]._id)) {
                        index1++;
                      }
                      var index2 = 0;
                      while (String(competitors[index2].competitor._id) !== String(goldBattle[1]._id)) {
                        index2++;
                      }
                      flights.push({
                        competitor: competitors[index1].competitor,
                        run: run._id,
                        order: (index1 < index2) ? 1 : 4   //3:4
                      });
                      flights.push({
                        competitor: competitors[index2].competitor,
                        run: run._id,
                        order: (index1 < index2) ? 4 : 1 //4:3
                      });


                      //Generate Run qualification
                      run.qualification = [];
                      competitors.forEach(function (competitor) {
                        run.qualification.push(
                          {
                            competitor: competitor.competitor,
                            previousPoints: competitor.points,
                            currentPoints: 0,
                            qualifyStatus: getInitialQualifyStatus(run, competitor)
                          }
                        );
                      });
                      //Creating Flights
                      Flight.remove({run: run._id}, function (err) {
                        Flight.create(flights, function (err, flights) {
                          if (err) {
                            return callback(err, false);
                          }
                          run.save(function (err, savedRun) {
                            if (err) {
                              return callback(err, false);
                            }
                            callback(null, true);
                          });
                        });
                      });
                    }
                  });
              }
            });
        }
        else {
          //Create flights and Qualification
          Flight.remove({run: run._id}, function (err) {
            var flights = [];
            //Empty -- CHECK!!!
            run.qualification = [];
            for (var index = 0; index < competitors.length; index++) {
              var competitor = competitors[index].competitor;
              flights.push(
                {
                  competitor: competitor,
                  run: run._id,
                  order: index
                }
              );

              run.qualification.push(
                {
                  competitor: competitor,
                  previousPoints: competitors[index].points,
                  currentPoints: 0,
                  qualifyStatus: getInitialQualifyStatus(run, competitor)
                }
              )
            }
            Flight.create(flights, function (err, flights) {
              if (err) {
                return callback(err, false);
              }
              run.save(function (err, savedRun) {
                if (err) {
                  return callback(err, false);
                }
                callback(null, true);
              });
            });
          });
        }
      }
    );
  });
}

//Start an existing run
exports.start = function (req, res) {
  Run.findById(req.params.id, function (err, run) {
    if (err) {
      return handleError(res, err);
    }
    if (!run) {
      return res.send(404);
    }
    run.startTime = new Date();
    run.endTime = null;
    run.save(function (err) {
      if (err) {
        return handleError(res, err);
      }
      if (req.body.display) {
        Displayer.startRun(run._id, function (err, result) {
          if (err) {
            console.log(err)
          }
        });
      }
      prepareRun(run._id, false, function (err, result) {
        if (err) {
          console.log('unable to prepare the run');
        }
        return res.json(200, run);
      });
    });
  });
};
//End an existing and started run
exports.end = function (req, res) {
  Run.findById(req.params.id)
    .populate('type')
    .exec(function (err, run) {
      if (err) {
        return handleError(res, err);
      }
      if (!run) {
        return res.send(404);
      }
      if (!run.startTime) {
        return res.send(404);
      }
      run.endTime = new Date();
      run.save(function (err) {
        if (err) {
          return handleError(res, err);
        }

        if (req.body.display) {
          Displayer.showRunResults(run._id, function (err, result) {
            if (err) {
              console.log(err)
            }
          });
        }

        Run.findOne({competition: run.competition, order: run.order + 1}, function (err, nextRun) {
          if (err) {
            console.log('Error finding next run');
            return res.json(200, run);
          }
          if (!nextRun) {
            console.log('Next run not available');
            return res.json(200, run);
          }
          prepareRun(nextRun._id, true, function (err) {
            if (err) {
              console.log('Error preparing next run');
            }
            return res.json(200, run);
          });
        });

      });
    });
};

exports.reopen = function (req, res) {
  Run.findById(req.params.id)
    .populate('type')
    .exec(function (err, run) {
      if (err) {
        return handleError(res, err);
      }
      if (!run) {
        return res.send(404);
      }
      if (!run.endTime) {
        return res.send(404);
      }
      console.log('reopening run');
      Run.count({
        competition: run.competition,
        startTime: {'$ne': null},
        order: {'$gt': run.order}
      }, function (err, count) {
        console.log('count: ' + count);
        if (count === 0) {
          run.endTime = null;
          run.save(function (err) {
            if (err) {
              return handleError(res, err);
            }
            return res.json(200, run);
          });
        }
      });
    });
};


// Deletes a run from the DB.
exports.destroy = function (req, res) {
  deleteRun(req.params.id, function (err, result) {
    if (err) {
      if (err === 404) {
        return res.send(404);
      }
      return handleError(res, err);
    }
    return res.send(204);
  });
};
exports.delete = function (runId, callback) {
  deleteRun(runId, function (err, result) {
    return callback(err, result);
  });
};
function deleteRun(runId, callback) {
  //TODO: Transactional
  Run.findById(runId, function (err, run) {
    if (err) {
      return callback(err, false);
    }
    if (!run) {
      return callback(404, false);
    }
    //Adjust runs order
    Run
      .find({competition: run.competition})
      .where('order').gt(run.order)
      .exec(function (err, runs) {
        runs.forEach(function (currRun) {
          currRun.order--;
          currRun.save();
        })
      });
    Flight.remove({run: run._id}, function (err) {

      run.remove(function (err) {
        if (err) {
          return callback(err, false);
        }
        return callback(null, true);
      });
    });
  });
}

exports.changeOrder = function (req, res) {
  var runId = req.body.runId;
  var newOrder = req.body.newOrder;
  Run.findById(runId, function (err, run) {
    if (err) {
      return handleError(res, err);
    }
    if (!run) {
      return res.send(404);
    }
    Run
      .findOne({competition: run.competition, order: newOrder}, function (err, otherRun) {
        if (err) {
          return handleError(res, err);
        }
        if (!otherRun) {
          return res.send(404);
        }
        otherRun.order = run.order;
        run.order = newOrder;
        run.save();
        otherRun.save();
        return res.json(200, {result: true});
      });
  });
};

exports.flights = function (req, res) {
  Run.findById(req.params.id, function (err, run) {
    if (err) {
      return handleError(res, err);
    }
    if (!run) {
      return res.send(404);
    }
    Flight.find({run: run._id})
      .populate('manoeuvres.manoeuvre')
      .populate('detailResults.markType', 'name')
      .exec(function (err, flights) {
        return res.json(200, flights);
      });
  });
};

exports.judges = function (req, res) {
  Run.findById(req.params.id)
    .populate('judges', 'name type')
    .populate('judges.type', 'name')
    .exec(function (err, run) {
      if (err) {
        return handleError(res, err);
      }
      if (!run) {
        return res.send(404);
      }

      return res.json(200, run.judges);
    });
};

exports.generateStartingOrderPDF = function (req, res) {
  Run.findById(req.params.id)
    .populate('competition', 'name')
    .exec(function (err, run) {
      Flight.find({run: run._id})
        .sort('order')
        .exec(function (err, flights) {
          run.flights = flights;
          Competition.findById(run.competition._id)
            .populate('type')
            .exec(function (err, competition) {
              if (competition.type.name === 'Solo') {
                ReportCtrl.generateRunStartingOrderPDF(run, function (err, result) {
                  if (err) {
                    return handleError(res, err);
                  }
                  res.json(200, result);
                });
              } else {
                ReportCtrl.generateRunStartingOrderSyncPDF(run, function (err, result) {
                  if (err) {
                    return handleError(res, err);
                  }
                  res.json(200, result);
                });
              }
            });


        });
    });
};

exports.generateManoeuvresPDF = function (req, res) {
  Run.findById(req.params.id)
    .populate('competition', 'name')
    .exec(function (err, run) {
      Flight.find({run: run._id})
        .populate('manoeuvres.manoeuvre')
        .sort('order')
        .exec(function (err, flights) {
          run.flights = flights;
          ReportCtrl.generateRunManoeuvresPDF(run, function (err, result) {
            if (err) {
              return handleError(res, err);
            }
            res.json(200, result);
          });
        });
    });
};


exports.generateResultsPDF = function (req, res) {
  Run.findById(req.params.id)
    .populate('competition', 'name')
    .exec(function (err, run) {
      _this.getResults(run._id, function (err, results) {
        run.results = results;
        Competition.findById(run.competition._id)
          .populate('type')
          .exec(function (err, competition) {
            if (competition.type.name === 'Solo') {
              ReportCtrl.generateRunResultsPDF(run, function (err, result) {
                if (err) {
                  return handleError(res, err);
                }
                res.json(200, result);
              });
            } else {
              ReportCtrl.generateRunResultsSyncPDF(run, function (err, result) {
                if (err) {
                  return handleError(res, err);
                }
                res.json(200, result);
              });
            }
          });
      });
    });
};


exports.displayStartingOrder = function (req, res) {
  Run.findById(req.params.id)
    .populate('type')
    .exec(function (err, run) {
      if (err) {
        return handleError(res, err);
      }
      if (!run) {
        return res.send(404);
      }

      if (run.type.name === 'Standard') {
        Displayer.startingOrder(run._id, function (err, result) {
          if (err) {
            return handleError(res, err);
          }
          return res.json(200, result);
        });
      } else {
        Displayer.startingOrderBattle(run._id, function (err, result) {
          if (err) {
            return handleError(res, err);
          }
          return res.json(200, result);
        });
      }
    });
};
exports.displayResults = function (req, res) {
  Run.findById(req.params.id, function (err, run) {
    if (err) {
      return handleError(res, err);
    }
    if (!run) {
      return res.send(404);
    }
    Displayer.showRunResults(run._id, function (err, result) {
      if (err) {
        return handleError(res, err);
      }
      return res.json(200, result);
    });
  });
};
exports.displayBattlesResults = function (req, res) {
  Run.findById(req.params.id)
    .populate('type')
    .exec(function (err, run) {
      if (err) {
        return handleError(res, err);
      }
      if (!run || run.type.name === 'Standard') {
        return res.send(404);
      }

      Displayer.showRunBattleResults(run._id, function (err, result) {
        if (err) {
          return handleError(res, err);
        }
        return res.json(200, result);
      });
    });
};

exports.updateFlightQualification = function (flight, callback) {
  Run.findById(flight.run, function (err, run) {
    if (err) {
      return callback(err, false);
    }
    if (!run) {
      return callback(404, false);
    }

    //Find the competitor and update currentPoints (TODO: Recalculate qualification status)
    var index = 0;
    var found = false;
    while (index < run.qualification.length && !found) {
      if (String(run.qualification[index].competitor._id) === String(flight.competitor._id)) {
        found = true;
        run.qualification[index].currentPoints = flight.computeResult;
      }
      index++;
    }

    if (!found) {
      return callback(404, false);
    }

    run.save(function (err, savedRun) {
      if (err) {
        return callback(err, false);
      }
      return callback(null, true);
    })
  });
};
exports.results = function (req, res) {
  _this.getResults(req.params.id, function (err, flights) {
    if (err) {
      return handleError(res, err);
    }
    return res.json(200, flights);
  });
};


exports.getResults = function (runId, callback) {
  Flight.find({run: runId, endTime: {'$ne': null}})
    .populate('detailResults.markType', 'name')
    .sort('computeResult')
    .exec(function (err, flights) {
      if (err) {
        return callback(err, null);
      }
      return callback(null, flights.reverse());
    });
};


exports.runResultsForRanking = function (req, res) {
  //Devuelve los resultados de los flights ordenados de mayor a menor
  // Para los ompetidores que no volaron en la run se calcula la compensacion que eso implica:
  // 1- Obtener el listado de competidores que no volaron y buscar su mejor vuelo (o promedio de vuelos)??
  // 2- Buscar el menor resultado de la run
  // 3- poner como resultado para cada piloto que no volo el menor entre ( 1 y 2)
};

exports.runQualify = function (req, res) {
  //Devuelve los clasificados en la run en orden:
  // - ver la cantidad que clasifican (si es 0 - todos, i es mayor que los competitors -> TODOS)
  // - ver si alguien con rank-civl mejorque el establecido se quedo fuera (quitar los ultimos por estos)
  // - ver si alguien con rank apwc mejorque el establecido se quedo fuera (quitar los ultimos por estos)
  // Esto se utiliza para setear los competidores de la proxima run
};

function handleError(res, err) {
  return res.send(500, err);
}
