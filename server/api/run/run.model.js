'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var QualificationSchema = new Schema({
  competitor:{
    _id: {type: mongoose.Schema.Types.ObjectId},
    name: String,
    //CIVL ranking
    civlRank: Number,
    //APWC ranking
    apwcRank: Number
  },
  previousPoints: Number,
  currentPoints: Number,
  //-1:eliminated, 0: not defined, 1:qualified direct, 2: qualifyLuckyByResult, 3: qualifyLuckyByRanking
  qualifyStatus: Number
});

var RunSchema = new Schema({
  //Run name
  name: String,
  //Run code (used to reports)
  code: String,
  //Run Type: Standard or Battle
  type: {type: mongoose.Schema.Types.ObjectId, ref: 'RunType'},
  startTime: Date,
  endTime: Date,
  applyPenalty: {type: Boolean, default: true},
  //competitors qualified automatically to the next run due to CIVL Rank
  qualifiedByCivlRank: {type: Number, default: 0},
  //competitors qualified automatically to the next run due to APWC Rank
  qualifiedByApwcRank: {type: Number, default: 0},
  //Amount of competitors qualified to the next run
  //0 mean all competitors qualify
  //TODO: Validate with previous run
  qualifiedForNextRun: {type: Number, default: 0},
  competition: {type: mongoose.Schema.Types.ObjectId, ref: 'Competition'},
  judges:[{type: mongoose.Schema.Types.ObjectId, ref: 'Judge'}],
  //TODO: Validate with amount of runs in this competition
  order: Number,
  qualification: [QualificationSchema],
  //TODO: Check next year DUBAI
  finalMedals: {type: Boolean, default: false}
});

module.exports = mongoose.model('Run', RunSchema);
