/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var sponsor = require('./sponsor.model');

exports.register = function(socket) {
  sponsor.schema.post('save', function (doc) {
    onSave(socket, doc);
  });
  sponsor.schema.post('remove', function (doc) {
    onRemove(socket, doc);
  });
}

function onSave(socket, doc, cb) {
  socket.emit('sponsor:save', doc);
}

function onRemove(socket, doc, cb) {
  socket.emit('sponsor:remove', doc);
}
