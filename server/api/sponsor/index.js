'use strict';

var express = require('express');
var multiparty = require('connect-multiparty');
var multipartyMiddleware = multiparty();

var controller = require('./sponsor.controller');

var router = express.Router();


router.get('/', controller.index);
router.get('/:id/logo', controller.getLogo);
router.get('/:id/logo/:cache', controller.getLogo);
router.get('/:id', controller.show);
router.post('/', controller.create);
router.put('/:id/updateLogo', multipartyMiddleware, controller.updateLogo);
router.put('/:id', controller.update);
router.patch('/:id', controller.update);
router.delete('/:id', controller.destroy);

module.exports = router;
