/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /sponsors              ->  index
 * POST    /sponsors              ->  create
 * GET     /sponsors/:id          ->  show
 * PUT     /sponsors/:id          ->  update
 * DELETE  /sponsors/:id          ->  destroy
 */

'use strict';

var _ = require('lodash');
var Sponsor = require('./sponsor.model');

// Get list of sponsors
exports.index = function(req, res) {
  Sponsor.find({}, {logo: 0})
    .sort('name')
    .exec(function (err, sponsors) {
    if(err) { return handleError(res, err); }
    return res.json(200, sponsors);
  });
};

// Get a single sponsor
exports.show = function(req, res) {
  Sponsor.findById(req.params.id, {logo: 0})
    .exec(function (err, sponsor) {
    if(err) { return handleError(res, err); }
    if(!sponsor) { return res.send(404); }
    return res.json(sponsor);
  });
};

// Creates a new sponsor in the DB.
exports.create = function(req, res) {
  Sponsor.create(req.body, function(err, sponsor) {
    if(err) { return handleError(res, err); }
    return res.json(201, sponsor);
  });
};

// Updates an existing sponsor in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  Sponsor.findById(req.params.id, function (err, sponsor) {
    if (err) { return handleError(res, err); }
    if(!sponsor) { return res.send(404); }
    var updated = _.merge(sponsor, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.json(200, sponsor);
    });
  });
};

exports.updateLogo = function(req, res){
  Sponsor.findById(req.params.id, function(err, sponsor){
    if (err) { return handleError(res, err); }
    if(!sponsor) { return res.send(404); }
    var fs = require('node-fs');
    var file = req.files.file;
    console.log('Logo SPPONSOR:'  + file.name);
    console.log('Logo type SPONSOR' + file.type);
    fs.readFile(req.files.file.path, function (dataErr, data) {
      if(data) {
        sponsor.logo = {};
        sponsor.logo.data = data;  // Assigns the image to the path.
        sponsor.logo.contentType = 'image/png';
        sponsor.save(function (err, saveUser) {
          if (err) {
            console.log('error updating the logo image: ' + err);
          }
          return res.json(200, saveUser);
        });
      }
    });
  });
};
exports.getLogo = function(req, res){
  Sponsor.findById(req.params.id)
    .exec(function (err, sponsor) {
      if(err) { return handleError(res, err); }
      if(!sponsor) { return res.send(404); }
     // var base64 = (sponsor.logo.data.toString('base64'));
      if(sponsor.logo && sponsor.logo.contentType) {
        res.contentType(sponsor.logo.contentType);
        return res.send(sponsor.logo.data);
      }else{
        var fs = require('node-fs');
        var defaultNoImage = fs.readFileSync(__dirname +  '/../../public/sponsor-no-image.jpg');
         //var defaultNoImage = fs.readFileSync('/tmp/Acro2015/sponsor-no-image.jpg');
        res.contentType('image/jpg');
        res.send(defaultNoImage);
      }
    });
};

// Deletes a sponsor from the DB.
exports.destroy = function(req, res) {
  Sponsor.findById(req.params.id, function (err, sponsor) {
    if(err) { return handleError(res, err); }
    if(!sponsor) { return res.send(404); }
    sponsor.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.send(204);
    });
  });
};

function handleError(res, err) {
  return res.send(500, err);
}
