'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var SponsorSchema = new Schema({
  //Sponsor name
  name: String,
  //Sponsor website url ...
  website: String,
  //Sponsor Logo image
  logo: {data: Buffer, contentType: String}
});

module.exports = mongoose.model('Sponsor', SponsorSchema);
