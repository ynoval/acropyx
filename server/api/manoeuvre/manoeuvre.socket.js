/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var Manoeuvre = require('./manoeuvre.model');

exports.register = function(socket) {
  Manoeuvre.schema.post('save', function (doc) {
    onSave(socket, doc);
  });
  Manoeuvre.schema.post('remove', function (doc) {
    onRemove(socket, doc);
  });
};

function onSave(socket, doc, cb) {
  socket.emit('Manoeuvre:save', doc);
}

function onRemove(socket, doc, cb) {
  socket.emit('Manoeuvre:remove', doc);
}
