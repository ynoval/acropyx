/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /manoeuvres              ->  index
 * POST    /manoeuvres              ->  create
 * GET     /manoeuvres/:id          ->  show
 * PUT     /manoeuvres/:id          ->  update
 * DELETE  /manoeuvres/:id          ->  destroy
 */

'use strict';

var _ = require('lodash');
var manoeuvre = require('./manoeuvre.model');

// Get list of manoeuvres
exports.index = function(req, res) {
  manoeuvre.find()
    .sort('name')
    .exec(function (err, manoeuvres) {
    if(err) { return handleError(res, err); }
    return res.json(200, manoeuvres);
  });
};

// Get a single manoeuvre
exports.show = function(req, res) {
  manoeuvre.findById(req.params.id, function (err, manoeuvre) {
    if(err) { return handleError(res, err); }
    if(!manoeuvre) { return res.send(404); }
    return res.json(manoeuvre);
  });
};

// Creates a new manoeuvre in the DB.
exports.create = function(req, res) {
  manoeuvre.create(req.body, function(err, manoeuvre) {
    if(err) { return handleError(res, err); }
    return res.json(201, manoeuvre);
  });
};

// Updates an existing manoeuvre in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  manoeuvre.findById(req.params.id, function (err, manoeuvre) {
    if (err) { return handleError(res, err); }
    if(!manoeuvre) { return res.send(404); }
    var updated = _.merge(manoeuvre, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.json(200, manoeuvre);
    });
  });
};

// Deletes a manoeuvre from the DB.
exports.destroy = function(req, res) {
  manoeuvre.findById(req.params.id, function (err, manoeuvre) {
    if(err) { return handleError(res, err); }
    if(!manoeuvre) { return res.send(404); }
    manoeuvre.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.send(204);
    });
  });
};


function handleError(res, err) {
  return res.send(500, err);
}
