'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var ManoeuvreSchema = new Schema({
  //ENTITY name
  name: String,
  //Bonus (0 by default)
  twistedBonus: { type: Number, default: 0 },
  flippedBonus: {type: Number, default: 0 },
  reversedBonus: {type: Number, default: 0 },
  //Coefficient (to calculate flight results)
  coefficient: Number,
  //the manoeuvre has left and right sides
  hasSides: Boolean,
  //the manoeuvre is only available in Synchro competitions
  onlySynchro: Boolean
});

module.exports = mongoose.model('Manoeuvre', ManoeuvreSchema);
