/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /flights              ->  index
 * POST    /flights              ->  create
 * GET     /flights/:id          ->  show
 * PUT     /flights/:id          ->  update
 * DELETE  /flights/:id          ->  destroy
 */

'use strict';

var _this = this;
var _ = require('lodash');
var Flight = require('./flight.model');
var Run = require('../run/run.model');
var RunCtrl = require('../run/run.controller');
var SettingsCtrl = require('../setting/setting.controller');
var CompetitionCtrl = require('../competition/competition.controller');
var JudgeType = require('../judgeType/judgeType.model') ;
var Competition = require('../competition/competition.model');
var Displayer = require('../../displayer/displayer.service');

// Get list of flights
exports.index = function(req, res) {
  Flight.find(function (err, flights) {
    if(err) { return handleError(res, err); }
    return res.json(200, flights);
  });
};

// Get a single flight
exports.show = function(req, res) {
  getFlightPopulated(req.params.id, function (err, flight) {
    if(err) { return handleError(res, err); }
    if(!flight) { return res.send(404); }
    return res.json(flight);
  });
};

// Creates a new flight in the DB.
exports.create = function(req, res) {
  Flight.create(req.body, function(err, flight) {
    if(err) { return handleError(res, err); }
    return res.json(201, flight);
  });
};

// Updates an existing flight in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  Flight.findById(req.params.id, function (err, flight) {
    if (err) { return handleError(res, err); }
    if(!flight) { return res.send(404); }
    var updated = _.merge(flight, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.json(200, flight);
    });
  });
};

// Deletes a flight from the DB.
exports.destroy = function(req, res) {
  Flight.findById(req.params.id, function (err, flight) {
    if(err) { return handleError(res, err); }
    if(!flight) { return res.send(404); }
    flight.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.send(204);
    });
  });
};

exports.changeOrder = function(req, res){
  var flightId = req.body.flightId;
  var newOrder = req.body.newOrder;
  Flight.findById(flightId, function (err, flight) {
    if (err) { return handleError(res, err); }
    if(!flight) { return res.send(404); }
    Flight
      .findOne({run: flight.run, order : newOrder}, function(err, otherFlight){
        if (err) { return handleError(res, err); }
        if(!otherFlight) { return res.send(404); }
        otherFlight.order = flight.order;
        flight.order = newOrder;
        flight.save();
        otherFlight.save();
        return res.json(200, {result : true});
      });
  });
};


exports.manoeuvres = function(req, res){
  Flight.findById(req.params.id)
    .populate('manoeuvres.manoeuvre')
    .exec(function(err, flight){
    if (err){ return handleError(res, err);}
    if (!flight) { return res.send(404);}
    return res.json(200, flight.manoeuvres);
  });
};

exports.votes = function(req, res){
  Flight
    .findById(req.params.id)
    .exec(function(err, flight){
      if (err){ return handleError(res, err);}
      if (!flight) { return res.send(404);}
      return res.json(200, flight.votes);
    });
};

exports.addWarning = function(req, res){
  Flight.findById(req.params.id, function(err, flight){
    if (err){ return handleError(res, err);}
    if (!flight) { return res.send(404);}
    SettingsCtrl.getSeasonSettingsByFlight(flight._id, function(err, settings) {
      if (flight.warnings < settings.warningsToDSQ) {
        flight.warnings++;
        flight.save(function () {
          //TODO: Change only the result ??
          processFlight(flight._id, function (err, processedFlight) {
            return res.json(200, processedFlight);
          });
        });
      } else {
        getFlightPopulated(flight._id, function (err, populatedFlight) {
          return res.json(200, populatedFlight);
        });
      }
    });
  });
};

exports.removeWarning = function(req, res){
  Flight.findById(req.params.id, function(err, flight){
    if (err){ return handleError(res, err);}
    if (!flight) { return res.send(404);}
    if(flight.warnings > 1 || (!flight.hasManoeuvresWarnings &&  flight.warnings === 1)) {
      flight.warnings--;
      flight.save(function () {
        //TODO: Change only the result ??
        processFlight(flight._id, function (err, processedFlight) {
          return res.json(200, processedFlight);
        });
      });
    } else {
      getFlightPopulated(flight._id, function (err, populatedFlight) {
        return res.json(200, populatedFlight);
      });
    }
  });
};

exports.saveManoeuvres = function(req, res){
  Flight
    .findById(req.params.id)
    .exec(function(err, flight){
      if (err){ return handleError(res, err);}
      if (!flight) { return res.send(404);}
      flight.manoeuvres = req.body.manoeuvres.map(function(mano){
        return {
          manoeuvre: mano.manoeuvre._id,
          side: mano.side,
          bonusType: mano.bonusType
        }
      });
      flight.save(function(err){
        if (err){ return handleError(res, err);}
        markManoeuvres(flight._id, function(err, result) {
          if(err){
            return handleError(res, err);
          }
          processFlight(flight._id, function (err, processedFlight) {
            return res.json(200, processedFlight);
          });
        });
      });
    });
};


/* Bonus */
function getBonus(flightManoeuvre){
  var bonus = 0;
  flightManoeuvre.bonusType.forEach(function(b){
    switch (b){
      case 'Twisted':{
        bonus += flightManoeuvre.manoeuvre.twistedBonus;
        break;
      }
      case 'Flipped':{
        bonus += flightManoeuvre.manoeuvre.flippedBonus;
        break;
      }
      case 'Reversed':{
        bonus += flightManoeuvre.manoeuvre.reversedBonus;
      }
    }
  });
  return bonus;
}
function getBonusManoeuvres(manoeuvres, callback){
  var bonusManoeuvres = [];
  manoeuvres.forEach(function(flightManoeuvre) {
    var bonus = getBonus(flightManoeuvre);
    if(bonus > 0){
      bonusManoeuvres.push(flightManoeuvre);
    }
  });
  return callback(null, bonusManoeuvres);
}

//TODO: implement using settings
// function getBonusManoeuvres(manoeuvres, callback){
//   var firstBonusManoeuvre = {
//     _id: null,
//     bonus: 0
//   };
//   var secondBonusManoeuvre = {
//     _id: null,
//     bonus: 0
//   };
//
//   var thirdBonusManoeuvre = {
//     _id: null,
//     bonus: 0
//   };
//
//   var fourthBonusManoeuvre = {
//     _id: null,
//     bonus: 0
//   };
//
//     manoeuvres.forEach(function(flightManoeuvre){
//    var bonus = getBonus(flightManoeuvre);
//     if(bonus  > firstBonusManoeuvre.bonus){
//       fourthBonusManoeuvre._id = thirdBonusManoeuvre._id;
//       fourthBonusManoeuvre.bonus = thirdBonusManoeuvre.bonus;
//       thirdBonusManoeuvre._id = secondBonusManoeuvre._id;
//       thirdBonusManoeuvre.bonus = secondBonusManoeuvre.bonus;
//       secondBonusManoeuvre._id = firstBonusManoeuvre._id;
//       secondBonusManoeuvre.bonus = firstBonusManoeuvre.bonus;
//       firstBonusManoeuvre._id = flightManoeuvre._id;
//       firstBonusManoeuvre.bonus = bonus;
//     } else {
//       if(bonus  > secondBonusManoeuvre.bonus){
//         fourthBonusManoeuvre._id = thirdBonusManoeuvre._id;
//         fourthBonusManoeuvre.bonus = thirdBonusManoeuvre.bonus;
//         thirdBonusManoeuvre._id = secondBonusManoeuvre._id;
//         thirdBonusManoeuvre.bonus = secondBonusManoeuvre.bonus;
//         secondBonusManoeuvre._id = flightManoeuvre._id;
//         secondBonusManoeuvre.bonus = bonus;
//       } else {
//         if(bonus  > thirdBonusManoeuvre.bonus){
//           fourthBonusManoeuvre._id = thirdBonusManoeuvre._id;
//           fourthBonusManoeuvre.bonus = thirdBonusManoeuvre.bonus;
//           thirdBonusManoeuvre._id = flightManoeuvre._id;
//           thirdBonusManoeuvre.bonus = bonus;
//         } else {
//           if(bonus  > fourthBonusManoeuvre.bonus){
//             fourthBonusManoeuvre._id = flightManoeuvre._id;
//             fourthBonusManoeuvre.bonus = bonus;
//           }
//         }
//       }
//     }
//   });
//   var bonusManoeuvres = [];
//   if (firstBonusManoeuvre._id){
//     bonusManoeuvres.push(firstBonusManoeuvre);
//     if(secondBonusManoeuvre._id){
//       bonusManoeuvres.push(secondBonusManoeuvre);
//       if(thirdBonusManoeuvre._id){
//         bonusManoeuvres.push(thirdBonusManoeuvre);
//         if(fourthBonusManoeuvre._id){
//           bonusManoeuvres.push(fourthBonusManoeuvre);
//         }
//       }
//     }
//   }
//   return callback(null, bonusManoeuvres);
// }

/*MALUS*/
function allowDuplicateManoeuvre(manoeuvre, manoeuvres, callback){
  var index = 0;
  var found = false;
  while(index < manoeuvres.length && !found){
    if (String(manoeuvres[index]) === String(manoeuvre._id)){
      found = true;
    }
    index++;
  }
  return callback(null, found);
}
function countFlightManoeuvre(flightMano, flightManoeuvres, callback){
  var index = 0;
  var count = 0;
  while(index < flightManoeuvres.length){
    if(isSameManoeuvre(flightMano, flightManoeuvres[index])){
      count++;
    }
    index++;
  }
  return callback(null, count);
}
function isSameManoeuvre(mano1, mano2){
  if(String(mano1.manoeuvre._id) !== String(mano2.manoeuvre._id)){
    return false;
  }
  //CHECK SIDE
  if (!isSameManoeuvresBySide(mano1, mano2)){
    return false;
  }

  //CHECK REVERSED
  if (!isSameManoeuvresByPosition(mano1, mano2)){
    return false;
  }
  return true;
}

function isSameManoeuvresBySide(mano1, mano2){
  if(!mano1.side && !mano2.side){
    return true;
  }

  if(mano1.side === mano2.side){
    return true;
  }

  return false;
}

function isSameManoeuvresByPosition(mano1, mano2){
  //CHECK REVERSED MANOEUVRES
  //No Bonus
  if(mano1.bonusType.length === 0 && mano2.bonusType.length === 0){
    return true;
  }
  //no Reversed
  var mano1Reversed = isReversedManoeuvre(mano1);
  var mano2Reversed = isReversedManoeuvre(mano2);

  return mano1Reversed === mano2Reversed;
}

function isReversedManoeuvre(mano){
  var result = false;
  mano.bonusType.forEach(function(b){
    if (b === 'Reversed'){
      result = true;
    }
  });
  return result;
}


function getPreviousManoeuvres(run, competitorId, callback){
  var manoeuvreList = [];
  Flight.find({'competitor._id': competitorId})
    .populate('manoeuvres.manoeuvre')
    .populate('run', 'competition order')
    .exec(function(err, flights){
      flights.forEach(function(flight){
        if(String(flight.run.competition) === String(run.competition) && flight.run.order < run.order){
          flight.manoeuvres.forEach(function(flightManoeuvre){
            manoeuvreList.push(flightManoeuvre);
          });
        }
      });
      return callback(null, manoeuvreList);
    });
}

function isMalus(run, flight, withoutPenaltyManoeuvres, index, callback){
  var step = require('step');
  step(
    function(){
      allowDuplicateManoeuvre(flight.manoeuvres[index].manoeuvre, withoutPenaltyManoeuvres, this);
    },
    function(err, allowDuplicate){
      if (allowDuplicate){
        return callback(null,{index: index, isMalus: false});
      }
      var processedManoeuvres = [];
      if(index > 0){
        processedManoeuvres = flight.manoeuvres.slice(0,index);
      }
      countFlightManoeuvre(flight.manoeuvres[index], processedManoeuvres, this);
    },
    function(err, count){
      if (count > 0){
        return callback(null, {index: index, isMalus: true});
      }
      getPreviousManoeuvres(run, flight.competitor._id, this);
    },
    function (err, previousManoeuvres){
      countFlightManoeuvre(flight.manoeuvres[index], previousManoeuvres, this);
    },
    function(err, countPrevious){
      if( countPrevious > 0){
        return callback(null, {index: index, isMalus: true});
      }
      return callback(null, {index: index, isMalus: false});
    }
  );
}
function getMalusManoeuvres(flight, callback){
  Run.findById(flight.run, function(err, run) {
    if (!run.applyPenalty) {
      return callback(null, []);
    }

    SettingsCtrl.getSeasonSettingsByFlight(flight._id, function (err, settings) {
      var processFlightManoeuvres = [];
      var malusManoeuvres = [];
      var step = require('step');
      step(
        function(){
          var group = this.group();
          var index = 0;
          while(index < flight.manoeuvres.length){
            isMalus(run, flight, settings.withoutPenaltyManoeuvres, index, group());
            index++;
          }
        },
        function(err, malusResults){
          malusResults.forEach(function(result){
            if(result.isMalus){
              malusManoeuvres.push(flight.manoeuvres[result.index]);
            }
          });
          return callback(null, malusManoeuvres);
        }
      );
    });
  });
}

function containManoeuvre(manoeuvreId, manoeuvres, callback){
  var found = false;
  var index = 0;
  while( index < manoeuvres.length && !found){
    if(String(manoeuvres[index]._id) === String(manoeuvreId)){
      found = true;
    }
    index++;
  }
  return callback(null, found);
}
function markManoeuvres(flightId, callback){
  Flight.findById(flightId)
    .populate('manoeuvres.manoeuvre')
    .exec(function(err, flight){
      var step = require('step');
      step(
        function(){
          getBonusManoeuvres(flight.manoeuvres, this.parallel());
          getMalusManoeuvres(flight, this.parallel());
        },
        function(err, bonusManoeuvres, malusManoeuvres){
          if (err){
            return callback(err, null);
          }
          flight.manoeuvres.forEach(function(mano){
            containManoeuvre(mano._id, bonusManoeuvres, function(err, isBonus){
              containManoeuvre(mano._id, malusManoeuvres, function(err, isMalus) {
                mano.applyBonus = isBonus;
                mano.applyMalus = isMalus;
              });
            });
          });
          flight.manoeuvres = flight.manoeuvres.map(function(mano){
            return {
              manoeuvre: mano.manoeuvre._id,
              side: mano.side,
              bonusType: mano.bonusType,
              applyBonus: mano.applyBonus,
              applyMalus: mano.applyMalus
            }
          });
          flight.save(function(err, result){
            return callback(null, result);
          });
        }
      );
    });
}

exports.saveVotes = function(req, res){
  Flight
    .findById(req.params.id)
    .exec(function(err, flight){
      if (err){ return handleError(res, err);}
      if (!flight) { return res.send(404);}
      flight.votes = req.body.votes;
      flight.save(function(err, savedFlight){
        processFlight(flight._id, function(err, processedFlight){
          return res.json(200, processedFlight);
        });
      });
    });
};

exports.reset = function(req, res) {
  Flight.findById(req.params.id, function(err, flight){
    if (err){ return handleError(res, err);}
    if (!flight) { return res.send(404);}
    flight.startTime = null;
    flight.endTime = null;
    flight.manoeuvres = [];
    flight.votes = [];
    flight.bonus = 0;
    flight.malus = 0;
    flight.mean = 0;
    flight.computeResult = 0;
    flight.detailResults = [];
    flight.warnings = 0;
    flight.save(function(err, savedFlight){
      return res.json(200, savedFlight);
    });
  });
};

exports.start = function(req, res){
  Flight.findById(req.params.id, function(err, flight){
    if (err){ return handleError(res, err);}
    if (!flight) { return res.send(404);}
    flight.startTime = new Date();
    flight.save(function(err, savedFlight){
      if(req.body.display){
        Displayer.showFlight(flight._id, function(err, result){
          if (err) {
            console.log('display flight error: ' + err);
          }
        });
      }
      getFlightPopulated(flight._id, function(err, populatedFlight){
        return res.json(200, populatedFlight);
      })
    });
  });
};

exports.end = function(req, res){
  Flight.findById(req.params.id, function(err, flight){
    if (err){ return handleError(res, err);}
    if (!flight) { return res.send(404);}
    flight.endTime = new Date();
    flight.save(function(err, savedFlight){
      if(req.body.display){
        Displayer.showFlightResults(flight._id, function(err, result){
          if (err) {
            console.log('display flight result error: ' + err);
          }
        });
      }
      RunCtrl.updateFlightQualification(flight, function(err, result){
        getFlightPopulated(flight._id, function(err, populatedFlight){
          return res.json(200, populatedFlight);
        })
      });

    });
  });
};

exports.display = function(req, res){
  Displayer.showFlight(req.params.id, function(err, result){
    if (err){
      console.log('display flight error: ' +  err);
      return handleError(res, err);
    }
    return res.json(200, true);
  });

};

exports.displayResults = function(req, res){
  Displayer.showFlightResults(req.params.id, function(err, result){
    if (err){
      console.log('display flight results error: ' +  err);
      return handleError(res, err);
    }
    return res.json(200, true);
  });
};


function processFlight(flightId, callback){
  Flight
    .findById(flightId)
    .populate('run', 'name applyPenalty competition')
    .populate('manoeuvres.manoeuvre')
    .populate('votes.judge')
    .populate('votes.markType')
    .exec(function(err, flight){
      var step = require('step');
      step(
        function(){
          processFlightManoeuvres(flight, this);
        },
        function(err, result){
          if (err){
            throw err;
          }
          flight.malus = result.malus;
          flight.bonus = result.bonus;
          flight.mean = result.mean;


          if(!flight.hasManoeuvresWarnings && result.hasWarning){
            flight.warnings++;
          }

          if(flight.hasManoeuvresWarnings && !result.hasWarning){
            flight.warnings--;
          }

          flight.hasManoeuvresWarnings = result.hasWarning;


          processFlightVotes(flight, this);
        }, function(err, result){
          if (err){
            throw err;
          }

          flight.detailResults = result;
          computeResult(flight, this);
        }, function(err, computeResult){
          if (err){
            return callback(err, null);
          }

          flight.computeResult = computeResult;
          flight.save(function(err, savedFlight){
            if(err){
              return callback(err, null);
            }
            //Update Run qualification
            RunCtrl.updateFlightQualification(flight, function(err, result){
               if(err){
                 console.log('Error updating run qualification');
               }
            });
            getFlightPopulated(flightId, function(err, populatedFlight) {
              return callback(err, populatedFlight);
            });
          });
        }
      );
    });

  // SALVAR y Mandar a procesar las maniobras que eso implica:
// Calcular el Mean
// - buscar las tres maniobras de mayor coefficiente y hallar el promedio
// - OJO: si hace mas de dos maniobras con bonus, la tercera se asumira con coefficient 1 (el mas bajo)
//    es decir la idea es que no aporte esa maniobra al mean

// Calcular el bonus
// - buscar las tres maniobras con mayor bonus (%) y sumarlos

// Calcular el malus
// - Contar de las maniobras realizadas las duplicadas
//     - ver si esta duplicada en la propia lista
//     - si no ver si esta duplicada en las ya realizadas en la competencia

// OJO: VER EL TEMA DE LAS RUN QUE PERMITEN DUPLICADOS


  //SALVAR y si ya estan las maniobras mandar a procesar los votos que eso implica:
  //Calcular para cada MArktype los puntos obtenidos
  // - Landing: PromVotosFAI * %FAI + PromVotosVIP* %VIP
  // - Choleography: Lo mismo pero al final sumar el % de bonus y quitar el % de malus
  // - Tech expression: al sumar el voto se multiplica por el Mean!!
  // - Sync: PromVotosFAI * %FAI + PromVotosVIP* %VIP
}

function processFlightManoeuvres(flight, callback){
  var step = require('step');
  step(
    function(){
      computeManoeuvresMean(flight.manoeuvres, this.parallel());
      computeManoeuvresBonus(flight.manoeuvres, this.parallel());
      computeManoeuvresMalus(flight, this.parallel());
      hasManoeuvresWarning(flight.manoeuvres, this.parallel());
    }, function(err, mean, bonus, malus, hasWarning){
        if(err){
          return callback(err, null);
        }
        return callback(null, {mean : mean, bonus: bonus, malus: malus, hasWarning: hasWarning});
    }
  );
}

//TODO: implement using settings
function computeManoeuvresMean(manoeuvres, callback){
  //Get Bonus manoeuvres with highest coefficient
  //Third manoeuvre with coefficient > 1.95 is discarded (consider coefficient equal 1)
  //Obtain Mean
  var firstManoeuvre = 0;
  var secondManoeuvre = 0;
  var thirdManoeuvre = 0;
  manoeuvres.forEach(function(flightManoeuvre) {
    var manoeuvre = flightManoeuvre.manoeuvre;
    var coefficient = manoeuvre.coefficient ;
    //if (manoeuvre.bonus > 0 && manoeuvre._id != firstBonus._id && manoeuvre._id != secondBonus._id){
    //  coefficient = 1;
    //}
    if (coefficient > thirdManoeuvre){
      if(coefficient > secondManoeuvre){
        if(coefficient > firstManoeuvre){
          //Check rule: only two ith coefficient >= 1.95
          if(secondManoeuvre >= 1.95){
            if(thirdManoeuvre === 0) {
              thirdManoeuvre = 1
            }
          } else {
            thirdManoeuvre = secondManoeuvre;
          }
          secondManoeuvre= firstManoeuvre;
          firstManoeuvre = coefficient;
        }
        else{
          //Check rule: only two ith coefficient >= 1.95
          if(secondManoeuvre >= 1.95){
            if(thirdManoeuvre === 0) {
              thirdManoeuvre = 1
            }
          } else {
            thirdManoeuvre = secondManoeuvre;
          }
          secondManoeuvre = coefficient;
        }
      }
      else{
        //Check rule: only two ith coefficient >= 1.95
        if(coefficient >= 1.95){
          if(thirdManoeuvre === 0) {
            thirdManoeuvre = 1
          }
        } else {
          thirdManoeuvre = coefficient;
        }
      }
    }
  });

  var result = 0;
  result+= (firstManoeuvre > 0)?firstManoeuvre:1;
  result+= (secondManoeuvre > 0)?secondManoeuvre:1;
  result+= (thirdManoeuvre > 0)?thirdManoeuvre:1;
  console.log('MEAN = ' + result/3);
  return callback(null, result/3);
}
function computeManoeuvresBonus(manoeuvres, callback){
  var bonus = 0;
  manoeuvres.forEach(function(mano){
    if(mano.applyBonus){
      mano.bonusType.forEach(function(b) {
        switch (b) {
          case 'Twisted': {
            bonus += mano.manoeuvre.twistedBonus;
            break;
          }
          case 'Flipped': {
            bonus += mano.manoeuvre.flippedBonus;
            break;
          }
          case 'Reversed': {
            bonus += mano.manoeuvre.reversedBonus;
            break;
          }
        }
      });
    }
  });
  return callback(null, bonus);
}
function hasManoeuvresWarning(manoeuvres, callback){
  // var countManoeuvres = 0;
  // manoeuvres.forEach(function(flightManoeuvre){
  //   //TODO: ADD TO SEASON CONFIG
  //   if (flightManoeuvre.manoeuvre.coefficient >= 1.9){
  //     countManoeuvres++;
  //   }
  // });
  // //TODO: ADD TO SEASON CONFIG
  // return callback(null, countManoeuvres > 3);
  return callback(null, false);
}
function computeManoeuvresMalus(flight, callback){
  SettingsCtrl.getSeasonSettingsByFlight(flight._id, function(err, settings) {
    var count = 0;
    flight.manoeuvres.forEach(function (mano) {
      if (mano.applyMalus) {
        count++
      }
    });
    return callback(null, count * settings.penalty);
  });
}
function getJudgesMean(settings, callback){
  var result = {};
  settings.judgesMean.forEach(function(judgeMean){
    result[judgeMean.judgeType] = judgeMean.mean;
  });
  callback(null, result);
}
function processFlightVotes(flight, callback){
  SettingsCtrl.getSeasonSettingsByFlight(flight._id, function(err, settings) {
    var results = [];
    var step = require('step');
    step(
      function () {
        JudgeType.findOne({name: 'FAI'}, this.parallel());
        JudgeType.findOne({name: 'VIP'}, this.parallel());
        getJudgesMean(settings, this.parallel());
      }, function (err, faiJudgeType, vipJudgeType, judgesMean) {
        if (err) {
          return callback(err, null);
        }

        //Run.findById(flight.run, function(err, run){
        var run = flight.run;
        CompetitionCtrl.getMarkTypes(run.competition, function (err, markTypes) {
          _.each(markTypes, function (markType) {
            var result = {markType: markType._id, value: 0};
            var markPoints = 0;
            var faiPoints = 0, faiCount = 0;
            var vipPoints = 0, vipCount = 0;
            _.each(flight.votes, function (vote) {
              if (vote.markType.name === markType.name) {
                if (markType.name === 'Technical expression') {
                  if (String(vote.judge.type) === String(faiJudgeType._id)) {
                    faiPoints += vote.points * flight.mean;
                    faiCount++;
                  } else {
                    vipPoints += vote.points * flight.mean;
                    vipCount++;
                  }
                } else {
                  if (String(vote.judge.type) === String(faiJudgeType._id)) {
                    faiPoints += vote.points;
                    faiCount++;
                  } else {
                    vipPoints += vote.points;
                    vipCount++;
                  }
                }
              }
            });

            if (faiCount > 0 && vipCount > 0) {
              var faiProm = faiPoints / faiCount;
              var vipProm = vipPoints / vipCount;
              var faiValue = faiProm * (judgesMean[faiJudgeType._id]/100);
              var vipValue = vipProm * (judgesMean[vipJudgeType._id]/100);
              markPoints = faiValue + vipValue;
            } else {
              if (faiCount > 0) {
                markPoints = faiPoints / faiCount;
              } else {
                if (vipCount > 0) {
                  markPoints = vipPoints / vipCount
                }
              }
            }

            //discount % peer duplicate manoeuvres and Apply bonus
            if (markType.name === 'Choreography') {
              result.value = markPoints * (100 - flight.malus + flight.bonus ) / 100;
            } else {
              result.value = markPoints;
            }

            if (result.value < 0) {
              result.value = 0;
            }

            results.push(result);
          });
          return callback(null, results);
        });
        //});
      }
    );
  });

}
function getCompetitionMarkTypePercents(settings, competitionType, callback){
  var index = 0;
  var results = {};
  while (index < settings.competitionMarkCoefficients.length &&
        String(settings.competitionMarkCoefficients[index].competitionType) !== String(competitionType)){
    index++;
  }

  if(index < settings.competitionMarkCoefficients.length){
    settings.competitionMarkCoefficients[index].markCoefficients.forEach(function(markTypePercent){
      results[markTypePercent.markType] = markTypePercent.coefficient;
    });
    return callback(null, results)
  }
}
function computeResult(flight, callback){
  SettingsCtrl.getSeasonSettingsByFlight(flight._id, function(err, settings){
    if (flight.warnings >= settings.warningsToDSQ){
      return callback(null, -1);
    } else {
      var step = require('step');
      step(
        function(){
          Competition.findById(flight.run.competition, this);
        },
        function(err, competition){
          getCompetitionMarkTypePercents(settings, competition.type,this);
        },
        function(err, markTypePercents){
          var flightResult = 0;
          _.each(flight.detailResults, function(result){
            var percent = markTypePercents[result.markType];
            flightResult+= result.value * percent/100;
          });
          //warning points
          var warningsPoints = 0;
          if (settings.warningIncremental){
            warningsPoints = settings.warningPoints * (flight.warnings * (flight.warnings + 1)/2);
          } else {
            warningsPoints =  settings.warningPoints * flight.warnings;
          }

          flightResult = (flightResult  > warningsPoints)? flightResult - warningsPoints : 0;
          return callback(null, flightResult );
        }
      );



    }
  });
}
function getFlightPopulated(flightId, callback){
  Flight
    .findById(flightId)
    .populate('manoeuvres.manoeuvre')
    .exec(function (err, flight){
      if (err){ callback(err, null);}
      return callback(null, flight);
    });
}

function handleError(res, err) {
  return res.send(500, err);
}
