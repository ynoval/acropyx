'use strict';

var express = require('express');
var controller = require('./flight.controller');

var router = express.Router();

router.get('/', controller.index);
router.get('/:id', controller.show);
router.post('/', controller.create);
router.put('/:id/reset/', controller.reset);
router.put('/:id/start/', controller.start);
router.put('/:id/end/', controller.end);
router.put('/:id/display/', controller.display);
router.put('/:id/displayResults/', controller.displayResults);
router.put('/:id/addWarning/', controller.addWarning);
router.put('/:id/removeWarning/', controller.removeWarning);
router.put('/:id/manoeuvres/', controller.saveManoeuvres);
router.put('/:id/votes/', controller.saveVotes);
router.put('/:id', controller.update);
router.patch('/:id', controller.update);
router.delete('/:id', controller.destroy);
router.post('/changeOrder', controller.changeOrder);

module.exports = router;
