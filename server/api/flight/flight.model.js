'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var VoteSchema = new Schema({
  judge: {type: mongoose.Schema.Types.ObjectId, ref: 'Judge'},
  markType:{type: mongoose.Schema.Types.ObjectId, ref: 'MarkType'},
  //Check value: 0, 0.5, 1, 1.5 ..., 9.5, 10
  points:Number
});

var ManoeuvreSchema = new Schema({
  manoeuvre: {type: mongoose.Schema.Types.ObjectId, ref: 'Manoeuvre'},
  //TODO: Check : LEFT or RIGHT
  side: String,
  //TODO: Check Twisted, Flipped or Reversed
  bonusType: [{type: String}],
  applyBonus: Boolean,
  applyMalus: Boolean
});

var ResultSchema = new Schema({
  markType:{type: mongoose.Schema.Types.ObjectId, ref: 'MarkType'},
  value: Number
});

var FlightSchema = new Schema({
  competitor: {
    _id: {type: mongoose.Schema.Types.ObjectId},
    name: String,
    //CIVL ranking
    civlRank: Number,
    //APWC ranking
    apwcRank: Number
  },
  startTime: Date,
  endTime: Date,
  votes: [VoteSchema],
  manoeuvres: [ManoeuvreSchema],

  bonus: Number,  //set it in processFlight
  malus: Number,  //set it in processFlight
  mean: Number, //set it in processFlight

  computeResult: Number,  //set it in processFlight
  detailResults: [ResultSchema], //set it in processFlight
  order:Number,
  warnings:{type: Number, default: 0},
  hasManoeuvresWarnings:{type: Boolean, default: false},
  run: {type: mongoose.Schema.Types.ObjectId, ref: 'Run'}
});

module.exports = mongoose.model('Flight', FlightSchema);
