'use strict';

var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

var ReportSchema = new Schema({
  //Report name (the file report name)
  name: String,
  //Title to show and report output
  title: String,
  //Season, Event, Competition, Run, Battle, Flight
  category: String,
  //PDF or CSV
  type: String
});

module.exports = mongoose.model('Report', ReportSchema);
