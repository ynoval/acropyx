/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /reports                         ->  index
 * POST    /reports                         ->  create
 * GET     /reports/:id                     ->  show
 * GET     /reports/:id/pdf/:entityId       ->  generate pdf report
 * GET     /reports/:id/csv/:entityId       ->  generate csv report
 * PUT     /reports/:id                     ->  update
 * DELETE  /reports/:id                     ->  destroy
 */

'use strict';

var _ = require('lodash');
var _this = this;
var config = require('../../config/environment');
var path = require('path');


var Report = require('./report.model');
var Pilot = require('../pilot/pilot.model');
var Team = require('../team/team.model');
var Run = require('../run/run.model');


// Get list of reports
exports.index = function(req, res) {
  Report.find(function (err, reports) {
    if(err) { return handleError(res, err); }
    return res.json(200, reports);
  });
};

// Get a single report
exports.show = function(req, res) {
  Report.findById(req.params.id, function (err, report) {
    if(err) { return handleError(res, err); }
    if(!report) { return res.send(404); }
    return res.json(report);
  });
};

exports.getReport = function(req, res){
  var fs = require('node-fs');
  var report = fs.readFileSync(__dirname +  '/PDFs/' + req.params.name + '.pdf');
  //res.contentType('application/pdf');
  res.setHeader('Content-type', "application/pdf");
  res.setHeader('Content-Length', report.size);
  res.send(report);
};

exports.showByName = function(req, res) {
  _this.getByName(req.params.name, function (err, report) {
    if(err) { return handleError(res, err); }
    if(!report) { return res.send(404); }
    return res.json(report);
  });
};

exports.getByName = function(reportName, callback) {
  Report.findOne({name: reportName}, function (err, report) {
    if(err) { return callback(err, null); }
    if(!report) { return callback(404, null); }
    return callback(null, report);
  });
};

// Creates a new report in the DB.
exports.create = function(req, res) {
  Report.create(req.body, function(err, report) {
    if(err) { return handleError(res, err); }
    return res.json(201, report);
  });
};

// Updates an existing report in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  Report.findById(req.params.id, function (err, report) {
    if (err) { return handleError(res, err); }
    if(!report) { return res.send(404); }
    var updated = _.merge(report, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.json(200, report);
    });
  });
};

// Deletes a report from the DB.
exports.destroy = function(req, res) {
  Report.findById(req.params.id, function (err, report) {
    if(err) { return handleError(res, err); }
    if(!report) { return res.send(404); }
    report.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.send(204);
    });
  });
};


//Generate CSV reports entry
exports.generateCSV = function(req, res) {
  var id = req.params.id;
  var entityId = req.params.entityId;

  console.log('generating CSV report with id:' + id + 'for entity: ' + entityId);
  return res.json({msg: 'generating CSV report with id:' + id + 'for entity: ' + entityId});
};


exports.generateCompetitionSoloResultsPDF = function(competition, callback){
  var runsAmount = 0;
  var resultsData = [];
  var jasper = require('node-jasper')({
    // path: '/opt/jasperreports-6.0.3/',
    path: '/opt/jasperreports/',
    reports: {
      competitionResultsSolo: {
        jasper: __dirname + '/jasper/competitionResultsSolo.jasper',
        jrxml: __dirname + '/jasper/competitionResultsSolo.jrxml',
        conn: 'in_memory_json'
      }
    }
  });

  var Step = require('step');
  Step(
    function(){
      Run.count({competition:competition._id}, this);
    },
    function(err, runsCount){
      runsAmount = runsCount;
      var group = this.group();
      competition.results.forEach(function(result){
        getPilotBasicInformation(result.competitor._id, group());
      });
    }, function(err, competitors){
      for(var i = 0; i < competition.results.length; i++) {
        var result = {
          "rank": i+1,
          "competitor": competitors[i].name,
          "Warnings": competition.results[i].warnings,
          "Result": competition.results[i].overall.toFixed(3),
          "Country": (competitors[i].country)?competitors[i].country.toUpperCase():'',
          "Glider": competitors[i].glider
        };
        for(var j = 0; j < competition.results[i].runResults.length; j++){
          result['r'+ (j + 1)] = competition.results[i].runResults[j].points.toFixed(3);
        }
        resultsData.push(result);
      }

      jasper.ready(function () {
          var fs = require('fs');
          var reportParams = {
            "ACROPYX_COMPETITION": competition.name,
            "ACROPYX_RESULT": (competition.endTime)?'Final overall ranking': 'Intermediate overall results'
          };

          for(var i = 0; i < competition.results[0].runResults.length; i++){
            reportParams["ACROPYX_RUN_TITLE_"+(i+1)] = (i + 1 < runsAmount)?(i+1).toString():'F';
          }


          var currentReport = jasper.export({
            report: 'competitionResultsSolo',
            data: reportParams,
            dataset: resultsData
          }, 'pdf');

          fs.writeFile(path.join(config.root, 'reports/') + competition.code + '_Results.pdf', currentReport,
            function(err){
            if (err) {
              return callback(err, false);
            }
            console.log('Competition Solo Results Report saved!');
            callback(null, true);
          });
      });
    }
  );
};
exports.generateCompetitionSyncResultsPDF = function(competition, callback){
  var runsAmount = 0;
  var resultsData = [];
  var jasper = require('node-jasper')({
    // path: '/opt/jasperreports-6.0.3/',
    path: '/opt/jasperreports/',
    reports: {
      competitionResultsSync: {
        jasper: __dirname + '/jasper/competitionResultsSync.jasper',
        jrxml: __dirname + '/jasper/competitionResultsSync.jrxml',
        conn: 'in_memory_json'
      }
    }
  });

  var Step = require('step');
  Step(
    function(){
      Run.count({competition:competition._id}, this);
    },
    function(err, runsCount){
      runsAmount = runsCount;
      var group = this.group();
      competition.results.forEach(function(result){
        getTeamBasicInformation(result.competitor._id, group());
      });
    }, function(err, competitors){
      for(var i = 0; i < competition.results.length; i++) {
        var result = {
          "rank": i+1,
          "competitor": competitors[i].name,
          "Pilot": competitors[i].pilot1,
          "Country": (competitors[i].country1)?competitors[i].country1.toUpperCase():'',
          "Glider": competitors[i].glider1,
          "Pilot1": competitors[i].pilot2,
          "Country1": (competitors[i].country2)?competitors[i].country2.toUpperCase():'',
          "Glider1": competitors[i].glider2,
          "Warnings": competition.results[i].warnings,
          "Result": competition.results[i].overall.toFixed(3)
        };
        for(var j = 0; j < competition.results[i].runResults.length; j++){
          result['r'+ (j + 1)] = competition.results[i].runResults[j].points.toFixed(3);
        }
        resultsData.push(result);
      }

      jasper.ready(function () {
        var fs = require('fs');
        var reportParams = {
          "ACROPYX_COMPETITION": competition.name,
          "ACROPYX_RESULT": (competition.endTime)?'Final overall ranking': 'Intermediate overall results'
        };

        for(var i = 0; i < competition.results[0].runResults.length; i++){
          reportParams["ACROPYX_RUN_TITLE_"+(i+1)] = (i + 1 < runsAmount)?(i+1).toString():'F';
        }


        var currentReport = jasper.export({
          report: 'competitionResultsSync',
          data: reportParams,
          dataset: resultsData
        }, 'pdf');

        fs.writeFile(path.join(config.root, 'reports/') + competition.code + '_Results.pdf', currentReport,
          function(err){
            if (err) {
              return callback(err, false);
            }
            console.log('Competition Sync Results Report saved!');
            callback(null, true);
          });
      });
    }
  );
};

exports.generateCompetitionSoloResultsFaiCSV = function(competition, callback){
  var json2csv  =  require('json2csv');
  var fields  =  ["Name","NAT","Score","Sex","Birthday","Valid_FAI_licence","Glider","Sponsor","CIVL_ID"];
  var csvData = [];
  var Step = require('step');
  Step(
    function(){
      var group = this.group();
      competition.results.forEach(function(result){
        getPilotInformation(result.competitor._id, group());
      });
    }, function(err, competitors){
      for(var i = 0; i < competitors.length; i++){
        csvData.push(
          {
            "Name" : competitors[i].name,
            "NAT": (competitors[i].country)?competitors[i].country.toUpperCase():"",
            "Score": competition.results[i].overall.toFixed(3),
            "Sex": (competitors[i].isFemale)?"1":"0",
            "Birthday": (competitors[i].birthDate)?formatDate(competitors[i].birthDate):"",
            "Valid_FAI_licence": (competitors[i].hasFaiLicence)?"1":"0",
            "Glider": competitors[i].glider ,
            "Sponsor": competitors[i].sponsors,
            "CIVL_ID": competitors[i].civlId
          }
        );
      }
      json2csv({ data :  csvData, fields :  fields }, function(err, csv){
        if  (err){
          return callback(err, false);
        }
        var fs = require('fs');
        var file = path.join(config.root, 'reports/') +  competition.code + '_Results.csv';
        fs.writeFile(file, csv, function(err){
          if  (err){
            return callback(err, false);
          }
          return callback(null, true);
        });
      });
    }
  );
};

exports.generateCompetitionSyncResultsFaiCSV = function(competition, callback){
  var json2csv  =  require('json2csv');
  var fields  =  ["Name","NAT","Score","Sex","Birthday","Valid_FAI_licence","Glider","Sponsor","CIVL_ID"];
  var csvData = [];
  var Step = require('step');
  Step(
    function(){
      var group = this.group();
      competition.results.forEach(function(result){
        getTeamInformation(result.competitor._id, group());
      });
    }, function(err, competitors){
      for(var i = 0; i < competitors.length; i++){
        csvData.push(
          {
            "Name" : competitors[i].pilot1.name,
            "NAT": (competitors[i].pilot1.country)?competitors[i].pilot1.country.toUpperCase():"",
            "Score": competition.results[i].overall.toFixed(3),
            "Sex": (competitors[i].pilot1.isFemale)?"1":"0",
            "Birthday": (competitors[i].pilot1.birthDate)?formatDate(competitors[i].pilot1.birthDate):"",
            "Valid_FAI_licence": (competitors[i].pilot1.hasFaiLicence)?"1":"0",
            "Glider": competitors[i].pilot1.glider ,
            "Sponsor": competitors[i].pilot1.sponsors,
            "CIVL_ID": competitors[i].pilot1.civlId
          }
        );
        csvData.push(
          {
            "Name" : competitors[i].pilot2.name,
            "NAT": (competitors[i].pilot2.country)?competitors[i].pilot2.country.toUpperCase():"",
            "Score": competition.results[i].overall.toFixed(3),
            "Sex": (competitors[i].pilot2.isFemale)?"1":"0",
            "Birthday": (competitors[i].pilot2.birthDate)?formatDate(competitors[i].pilot2.birthDate):"",
            "Valid_FAI_licence": (competitors[i].pilot2.hasFaiLicence)?"1":"0",
            "Glider": competitors[i].pilot2.glider ,
            "Sponsor": competitors[i].pilot2.sponsors,
            "CIVL_ID": competitors[i].pilot2.civlId
          }
        );
      }
      json2csv({ data :  csvData, fields :  fields }, function(err, csv){
        if  (err){
          return callback(err, false);
        }
        var fs = require('fs');
        var file = path.join(config.root, 'reports/') +  competition.code + '_Results.csv';
        fs.writeFile(file, csv, function(err){
          if  (err){
            return callback(err, false);
          }
          return callback(null, true);
        });
      });
    }
  );
};


exports.generateRunStartingOrderPDF = function(run, callback){
  var jasper = require('node-jasper')({
    // path: '/opt/jasperreports-6.0.3/',
    path: '/opt/jasperreports/',
    reports: {
      runStartingOrder: {
        jasper: __dirname + '/jasper/runStartingOrder.jasper',
        jrxml: __dirname + '/jasper/runStartingOrder.jrxml',
        conn: 'in_memory_json'
      }
    }
  });

  var resultsData = [];

  var Step = require('step');

  Step(
    function(){
      var group = this.group();
      for(var i= 0; i< run.flights.length; i++) {
        getPilotBasicInformation(run.flights[i].competitor._id, group());
      }
    },
    function(err, competitors){
      for(var i= 0; i< competitors.length; i++){
        resultsData.push(
          {
            "rank": i+1,
            "competitor": competitors[i].name,
            "country": (competitors[i].country)?competitors[i].country.toUpperCase():'',
            "glider": competitors[i].glider
          }
        );
      }
      jasper.ready(function () {
        var fs = require('fs');
        var reportParams = {
          "ACROPYX_COMPETITION": run.competition.name,
          "ACROPYX_RUN": "Run: " + run.name
        };

        var currentReport = jasper.export({
          report: 'runStartingOrder',
          data: reportParams,
          dataset: resultsData
        }, 'pdf');

        fs.writeFile(path.join(config.root, 'reports/') +  run.code + '_StartingOrder.pdf', currentReport, function(err){
          if (err) {
            return callback(err, false);
          }
          console.log('Run Starting Order saved!');
          callback(null, true);
        });
      });
    }
  );
};

exports.generateRunStartingOrderSyncPDF = function(run, callback){
  var jasper = require('node-jasper')({
    // path: '/opt/jasperreports-6.0.3/',
    path: '/opt/jasperreports/',
    reports: {
      runStartingOrderSync: {
        jasper: __dirname + '/jasper/runStartingOrderSync.jasper',
        jrxml: __dirname + '/jasper/runStartingOrderSync.jrxml',
        conn: 'in_memory_json'
      }
    }
  });

  var resultsData = [];

  var Step = require('step');

  Step(
    function(){
      var group = this.group();
      for(var i= 0; i< run.flights.length; i++) {
        getTeamBasicInformation(run.flights[i].competitor._id, group());
      }
    },
    function(err, competitors){
      for(var i= 0; i< competitors.length; i++){
        resultsData.push(
          {
            "rank": i+1,
            "competitor": competitors[i].name,
            "pilot": competitors[i].pilot1,
            "country": (competitors[i].country1)?competitors[i].country1.toUpperCase():'',
            "glider": competitors[i].glider1,
            "pilot1": competitors[i].pilot2,
            "country1": (competitors[i].country2)?competitors[i].country2.toUpperCase():'',
            "glider1": competitors[i].glider2
          }
        );
      }
      jasper.ready(function () {
        var fs = require('fs');
        var reportParams = {
          "ACROPYX_COMPETITION": run.competition.name,
          "ACROPYX_RUN": "Run: " + run.name
        };

        var currentReport = jasper.export({
          report: 'runStartingOrderSync',
          data: reportParams,
          dataset: resultsData
        }, 'pdf');

        fs.writeFile(path.join(config.root, 'reports/') +  run.code + '_StartingOrder.pdf', currentReport, function(err){
          if (err) {
            return callback(err, false);
          }
          console.log('Run Starting Order saved!');
          callback(null, true);
        });
      });
    }
  );
};

exports.generateRunManoeuvresPDF = function(run, callback){
  var jasper = require('node-jasper')({
    // path: '/opt/jasperreports-6.0.3/',
    path: '/opt/jasperreports/',
    reports: {
      runManoeuvres: {
        jasper: __dirname + '/jasper/runManoeuvres.jasper',
        jrxml: __dirname + '/jasper/runManoeuvres.jrxml',
        conn: 'in_memory_json'
      }
    }
  });

  var resultsData = [];

  for(var i= 0; i< run.flights.length; i++){
    if (run.flights[i].endTime){
      var result = {
        "competitor": run.flights[i].competitor.name,
        "bonus": (run.flights[i].bonus)?'+'+run.flights[i].bonus+'%':0,
        "penalty": (run.flights[i].malus)?'-'+run.flights[i].malus+'%':0
      };

      var manoeuvres = '';
      for(var j = 0; j< run.flights[i].manoeuvres.length; j++){

        manoeuvres+= '- ' + getBonusType(run.flights[i].manoeuvres[j].bonusType)+ run.flights[i].manoeuvres[j].manoeuvre.name;
        if (run.flights[i].manoeuvres[j].side){
          manoeuvres+= ' ('+ run.flights[i].manoeuvres[j].side + ')'
        }

        manoeuvres+= ' -';

      }
      result.manoeuvres = manoeuvres;
      resultsData.push( result);
    }

  }

  jasper.ready(function () {
    var fs = require('fs');
    var reportParams = {
      language: 'spanish',
      "ACROPYX_COMPETITION": run.competition.name,
      "ACROPYX_RUN": "Run: " + run.name
    };

    var currentReport = jasper.export({
      report: 'runManoeuvres',
      data: reportParams,
      dataset: resultsData
    }, 'pdf');

    fs.writeFile(path.join(config.root, 'reports/') + run.code + '_Manoeuvres.pdf', currentReport, function(err){
      if (err) {
        return callback(err, false);
      }
      console.log('Run Manoeuvres saved!');
      callback(null, true);
    });
  });
};
exports.generateRunResultsPDF = function(run, callback){
  var resultsData = [];
  var jasper = require('node-jasper')({
    // path: '/opt/jasperreports-6.0.3/',
    path: '/opt/jasperreports/',
    reports: {
      runResults: {
        jasper: __dirname + '/jasper/runResults.jasper',
        jrxml: __dirname + '/jasper/runResults.jrxml',
        conn: 'in_memory_json'
      }
    }
  });

  var Step = require('step');
  Step(
    function(){
      var group = this.group();
      run.results.forEach(function(result){
        getPilotBasicInformation(result.competitor._id, group());
      });
    }, function(err, competitors){
      for(var i = 0; i < run.results.length; i++) {
        var result = {
          "rank": i+1,
          "competitor": competitors[i].name,
          "Warnings": run.results[i].warnings,
          "Result": run.results[i].computeResult.toFixed(3),
          "Country": (competitors[i].country)?competitors[i].country.toUpperCase():'',
          "Glider": competitors[i].glider
        };
        for(var j = 0; j < run.results[i].detailResults.length; j++){
          var detailResult = run.results[i].detailResults[j]._doc;
          var markType = detailResult.markType._doc;
          result[markType.name] = detailResult.value.toFixed(3);
        }
        resultsData.push(result);
      }

      jasper.ready(function () {
        var fs = require('fs');
        var reportParams = {
          "ACROPYX_COMPETITION": run.competition.name,
          "ACROPYX_RUN": run.name,
          "ACROPYX_RESULT": (run.endTime)?'Final ranking': 'Intermediate ranking'
        };

        var currentReport = jasper.export({
          report: 'runResults',
          data: reportParams,
          dataset: resultsData
        }, 'pdf');

        fs.writeFile(path.join(config.root, 'reports/') + run.code + '_Results.pdf', currentReport, function(err){
          if (err) {
            return callback(err, false);
          }
          console.log('Run Results Report saved!');
          callback(null, true);
        });
      });
    }
  );
};

exports.generateRunResultsSyncPDF = function(run, callback){
  var resultsData = [];
  var jasper = require('node-jasper')({
    // path: '/opt/jasperreports-6.0.3/',
    path: '/opt/jasperreports/',
    reports: {
      runResultsSync: {
        jasper: __dirname + '/jasper/runResultsSync.jasper',
        jrxml: __dirname + '/jasper/runResultsSync.jrxml',
        conn: 'in_memory_json'
      }
    }
  });

  var Step = require('step');
  Step(
    function(){
      var group = this.group();
      run.results.forEach(function(result){
        getTeamBasicInformation(result.competitor._id, group());
      });
    }, function(err, competitors){
      for(var i = 0; i < run.results.length; i++) {
        var result = {
          "rank": i+1,
          "competitor": competitors[i].name,
          "Pilot": competitors[i].pilot1,
          "Country": (competitors[i].country1)?competitors[i].country1.toUpperCase():'',
          "Glider": competitors[i].glider1,
          "Pilot1": competitors[i].pilot2,
          "Country1": (competitors[i].country2)?competitors[i].country2.toUpperCase():'',
          "Glider1": competitors[i].glider2,
          "Warnings": run.results[i].warnings,
          "Result": run.results[i].computeResult.toFixed(3)
        };
        for(var j = 0; j < run.results[i].detailResults.length; j++){
          var detailResult = run.results[i].detailResults[j]._doc;
          var markType = detailResult.markType._doc;
          result[markType.name] = detailResult.value.toFixed(3);
        }
        resultsData.push(result);
      }

      jasper.ready(function () {
        var fs = require('fs');
        var reportParams = {
          "ACROPYX_COMPETITION": run.competition.name,
          "ACROPYX_RUN": run.name,
          "ACROPYX_RESULT": (run.endTime)?'Final ranking': 'Intermediate ranking'
        };

        var currentReport = jasper.export({
          report: 'runResultsSync',
          data: reportParams,
          dataset: resultsData
        }, 'pdf');

        fs.writeFile(path.join(config.root, 'reports/') + run.code + '_Results.pdf', currentReport, function(err){
          if (err) {
            return callback(err, false);
          }
          console.log('Run Results Report saved!');
          callback(null, true);
        });
      });
    }
  );
};

function getPilotInformation(pilotId, callback){
  var result = {
    name: '',
    birthDate: '',
    country: '',
    glider: '',
    sponsors: '',
    civlId: '',
    hasFaiLicence: false,
    isFemale: true

  };
  Pilot.findById(pilotId)
    .populate('country')
    .populate('sponsors', 'name')
    .exec(function(err, pilot){
      result.name = pilot.name;
      if(pilot.country){
        result.country = pilot.country.ioc;
      }

      if(pilot.dateOfBirth){
        result.birthDate  =  pilot.dateOfBirth;
      }

      if(pilot.glide){
        result.glider = pilot.glide; //TODO: FIX database
      }

      if(pilot.sponsors && pilot.sponsors.length > 0){
        result.sponsors = '';
        pilot.sponsors.forEach(function(sponsor){
          result.sponsors += sponsor.name + ' / ';
        });
        result.sponsors = result.sponsors.slice(0, result.sponsors.length - 3);
      }

      if(pilot.civlId){
        result.civlId =  pilot.civlId;
      }

      if(pilot.hasFaiLicence){
        result.hasFaiLicence = true;
      }

      if(pilot.sex === 'Male'){
        result.isFemale = false;
      }

      return callback(null, result);
    });
}
function getPilotBasicInformation(pilotId, callback){
  var result = {};
  Pilot.findById(pilotId)
    .populate('country')
    .exec(function(err, pilot){
      if(err){
        return callback(err, null);
      }
      if(!pilot){
        return callback('Pilot not found', null);
      }

      result.name = pilot.name;
      result.glider = pilot.glide;
      if(pilot.country){
        result.country = pilot.country.ioc;
      }
      return callback(null, result);
    });
}

function getTeamInformation(teamId, callback) {
  var result = {};
  var Step = require('step');
  Step(
    function () {
      Team.findById(teamId)
        .exec(this);
    },
    function (err, team) {
      result.name = team.name;
      getPilotInformation(team.pilots[0], this.parallel());
      getPilotInformation(team.pilots[1], this.parallel());
    },
    function (err, firstPilot, secondPilot) {
      result.pilot1 = firstPilot;
      result.pilot2 = secondPilot;
      return callback(null, result);
    }
  );
}
function getTeamBasicInformation(teamId, callback){
var result = {};
var Step = require('step');
Step(
  function(){
    Team.findById(teamId)
      .exec(this);
  },
  function(err, team){
    result.name = team.name;
    getPilotBasicInformation(team.pilots[0], this.parallel());
    getPilotBasicInformation(team.pilots[1], this.parallel());
  },
  function (err, firstPilot, secondPilot){
    result.pilot1 = firstPilot.name;
    result.country1 = firstPilot.country;
    result.glider1 = firstPilot.glider;
    result.pilot2 = secondPilot.name;
    result.country2 = secondPilot.country;
    result.glider2 = secondPilot.glider;
    return callback(null, result);
  }
);
}
function formatDate(inputDate){
  var month = inputDate.getMonth()+1;
  return inputDate.getFullYear() + '-' + month + '-' + inputDate.getDate();
}


function getBonusType(bonus){
  var result = '';
  bonus.forEach(function(b){
    result += b + ' ';
  });
  return result;
}

////Generate PDF reports entry
//exports.generatePDF = function(reportIdreq, res){
//  var id = req.params.id;
//
//  var Step = require('step');
//  var Report = require('./report.model');
//  Report.findById(id, function(err, report){
//    if(err) { return handleError(res, err); }
//    if(!report) { return res.send(404); }
//
//    var entityId = req.params.entityId;
//
//
//    switch (report.category){
//      case "SEASON":
//      {
//        var Season = require('../season/season.model');
//        Season.findById(entityId, function(err, season){
//          if(err) { return handleError(res, err); }
//          if(!season) { return res.send(404); }
//          generateSeasonPDFReport(report, season);
//        });
//
//        break;
//      }
//      case "COMPETITION":
//      {
//        var Competition = require('../competition/competition.model');
//        Competition.findById(entityId)
//          .populate('type')
//          .exec(function(err, competition){
//          if(err) { return handleError(res, err); }
//          if(!competition) { return res.send(404); }
//          generateCompetitionPDFReport(report, competition, function(err, pdf){
//            if(err){
//              return handleError(res, err);
//            }
//            res.set({
//              'Content-type': 'application/pdf',
//              'Content-Length': pdf.length
//            });
//            res.send(pdf);
//          });
//        });
//        break;
//      }
//      case "RUN":
//      {
//        var Run = require('../run/run.model');
//        Run.findById(entityId, function(err, run){
//          if(err) { return handleError(res, err); }
//          if(!run) { return res.send(404); }
//          generateRunPDFReport(report, run);
//        });
//        break
//      }
//    }
//  });
//
//};




/***  Season Reports ***/
exports.generateSeasonSoloRankingReportPDF = function(season, callback){
  var resultsData = [];
  var jasper = require('node-jasper')({
    // path: '/opt/jasperreports-6.0.3/',
    path: '/opt/jasperreports/',
    reports: {
      seasonRankingSolo: {
        jasper: __dirname + '/jasper/ranking.jasper',
        jrxml: __dirname + '/jasper/ranking.jrxml',
        conn: 'in_memory_json'
      }
    }
  });

  var Step = require('step');
  Step(
    function(){
      var group = this.group();
      season.soloRanking.forEach(function(competitorResult){
        getPilotBasicInformation(competitorResult.competitor._id, group());
      });
    }, function(err, competitors){
      for(var i = 0; i < season.soloRanking.length; i++) {
        var result = {
          "rank": i+1,
          "competitor": competitors[i].name,
          "Country": (competitors[i].country)?competitors[i].country.toUpperCase():'',
          "Result": season.soloRanking[i].points.toFixed(3)
        };
        for(var j = 0; j < season.soloRanking[i].competitions.length; j++){
          result['r'+ (j + 1)] = (season.soloRanking[i].competitions[j].result !== null)?
            season.soloRanking[i].competitions[j].result.toFixed(3):'';
        }
        resultsData.push(result);
      }

      jasper.ready(function () {
        var fs = require('fs');
        var reportParams = {
          "ACROPYX_RANKING": season.name,
          "ACROPYX_RANKING_TYPE": " SOLO",
          "ACROPYX_RANKING_TITLE": (season.ended)?'Final overall ranking': 'Provisional overall standings'
        };

        for(var i = 0; i < season.soloRanking[0].competitions.length; i++){
          reportParams["ACROPYX_COMPETITION_TITLE_"+(i+1)] = season.soloRanking[0].competitions[i].competition ;
        }


        var currentReport = jasper.export({
          report: 'seasonRankingSolo',
          data: reportParams,
          dataset: resultsData
        }, 'pdf');

        fs.writeFile(path.join(config.root, 'reports/') + season.code + '_RankingSolo.pdf', currentReport,
          function(err){
            if (err) {
              return callback(err, false);
            }
            console.log('Season Solo Ranking Report saved!');
            callback(null, true);
          });
      });
    }
  );
};

/***  Season Reports ***/
exports.generateSeasonSynchroRankingReportPDF = function(season, callback){
  var resultsData = [];
  var jasper = require('node-jasper')({
    // path: '/opt/jasperreports-6.0.3/',
    path: '/opt/jasperreports/',
    reports: {
      seasonRankingSync: {
        jasper: __dirname + '/jasper/rankingSync.jasper',
        jrxml: __dirname + '/jasper/rankingSync.jrxml',
        conn: 'in_memory_json'
      }
    }
  });

  var Step = require('step');
  Step(
    function(){
      var group = this.group();
      season.synchroRanking.forEach(function(competitorResult){
        getTeamBasicInformation(competitorResult.competitor._id, group());
      });
    }, function(err, competitors){
      for(var i = 0; i < season.synchroRanking.length; i++) {
        var result = {
          "rank": i+1,
          "competitor": competitors[i].name,
          "Pilot": competitors[i].pilot1,
          "Country": (competitors[i].country1)?competitors[i].country1.toUpperCase():'',
          "Pilot1": competitors[i].pilot2,
          "Country1": (competitors[i].country2)?competitors[i].country2.toUpperCase():'',
          "Result": season.synchroRanking[i].points.toFixed(3)
        };
        for(var j = 0; j < season.synchroRanking[i].competitions.length; j++){
          result['r'+ (j + 1)] = (season.synchroRanking[i].competitions[j].result !== null)?
            season.synchroRanking[i].competitions[j].result.toFixed(3):'';
        }
        resultsData.push(result);
      }

      jasper.ready(function () {
        var fs = require('fs');
        var reportParams = {
          "ACROPYX_RANKING": season.name,
          "ACROPYX_RANKING_TYPE": " SYNCHRO",
          "ACROPYX_RANKING_TITLE": (season.ended)?'Final overall ranking': 'Provisional overall standings'
        };

        for(var i = 0; i < season.synchroRanking[0].competitions.length; i++){
          reportParams["ACROPYX_COMPETITION_TITLE_"+(i+1)] = season.synchroRanking[0].competitions[i].competition ;
        }


        var currentReport = jasper.export({
          report: 'seasonRankingSync',
          data: reportParams,
          dataset: resultsData
        }, 'pdf');

        fs.writeFile(path.join(config.root, 'reports/') + season.code + '_RankingSynchro.pdf', currentReport,
          function(err){
            if (err) {
              return callback(err, false);
            }
            console.log('Season Synchro Ranking Report saved!');
            callback(null, true);
          });
      });
    }
  );
};
/*** END Season Reports ***/

/*** Competition Reports ***/
function getJasperReporter(reportName, callback){
  var jasper = require('node-jasper')({
    // path: '/opt/jasperreports-6.0.3/',
    path: '/opt/jasperreports/',
    reports: {
      current_report: {
        jasper: __dirname + '/jasper/' + reportName + '.jasper',
        jrxml: __dirname + '/jasper/' + reportName + '.jrxml',
        conn: 'in_memory_json'
      }
    }
  });
  return callback(null, jasper);
}
//function generateCompetitionPDFReport(report, competition, callback){
//  var Step = require('step');
//  var reportOutputFile = competition.code + '_Results.pdf';
//
//  Step(
//    function prepareReport(){
//      getJasperReporter(report, this.parallel());
//      getCompetitionResultsReportInfo(competition, this.parallel());
//    },
//    function executeReport(err, jasper, reportData){
//      jasper.ready(function () {
//        var fs = require('fs');
//        var currentReport = jasper.export({
//          report: 'current_report',
//          //data: { language: 'english', params: reportData.params},
//          data: { params: reportData.params},
//          dataset: reportData.data
//        }, 'pdf');
//
//        fs.writeFile(path.join(config.root, 'reports/')  + reportOutputFile, currentReport);
//        callback(null, currentReport);
//      });
//    }
//  );
//}
//
//function getCompetitionResultsReportInfo(competition, callback){
//  var info = {
//    params: { ACROPYX_COMPETITION: competition.name }
//  };
//
//  switch (competition.type.name){
//    case  "Solo":{
//      info.params.ACROPYX_RESULT = (competition.endTime)?'Final SOLO overall ranking': 'Intermediate SOLO overall results';
//      break;
//    }
//    case  "Synchro":
//    {
//      info.params.ACROPYX_RESULT = (competition.endTime)?'Final SYNCHRO overall ranking': 'Intermediate SYNCHRO overall results';
//      break;
//    }
//  }
//
//  var CompetitionCtrl = require('../competition/competition.controller');
//  CompetitionCtrl.getResults(competition._id, function(err, data){
//    if (err){
//      callback(err, null);
//    }
//    info.data = []; //data;
//    callback(null, info);
//  });
//}

/*** END Competition Reports ***/


/*** Run Reports ***/
function generateRunPDFReport(report, run){

}
/*** END Run Reports ***/

function handleError(res, err) {
  return res.send(500, err);
}


