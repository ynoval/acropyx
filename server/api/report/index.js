'use strict';

var express = require('express');
var controller = require('./report.controller');

var router = express.Router();

router.get('/', controller.index);
router.get('/showReport/:name',controller.getReport);
router.get('/:id',controller.show);

//router.get('/:id/pdf/:entityId',controller.generatePDF);
//router.get('/:id/csv/:entityId',controller.generateCSV);

router.post('/', controller.create);
router.put('/:id', controller.update);
router.patch('/:id', controller.update);
router.delete('/:id', controller.destroy);

module.exports = router;
