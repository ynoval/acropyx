/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /markTypes              ->  index
 * POST    /markTypes              ->  create
 * GET     /markTypes/:id          ->  show
 * PUT     /markTypes/:id          ->  update
 * DELETE  /markTypes/:id          ->  destroy
 */

'use strict';

var _ = require('lodash');
var MarkType = require('./markType.model');

// Get list of markTypes
exports.index = function(req, res) {
  MarkType.find(function (err, markTypes) {
    if(err) { return handleError(res, err); }
    return res.json(200, markTypes);
  });
};

// Get a single markType
exports.show = function(req, res) {
  MarkType.findById(req.params.id, function (err, markType) {
    if(err) { return handleError(res, err); }
    if(!markType) { return res.send(404); }
    return res.json(markType);
  });
};

// Creates a new markType in the DB.
exports.create = function(req, res) {
  MarkType.create(req.body, function(err, markType) {
    if(err) { return handleError(res, err); }
    return res.json(201, markType);
  });
};

// Updates an existing markType in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  MarkType.findById(req.params.id, function (err, markType) {
    if (err) { return handleError(res, err); }
    if(!markType) { return res.send(404); }
    var updated = _.merge(markType, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.json(200, markType);
    });
  });
};

// Deletes a markType from the DB.
exports.destroy = function(req, res) {
  MarkType.findById(req.params.id, function (err, markType) {
    if(err) { return handleError(res, err); }
    if(!markType) { return res.send(404); }
    markType.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.send(204);
    });
  });
};

function handleError(res, err) {
  return res.send(500, err);
}
