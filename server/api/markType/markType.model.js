'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var MarkTypeSchema = new Schema({
  //Mark Definition name
  name: String
});

module.exports = mongoose.model('MarkType', MarkTypeSchema);
