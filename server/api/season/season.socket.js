/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var season = require('./season.model');

exports.register = function(socket) {
  season.schema.post('save', function (doc) {
    onSave(socket, doc);
  });
  season.schema.post('remove', function (doc) {
    onRemove(socket, doc);
  });
}

function onSave(socket, doc, cb) {
  socket.emit('season:save', doc);
}

function onRemove(socket, doc, cb) {
  socket.emit('season:remove', doc);
}
