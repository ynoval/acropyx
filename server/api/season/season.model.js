'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var SeasonSchema = new Schema({
  //Season name
  name: {type: String, required: true, unique: true},
  //Season code is used to generate the displayer URL and Report info
  //ex: Acro2015.acropyx.com
  code: {type: String, required: true, unique: true},
  //settings Id
  settings: {type: mongoose.Schema.Types.ObjectId, ref: 'Setting'},
  // the season is ended (used to collapse all season data)
  ended: Boolean,
  // the season events list
  //events: [{type: mongoose.Schema.Types.ObjectId, ref: 'Event'}],
  //the season sponsors
  sponsors: [{type:mongoose.Schema.Types.ObjectId, ref: 'Sponsor'}],
  //Season and events are showing in acropyx.com home page
  isPublic: {type: Boolean, default: false}
});

module.exports = mongoose.model('Season', SeasonSchema);
