'use strict';

var express = require('express');
var controller = require('./season.controller');

var router = express.Router();

router.get('/', controller.index);
router.get('/:id/displayUrl', controller.displayUrl);
router.get('/:id/events', controller.events);
router.get('/:id/rankingSolo', controller.rankingSolo);
router.put('/:id/generateSoloRankingReportPDF', controller.generateSoloRankingReportPDF);
router.get('/:id/rankingSoloFemale', controller.rankingSoloFemale);
router.get('/:id/rankingSynchro', controller.rankingSynchro);
router.put('/:id/generateSynchroRankingReportPDF', controller.generateSynchroRankingReportPDF);
router.get('/:id/rankingSynchroFemale', controller.rankingSynchroFemale);
router.get('/:id', controller.show);
router.post('/', controller.create);
router.put('/:id/displayRankings', controller.displayRankings);
router.put('/:id', controller.update);
router.patch('/:id', controller.update);
router.delete('/:id', controller.destroy);

module.exports = router;
