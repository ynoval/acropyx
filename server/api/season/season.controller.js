/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /seasons              ->  index
 * POST    /seasons              ->  create
 * GET     /seasons/:id          ->  show
 * PUT     /seasons/:id          ->  update
 * DELETE  /seasons/:id          ->  destroy
 */

'use strict';

var _ = require('lodash');
var config = require('../../config/environment');
var Season = require('./season.model');
var Event = require('../event/event.model');
var Competition = require('../competition/competition.model');
var CompetitionCtrl = require('../competition/competition.controller');
var Run = require('../run/run.model');
var EventCtrl = require('../event/event.controller');
var Displayer = require('../../displayer/displayer.service');
var ReportCtrl = require('../report/report.controller');
var SettingsCtrl = require('../setting/setting.controller');

var _this = this;
// Get list of seasons
exports.index = function(req, res) {
  Season.find(function (err, seasons) {
    if(err) { return handleError(res, err); }
    return res.json(200, seasons);
  });
};
// Get list of events
exports.events = function(req, res) {
  Event.find({season: req.params.id},{logo: 0},function (err, events) {
    if(err) { return handleError(res, err); }
    return res.json(200, events);
  });
};
//Get display URL
exports.displayUrl = function(req, res){
  Season.findById(req.params.id, function(err, season){
    if (err){
      return handleError(res, err);
    }
    if(!season) { return res.send(404); }
    var displayUrl = {
      url: 'http://' + season.code + '.' + config.displayDomain + ':' + config.displayPort
    };
    return res.json(displayUrl);
  });
};

function getRunsAmount(seasonId, callback){
  var results = {
    solo: 0,
    synchro: 0
  };

  var step = require('step');
  var validCompetitions = [];

  step(
    function() {
      Event.find({season: seasonId},{logo: 0})
        .exec(this);
    },
    function(err, events) {
      var group = this.group();
      events.forEach(function (event) {
        Competition.find({event: event._id})
          .populate('type')
          .exec(group());
      });
    },
    function (err, competitionList){

      var group = this.group();
      competitionList.forEach(function(competitions){
          competitions.forEach(function(competition) {
            validCompetitions.push(competition);
            Run.count({competition: competition._id, endTime: {$ne: null}}, group());
          });
      })
    }, function(err, runAmounts){
      for(var i = 0; i < validCompetitions.length; i++){
        if(validCompetitions[i].type.name === 'Solo'){
          results.solo += runAmounts[i];
        } else{
          results.synchro += runAmounts[i];
        }
      }
      return callback(null, results);
    }
  );
}

exports.getRankings = function(seasonId, callback){
  var step = require('step');

  step(
    function(){
      getRunsAmount(seasonId, this.parallel());
      computeRankingSolo(seasonId, this.parallel());
      computeRankingSynchro(seasonId, this.parallel());
    }, function(err, runAmounts, rankingSolo, rankingSynchro){
      var results = {
        solo: {
          runsAmount: runAmounts.solo,
          competitors: rankingSolo
        },
        synchro : {
          runsAmount: runAmounts.synchro,
          competitors: rankingSynchro
        }
      };

      return callback(null, results);
    }
  );
};


function getSoloCompetitions(eventId, callback){
  Competition.find({event: eventId})
    .populate('type')
    .exec(function(err, competitions){
      var startedCompetitions = [];
      competitions.forEach(function(competition){
        if(competition.type.name === 'Solo' && competition.startTime){
          startedCompetitions.push(competition);
        }
      });
      return callback(null, startedCompetitions);
    });
}

exports.rankingSolo = function(req, res) {

  computeRankingSolo(req.params.id, function (err, ranking) {
    if(err) { return handleError(res, err); }
    return res.json(200, ranking);
  });
};

function computeRankingSolo(seasonId, callback){
  //1- Obtener los events
  //2- Obtener las competitions del tipo Solo
  //3- Obtener los resultados con compensacion para esas competencias
  //5- unir los  resultados
  var step = require('step');
  step(
    function (){
      Event.find({season: seasonId, isStarted: true}, {logo: 0})
        .sort('startTime')
        .exec(this);
    }, function (err, events) {
      if(err) { return callback(err, null);}
      var group = this.group();
      events.forEach(function(event){
        getSoloCompetitions(event._id, group());
      });
    }, function(err, eventsCompetitionList){
      var group = this.group();
      eventsCompetitionList.forEach(function(eventCompetitions){
        eventCompetitions.forEach(function(competition){
          getCompetitionResultsWithCompensation(competition._id, group());
        });
      });
    }, function(err, competitionsResults){
      var ranking = [];
      competitionsResults.forEach(function(competitionResults){
        competitionResults.results.forEach(function(competitionResult){
          if(competitionResult){
            var index = 0;
            var found = false;
            while(index < ranking.length && !found){
              if(String(ranking[index].competitor._id) === String(competitionResult.competitor._id)){
                found = true;
              } else {
                index++;
              }
            }
            if(found){
              ranking[index].points += competitionResult.overall;
            } else {
              ranking.push({
                competitor: competitionResult.competitor,
                points: competitionResult.overall,
                competitions: []
              });
            }
          }
        });
      });

      //Add competitions details
      for(var i = 0 ; i < ranking.length; i++){
        competitionsResults.forEach(function(competitionResults){
          var competitionCode = competitionResults.code;
          var index = 0;
          var found = false;
          while(index < competitionResults.results.length && !found){
            if(String(ranking[i].competitor._id) === String(competitionResults.results[index].competitor._id)){
              found = true;
            } else {
              index++;
            }
          }
          if(found){
            ranking[i].competitions.push({competition: competitionCode , result: competitionResults.results[index].overall})
          } else {
            ranking[i].competitions.push({competition: competitionCode , result: null})
          }
        });
      }
      return callback(null, _.sortBy(ranking, 'points').reverse());
    }
  );
}
function getCompetitionResultsWithCompensation(competitionId, callback){
  Competition.findById(competitionId, function(err, competition){
    var competitionResults = {
      code: competition.code
    };
    CompetitionCtrl.getResultsWithCompensation(competition._id, function(err, results){
      competitionResults.results = results;
      return callback(null, competitionResults);
    });
  });
}


function getSynchroCompetitions(eventId, callback){
  Competition.find({event: eventId})
    .populate('type')
    .exec(function(err, competitions){
      var startedCompetitions = [];
      competitions.forEach(function(competition){
        if(competition.type.name === 'Synchro' && competition.startTime){
          startedCompetitions.push(competition);
        }
      });
      return callback(null, startedCompetitions);
    });
}
exports.rankingSynchro = function(req, res) {
  computeRankingSynchro(req.params.id, function (err, ranking) {
    if (err) {
      return handleError(res, err);
    }
    return res.json(200, ranking);
  });
};
//TODO: refactoring - create only one method to compute ranking- using a list of competitions
function computeRankingSynchro (seasonId, callback){
  //1- Obtener los events
  //2- Obtener las competitions
  //3- Obtener los resultados con compensacion
  //5- unir esos resultados
  var step = require('step');
  step(
    function (){
      Event.find({season: seasonId}, {logo: 0})
      .exec(this);
    }, function (err, events) {
      if(err) { return callback(err, null); }
      var group = this.group();
      events.forEach(function(event){
        getSynchroCompetitions(event._id, group());
      });
    }, function(err, eventsCompetitionList){
      var group = this.group();
      eventsCompetitionList.forEach(function(eventCompetitions){
        eventCompetitions.forEach(function(competition){
          getCompetitionResultsWithCompensation(competition._id, group());
        });
      });
    }, function(err, competitionsResults){
      var ranking = [];
      competitionsResults.forEach(function(competitionResults){
        competitionResults.results.forEach(function(result){
          if(result){
            var index = 0;
            var found = false;
            while(index < ranking.length && !found){
              if(String(ranking[index].competitor._id) === String(result.competitor._id)){
                found = true;
              } else {
                index++;
              }
            }
            if(found){
              ranking[index].points += result.overall;
            } else {
              ranking.push({
                competitor: result.competitor,
                points: result.overall,
                competitions: []
              });
            }
          }
        });
      });

      //Add competitions details
      for(var i = 0 ; i < ranking.length; i++){
        competitionsResults.forEach(function(competitionResults){
          var competitionCode = competitionResults.code;
          var index = 0;
          var found = false;
          while(index < competitionResults.results.length && !found){
            if(String(ranking[i].competitor._id) === String(competitionResults.results[index].competitor._id)){
              found = true;
            } else {
              index++;
            }
          }
          if(found){
            ranking[i].competitions.push({competition: competitionCode , result: competitionResults.results[index].overall})
          } else {
            ranking[i].competitions.push({competition: competitionCode , result: null})
          }
        });
      }
      return callback(null, _.sortBy(ranking, 'points').reverse());
    }
  );
};


exports.rankingSoloFemale = function(req, res) {
  return res.json(200, []);
};
exports.rankingSynchroFemale = function(req, res) {
  return res.json(200, []);
};

// Get a single season
exports.show = function(req, res) {
  Season
  .findById(req.params.id)
  .populate('sponsors', 'name')
  .exec(function (err, season) {
    if(err) { return handleError(res, err); }
    if(!season) { return res.send(404); }
    return res.json(season);
  });
};

// Creates a new season in the DB.
exports.create = function(req, res) {
  var SettingsCtrl = require('../setting/setting.controller');
  SettingsCtrl.createSeasonSettings(req.body.name, function(err, settings){
    if(err) { return handleError(res, err); }
    var newSeason = {
      name : req.body.name,
      code: req.body.code,
      sponsors:[],
      settings: settings._id
    };
    Season.create(newSeason, function(err, season) {
      if(err) { return handleError(res, err); }
      return res.json(201, season);
    });
  });
};

// Updates an existing season in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  Season.findById(req.params.id, function (err, season) {
    if (err) { return handleError(res, err); }
    if(!season) { return res.send(404); }
    //TODO: FIX!!
    //Saving sponsors
    req.body.sponsors = req.body.sponsors.map(function(option) { return option._id; });
    season.sponsors = req.body.sponsors;
    req.body.sponsors = [];

    var updated = _.merge(season, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.json(200, season);
    });
  });
};

// Deletes a season from the DB.
exports.destroy = function(req, res) {
  deleteSeason(req.params.id, function(err, result){
    if(err){
      if (err === 404){
        return  res.send(404);
      }
      return handleError(res, err);
    }
    return res.send(204);
  });
  //Season.findById(req.params.id, function (err, season) {
  //  if(err) { return handleError(res, err); }
  //  if(!season) { return res.send(404); }
  //  //TODO: Remove Events
  //  season.remove(function(err) {
  //    if(err) { return handleError(res, err); }
  //    return res.send(204);
  //  });
  //});
};
exports.delete = function(seasonId, callback){
  deleteSeason(seasonId, function(err, result){
    return callback(err, result);
  });
};
function deleteSeason(seasonId, callback){
  Season.findById(seasonId, function (err, season) {
    if(err) { return callback(err, false); }
    if(!season) { return callback(404, false); }
    //Remove Events
    Event.find({season: seasonId}, '_id', function(err, eventsId){
      eventsId.forEach(function(eventId){
        EventCtrl.delete(eventId, function(err, result){
          if(err){
            return callback(err, false);
          }
        });
      });

      SettingsCtrl.delete(season.settings, function(err, result){
        if(err){
          return callback(err, false);
        }
        season.remove(function(err) {
          if(err) { return callback(err, false); }
          return callback(null, true);
        });
      });
    });

  });
}

exports.displayRankings = function(req, res) {
  Season.findById(req.params.id, function (err, season) {
    if (err) {
      return handleError(res, err);
    }
    if (!season) {
      return res.send(404);
    }
    Displayer.showSeasonRankings(req.params.id, function (err, result) {
      if (err) {
        return handleError(res, err);
      }
      return res.json(200, result);
    });
  });
};


exports.generateSoloRankingReportPDF = function(req, res){
  _this.generateSeasonSoloRankingReportPDF(req.params.id, function(err, result){
    if(err){ return handleError(res, err);}
    return res.json(200, result);
  });
};

exports.generateSeasonSoloRankingReportPDF = function(seasonId, callback){
  Season.findById(seasonId, function(err, season){
    if (err){
      return callback(err, false);
    }
    if (!season){
      return callback('404', false);
    }
    computeRankingSolo(seasonId, function(err, ranking) {
      if (err) {
        return callback(err, false);
      }
      season.soloRanking = ranking;
      ReportCtrl.generateSeasonSoloRankingReportPDF(season, function (err, result) {
        if (err) {
          return callback(err, null);
        }
        return callback(null, result);
      });
    });
  });
};

exports.generateSynchroRankingReportPDF = function(req, res){
  _this.generateSeasonSynchroRankingReportPDF(req.params.id, function(err, result){
    if(err){ return handleError(res, err);}
    return res.json(200, result);
  });
};

exports.generateSeasonSynchroRankingReportPDF = function(seasonId, callback){
  Season.findById(seasonId)
    .populate('competitions')
    .exec(function(err, season){
    if (err){
      return callback(err, false);
    }
    if (!season){
      return callback('404', false);
    }
    computeRankingSynchro(seasonId, function(err, ranking) {
      if (err) {
        return callback(err, false);
      }
      season.synchroRanking = ranking;
      ReportCtrl.generateSeasonSynchroRankingReportPDF(season, function (err, result) {
        if (err) {
          return callback(err, null);
        }
        return callback(null, result);
      });
    });
  });
};


function handleError(res, err) {
  return res.send(500, err);
}
