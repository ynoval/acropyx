'use strict';

var express = require('express');
var controller = require('./battle.controller');

var router = express.Router();

router.put('/display', controller.display);
router.put('/displayResults', controller.displayResults);


module.exports = router;
