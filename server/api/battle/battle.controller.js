'use strict';

var Flight = require('../flight/flight.model');
var Displayer = require('../../displayer/displayer.service');


exports.display = function(req, res){
  Flight.findById(req.body.firstFlightId, function(err, firstFlight){
    if (err) { return handleError(res, err); }
    if(!firstFlight) { return res.send(404); }
    Flight.findById(req.body.secondFlightId, function(err, secondFlight){
      if (err) { return handleError(res, err); }
      if(!secondFlight) { return res.send(404); }
      Displayer.showBattle(firstFlight._id, secondFlight._id, function(err, result){
        if (err){
          return handleError(res, err);
        }
        return res.json(200, result);
      });
    });
  });
};

exports.displayResults = function(req, res){
  Flight.findById(req.body.firstFlightId, function(err, firstFlight){
    if (err) { return handleError(res, err); }
    if(!firstFlight) { return res.send(404); }
    Flight.findById(req.body.secondFlightId, function(err, secondFlight){
      if (err) { return handleError(res, err); }
      if(!secondFlight) { return res.send(404); }
      Displayer.showBattleResults(firstFlight._id, secondFlight._id, function(err, result){
        if (err){
          return handleError(res, err);
        }
        return res.json(200, result);
      });
    });
  });
};


function handleError(res, err) {
  return res.send(500, err);
}
