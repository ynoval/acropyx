/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var Judge = require('./judge.model');

exports.register = function(socket) {
  Judge.schema.post('save', function (doc) {
    onSave(socket, doc);
  });
  Judge.schema.post('remove', function (doc) {
    onRemove(socket, doc);
  });
}

function onSave(socket, doc, cb) {
  socket.emit('Judge:save', doc);
}

function onRemove(socket, doc, cb) {
  socket.emit('Judge:remove', doc);
}
