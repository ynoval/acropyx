/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /judges              ->  index
 * POST    /judges              ->  create
 * GET     /judges/:id          ->  show
 * PUT     /judges/:id          ->  update
 * DELETE  /judges/:id          ->  destroy
 */

'use strict';

var _ = require('lodash');
var Judge = require('./judge.model');

// Get list of judges
exports.index = function(req, res) {
  Judge.find()
  .sort('name')
  .exec(function (err, judges) {
    if(err) { return handleError(res, err); }
    return res.json(200, judges);
  });
};

// Get a single judge
exports.show = function(req, res) {
  Judge.findById(req.params.id, function (err, judge) {
    if(err) { return handleError(res, err); }
    if(!judge) { return res.send(404); }
    return res.json(judge);
  });
};

// Creates a new judge in the DB.
exports.create = function(req, res) {
  Judge.create(req.body, function(err, judge) {
    if(err) { return handleError(res, err); }
    return res.json(201, judge);
  });
};

// Updates an existing judge in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  Judge.findById(req.params.id, function (err, judge) {
    if (err) { return handleError(res, err); }
    if(!judge) { return res.send(404); }
    var updated = _.merge(judge, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.json(200, judge);
    });
  });
};

// Deletes a judge from the DB.
exports.destroy = function(req, res) {
  Judge.findById(req.params.id, function (err, judge) {
    if(err) { return handleError(res, err); }
    if(!judge) { return res.send(404); }
    judge.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.send(204);
    });
  });
};

function handleError(res, err) {
  return res.send(500, err);
}
