'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var JudgeSchema = new Schema({
  //judge name
  name: String,
  //Type: FAI, VIP
  type: {type: mongoose.Schema.Types.ObjectId, ref: 'JudgeType'}
});

module.exports = mongoose.model('Judge', JudgeSchema);
