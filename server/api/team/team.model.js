'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var TeamSchema = new Schema({
  //Team name
  name: String,
  //Pilot1
  pilots: [{type: mongoose.Schema.Types.ObjectId, ref: 'Pilot'}],
  //best tem result
  bestResults: String
});

module.exports = mongoose.model('Team', TeamSchema);
