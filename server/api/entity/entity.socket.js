/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var ENTITY = require('./entity.model');

exports.register = function(socket) {
  ENTITY.schema.post('save', function (doc) {
    onSave(socket, doc);
  });
  ENTITY.schema.post('remove', function (doc) {
    onRemove(socket, doc);
  });
};

function onSave(socket, doc, cb) {
  socket.emit('ENTITY:save', doc);
}

function onRemove(socket, doc, cb) {
  socket.emit('ENTITY:remove', doc);
}
