/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /ENTITIES              ->  index
 * POST    /ENTITIES              ->  create
 * GET     /ENTITIES/:id          ->  show
 * PUT     /ENTITIES/:id          ->  update
 * DELETE  /ENTITIES/:id          ->  destroy
 */

'use strict';

var _ = require('lodash');
var ENTITY = require('./entity.model');

// Get list of ENTITIES
exports.index = function(req, res) {
  ENTITY.find(function (err, ENTITIES) {
    if(err) { return handleError(res, err); }
    return res.json(200, ENTITIES);
  });
};

// Get a single ENTITY
exports.show = function(req, res) {
  ENTITY.findById(req.params.id, function (err, ENTITY) {
    if(err) { return handleError(res, err); }
    if(!ENTITY) { return res.send(404); }
    return res.json(ENTITY);
  });
};

// Creates a new ENTITY in the DB.
exports.create = function(req, res) {
  ENTITY.create(req.body, function(err, ENTITY) {
    if(err) { return handleError(res, err); }
    return res.json(201, ENTITY);
  });
};

// Updates an existing ENTITY in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  ENTITY.findById(req.params.id, function (err, ENTITY) {
    if (err) { return handleError(res, err); }
    if(!ENTITY) { return res.send(404); }
    var updated = _.merge(ENTITY, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.json(200, ENTITY);
    });
  });
};

// Deletes a ENTITY from the DB.
exports.destroy = function(req, res) {
  ENTITY.findById(req.params.id, function (err, ENTITY) {
    if(err) { return handleError(res, err); }
    if(!ENTITY) { return res.send(404); }
    ENTITY.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.send(204);
    });
  });
};

function handleError(res, err) {
  return res.send(500, err);
}
