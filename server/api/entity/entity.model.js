'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var ENTITYSchema = new Schema({
  //ENTITY name
  name: String
});

module.exports = mongoose.model('ENTITY', ENTITYSchema);
