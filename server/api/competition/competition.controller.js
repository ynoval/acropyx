/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /competitions              ->  index
 * POST    /competitions              ->  create
 * GET     /competitions/:id          ->  show
 * PUT     /competitions/:id          ->  update
 * DELETE  /competitions/:id          ->  destroy
 */

'use strict';

var _ = require('lodash');
var Competition = require('./competition.model');
var Run = require('../run/run.model');
var RunCtrl = require('../run/run.controller');
var CompetitionType = require('../competitionType/competitionType.model');
var Displayer = require('../../displayer/displayer.service');
var Flight = require('../flight/flight.model');
var ReportCtrl = require('../report/report.controller');

var _this = this;

var Pilot = require('../pilot/pilot.model');
var Team = require('../team/team.model');

// Get list of competitions
exports.index = function(req, res) {
  Competition
    .find()
    .populate('event', 'name')
    .exec(function (err, competitions) {
    if(err) { return handleError(res, err); }
    return res.json(200, competitions);
  });
};
// Get list of runs
exports.runs = function(req, res) {
  getCompetitionRuns(req.params.id, function(err, runs){
    if(err) { return handleError(res, err); }
    return res.json(200, runs);
  })
};

// Get list of markTypes
exports.markTypes = function(req, res) {
  _this.getMarkTypes(req.params.id, function(err, markTypes){
    if(err) { return handleError(res, err); }
    if(!markTypes) { return res.send(404); }
    return res.json(200, markTypes);
  });
};

exports.getMarkTypes = function(competitionId, callback){
  Competition.findById(competitionId, function(err, competition){
    if(err) { return callback(err, null); }
    if(!competition) { return callback(404, null); }

    CompetitionType.findById(competition.type)
      .populate('markTypes')
      .exec(function(err, competitionType){
        if(err) { return callback(err, null); }
        if(!competitionType) { return callback(404, null); }
        return callback(null, competitionType.markTypes);
      });
  });
};

// Get a single competition
exports.show = function(req, res) {
  Competition.findById(req.params.id)
    .populate('event', 'name')
    .populate('type', 'name')
    .exec(function (err, competition) {
    if(err) { return handleError(res, err); }
    if(!competition) { return res.send(404); }
    return res.json(competition);
  });
};

// Creates a new competition in the DB.
exports.create = function(req, res) {
  Competition.create(req.body, function(err, competition) {
    if(err) { return handleError(res, err); }
    return res.json(201, competition);
  });
};

// Updates an existing competition in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  Competition.findById(req.params.id, function (err, competition) {
    if (err) { return handleError(res, err); }
    if(!competition) { return res.send(404); }
    delete req.body.runs;
    delete req.body.event;
    delete req.body.type;

    competition.competitors = req.body.competitors;
    delete req.body.competitors;

    var updated = _.merge(competition, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.json(200, competition);
    });
  });
};

// Deletes a competition from the DB.
exports.destroy = function(req, res) {
  //Competition.findById(req.params.id, function (err, competition) {
  //  if(err) { return handleError(res, err); }
  //  if(!competition) { return res.send(404); }
  //  //TODO: Remove Runs
  //  competition.remove(function(err) {
  //    if(err) { return handleError(res, err); }
  //    return res.send(204);
  //  });
  //});
  deleteCompetition(req.params.id, function(err, result){
    if(err){
      if (err === 404){
        return  res.send(404);
      }
      return handleError(res, err);
    }
    return res.send(204);
  });
};
exports.delete = function(competitionId, callback){
  deleteCompetition(competitionId, function(err, result){
    return callback(err, result);
  });
};

function deleteCompetition(competitionId, callback){
  Competition.findById(competitionId, function (err, competition) {
    if(err) { return callback(err, false); }
    if(!competition) { return callback(404, false); }
    //Remove Runs
    Run.find({competition: competitionId}, '_id', function(err, runsId){
      runsId.forEach(function(runId){
        RunCtrl.delete(runId, function(err, result){
           if(err){
             return callback(err, false);
           }
        });
      });
      competition.remove(function(err) {
        if(err) { return callback(err, false); }
        return callback(null, true);
      });
    });
  });
}

//Competitors
exports.allCompetitors = function(req, res){
  var compId = req.params.id;
  Competition.findById(compId)
    .populate('type')
    .exec(function(err, competition) {
    if (err) {
      return handleError(res, err);
    }
    if (!competition) {
      return res.send(404);
    }
    if (competition.type.name === 'Solo') {
      getPilotsCompetitors(function(err, competitors){
        if(err){
          return handleError(res, err);
        }
        return res.json(200, competitors);
      });
    } else {
      getTeamsCompetitors(function (err, competitors) {
        if(err){
          return handleError(res, err);
        }
        return res.json(200, competitors);
      });
    }
  });
};
function getPilotsCompetitors(callback) {
  Pilot.find({}, function (err, pilots) {
    if (err) {
      return callback(err, null);
    }
    var competitors = [];
    pilots.forEach(function (pilot) {
      competitors.push({
          _id: pilot._id,
          name: pilot.name,
          civlRank: pilot.civlRank,
          apwcRank: pilot.apwcRank
      });
    });
    callback(null, competitors);
  });
}
function getTeamsCompetitors(callback){
  Team.find()
    .populate('pilots','-picture')
    .exec(function (err, teams) {
    if (err) {
      return callback(err, null);
    }
    var competitors = [];
    teams.forEach(function(team){
      var ranks = computeTeamRanks(team);
      competitors.push({
        _id: team._id,
        name: team.name,
        civlRank: ranks.civlRank,
        apwcRank: ranks.apwcRank
      });
    });
    callback(null, competitors);
  });
}
function computeTeamRanks(team) {
  var ranks = { civlRank: 0, apwcRank: 0};
  team.pilots.forEach(function(pilot){
    ranks.civlRank += pilot.civlSyncRank;
    ranks.apwcRank += pilot.apwcSyncRank;
  });
  return ranks;
}


/* Prepare to start */
exports.prepareCompetitions = function(eventId, callback){
  var step = require('step');
  step(
    function(){
      Competition.find({event: eventId},this);
    },
    function(err, competitions){
      if(err){
        return callback(err, false);
      }
      var group = this.group();
      competitions.forEach(function(competition){
        prepareCompetition(competition, group());
      });
    },
    function(err, results){
      if(err){
        return callback(err, false);
      }
      return callback(null, true);
    }
  );
};
function prepareCompetition(competitionId, callback){
  Run.findOne({competition: competitionId, order: 1}, function(err, firstRun){
    if (err){
      return callback(err, false);
    }
    if (!firstRun){
      return callback('Error preparing competition, first run is not available', false);
    }
    RunCtrl.prepareRun(firstRun._id, true, callback);
  });
}

exports.reset = function(req, res){
  _this.resetCompetition(req.params.id, function(err, competition){
    if (err) { return handleError(res, err); }
    if(!competition) { return res.send(404); }
    return res.json(200, competition);
  });
};
exports.resetCompetition = function(competitionId, callback){
  Competition.findById(competitionId, function (err, competition) {
    if (err) { return handleError(res, err); }
    if(!competition) { return res.send(404); }

    RunCtrl.resetRuns(competitionId, function(err){
      if(err){
        return callback (err, null);
      }
      competition.startTime = null;
      competition.endTime = null;
      competition.save(function (err) {
        if (err) { return handleError(res, err); }
        // CompetitionCtrl.runResetNotification(run._id);
        return callback(null, competition);
      });
    });

  });
};

exports.resetCompetitions = function(eventId, callback){
  Competition.find({event: eventId}, function(err, competitions){
    if (err){
      return callback(err, null);
    }

    if(!competitions || competitions.length === 0){
      return callback(null, true);
    }

    var step = require('step');
    step(
      function (){
        // Create a new group
        var group = this.group();
        competitions.forEach(function (competition) {
          _this.resetCompetition(competition._id, group());
        });
      },
      function showAll(err , results) {
        if (err) { throw err; }
        callback(null, true);
      }
    );
  });
};

//Start a competition
exports.start = function(req, res){
  Competition.findById(req.params.id, function (err, competition) {
    if (err) { return handleError(res, err); }
    if(!competition) { return res.send(404); }
    if(!competition.competitors || competition.competitors.length === 0){
      return res.send('Unable to start the competition without competitors');
    }
    competition.startTime = new Date();
    competition.endTime = null;
    competition.save(function (err) {
      if (err) { return handleError(res, err); }
      if(req.body.display){
        Displayer.startCompetition(competition._id, function(err, result){
          if (err){
            console.log(err)
          }
        });
      }

      Run.findOne({competition: competition._id, order: 1}, function(err, firstRun){
        if (err){
          return res.send('Error: Unable to find the competition first run');
        }
        if (!firstRun){
          return res.send('Error: Unable to find the competition first run');
        }
        RunCtrl.prepareRun(firstRun._id, false, function(err, result){
          if (err){
            return res.send('Error: Unable to prepare the competition first run');
          }
          return res.json(200, competition);
        });
      });
    });
  });
};
//End a started competition
exports.end = function(req, res){
  Competition.findById(req.params.id, function (err, competition) {
    if (err) { return handleError(res, err); }
    if(!competition) { return res.send(404); }
    if(!competition.startTime) { return res.send(404); }
    competition.endTime = new Date();
    competition.save(function (err) {
      if (err) { return handleError(res, err); }
      if(req.body.display) {
        Displayer.showCompetitionResults(competition._id, function (err, result) {
          if (err) {
            console.log(err)
          }
        });
      }
      return res.json(200, competition);
    });
  });
};
exports.reopen = function(req, res){
  Competition.findById(req.params.id, function (err, competition) {
    if (err) { return handleError(res, err); }
    if(!competition) { return res.send(404); }
    if(!competition.endTime) { return res.send(404); }
    competition.endTime = null;
    competition.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.json(200, competition);
    });
  });
};


exports.displayResults = function(req, res){
  Competition.findById(req.params.id, function (err, competition) {
    if (err) { return handleError(res, err); }
    if(!competition) { return res.send(404); }
    Displayer.showCompetitionResults(competition._id, function(err, result){
      if (err){
        return handleError(res, err);
      }
      return res.json(200, result);
    });
  });
};

function getCompetitionRuns(competitionId, callback){
  Run
    .find({competition: competitionId})
    .populate('type', 'name')
    .sort('order')
    .exec(function(err, runs){
      if(err) { return callback(err, null); }
      callback(null, runs);
    });
}

function getCompetitionEndedRuns(competitionId, callback){
  Run
    .find({competition: competitionId, endTime: {$ne: null}})
    .populate('type', 'name')
    .sort('order')
    .exec(function(err, runs){
      if(err) { return callback(err, null); }
      callback(null, runs);
    });
}

function computeRunCompensation(runId, callback){
  Flight.find({run: runId}, function(err, flights){
    if (err){
     callback(err, null);
    }
    if(flights && flights.length > 0){
      var minFlightResult = flights[0].computeResult;
      flights.forEach(function(flight){
        if(flight.computeResult < minFlightResult){
          minFlightResult = flight.computeResult;
        }
      });
      return callback(null, minFlightResult);
    }
    return callback(null, 0);
  });
}

function computeCompetitorCompensation(runId, competitorId, callback){
//  return callback(null, 100);
  Run.findById(runId, function(err, run){
    if (err){
      return callback(err, null);
    }
    if(!run){
      return callback('run not found', null);
    }

    Run.find({competition: run.competition, order: {$lt: run.order}}, function(err, prevRuns){
      var step = require('step');
      step(
        function(){
          var group = this.group();
          prevRuns.forEach(function(prevRun){
            Flight.findOne({run: prevRun._id, 'competitor._id': competitorId}, group());
          });
        },
        function(err, flights){
          if(flights && flights.length > 0){
            var flightsComputeResults = 0;
            var count = 0;
            flights.forEach(function(flight){
              if(flight && flight.computeResult >= 0){
                flightsComputeResults += flight.computeResult;
                count++;
              }
            });
            return callback(null, (flightsComputeResults / count));
          }
          return callback(null, 0);
        }
      );
    });
  });
}
function getFlightResultWithCompensation(competitorId, runId, callback){
  Flight.findOne({'competitor._id': competitorId, run: runId})
    .exec(function(err, flight){
      if (err){
        return callback(err, null);
      }
      if(flight){
        return callback(null, {computeResult : flight.computeResult, warnings: flight.warnings});
      }
      else {
        //Flight not executed (//TODO: Check no ended flight!!)
        var step = require('step');
        step(
          function(){
            computeRunCompensation(runId, this.parallel());
            computeCompetitorCompensation(runId, competitorId, this.parallel());
          },
          function(err, runCompensation, competitorCompensation){
            if (runCompensation < competitorCompensation){
              return callback(null, {computeResult: runCompensation, warnings: 0});
            } else {
              return callback(null, {computeResult: competitorCompensation, warnings: 0});
            }
          }
        );
      }
    });
}
function getFlightResult(competitorId, runId, callback){
  Flight.findOne({'competitor._id': competitorId, run: runId})
    .populate('run', 'name order')
    .exec(function(err, flightResult){
      if (err){
        return callback(err, null);
      }
      return callback(null, flightResult);
    });
}
function getCompetitorResults(competitor, runs, callback){
  var results = {
    competitor: {
      _id: competitor._id,
      name: competitor.name
    },
    runResults: [],
    warnings: 0,
    overall: 0
  };
  var step = require('step');
  step(
    function(){
      var group = this.group();
      runs.forEach(function(run){
        getFlightResult(competitor._id, run._id, group());
      });
    }, function(err, flights){
      if(err){
        return callback(err, null);
      }
      flights.forEach(function(flight){
        if(flight && (flight.computeResult||flight.computeResult === 0)){
          results.runResults.push(
            {
              name: flight.run.name,
              order: flight.run.order,
              points: flight.computeResult
            }
          );
          results.warnings+= flight.warnings;

          results.overall += flight.computeResult;
        }
      });
      //TODO: FIX Get warningsToDSQ Season settings
      if (results.warnings >= 3){
        results.overall = -1;
      }

      return callback(null, results);
    }
  );
}
function getCompetitorResultsWithCompensation(competitor, runs, callback){
  var results = {
    competitor: {
      _id: competitor._id,
      name: competitor.name
    },
    runResults: [],
    warnings: 0,
    overall: 0
  };
  var step = require('step');
  step(
    function(){
      var group = this.group();
      runs.forEach(function(run){
        results.runResults.push({name: run.name, order: run.order, points: 0});
        getFlightResultWithCompensation(competitor._id, run._id, group());
      });
    }, function(err, flights){
      if(err){
        return callback(err, null);
      }
      for(var i = 0; i <  results.runResults.length; i++){
        if(flights[i]){
          results.runResults[i].points =  flights[i].computeResult;
          results.warnings += flights[i].warnings;
          results.overall += flights[i].computeResult;
        }
      }
      //TODO: FIX Get warningsToDSQ Season settings
      if (results.warnings >= 3){
         results.overall = -1;
      }

      return callback(null, results);
    }
  );
}

exports.results = function(req, res){
  _this.getResults(req.params.id, function(err, results){
    if(err){ return handleError(res, err);}
    return res.json(200, results);
  })
};
exports.resultsFAI = function(req, res){
  _this.getResultsWithCompensation(req.params.id, function(err, results){
    if(err){ return handleError(res, err);}
    return res.json(200, results);
  })
};

exports.getResults = function(competitionId, callback){
  var step = require('step');
  step(
    function(){
      getCompetitionRuns(competitionId, this.parallel());
      Competition.findById(competitionId, this.parallel());
    },
    function(err, runs, competition){
      var group = this.group();
      competition.competitors.forEach(function(competitor) {
        getCompetitorResults(competitor, runs, group());
      });
    },
    function(err, competitionResults){
      if (err){
        return callback(err, null);
      }
      var results =  [];
      competitionResults.forEach(function(result){
        if(result.runResults && result.runResults.length > 0){
          results.push(result);
        }
      });

      results = _.sortBy(results, 'overall');
      return callback(null, results.reverse());
    }
  );
};
exports.getResultsWithCompensation = function(competitionId, callback){
  var step = require('step');
  step(
    function(){
      getCompetitionEndedRuns(competitionId, this.parallel());
      Competition.findById(competitionId, this.parallel());
    },
    function(err, runs, competition){
      var group = this.group();
      competition.competitors.forEach(function(competitor) {
        getCompetitorResultsWithCompensation(competitor, runs, group());
      });
    },
    function(err, competitionResults){
      if (err){
        return callback(err, null);
      }
      var results =  [];
      competitionResults.forEach(function(result){
        if(result.runResults && result.runResults.length > 0){
          results.push(result);
        }
      });

      results = _.sortBy(results, 'overall');
      return callback(null, results.reverse());
    }
  );
};

exports.generateResultsReportPDF = function(req, res){
  _this.generateCompetitionResultsReportPDF(req.params.id, function(err, result){
    if(err){ return handleError(res, err);}
    return res.json(200, result);
  });
};

exports.generateCompetitionResultsReportPDF = function(competitionId, callback){
  Competition.findById(competitionId)
    .populate('type')
    .exec(function(err, competition){
      if (err){ return callback(err, false);}
      if (!competition){ return callback(404, false);}
      _this.getResults(competitionId, function(err, results) {
        competition.results = results;
        if(competition.type.name === 'Solo') {
          ReportCtrl.generateCompetitionSoloResultsPDF(competition, function(err, result){
            if (err){ return callback(err, null);}
            return callback(null, result);
          });
        }
        else {
          ReportCtrl.generateCompetitionSyncResultsPDF(competition, function (err, result) {
            if (err) { return callback(err, null);}
            return callback(null, result);
          });
        }
      });
    });
};

exports.generateResultsReportFaiCsv = function(req, res){
  _this.generateCompetitionResultsReportFaiCSV(req.params.id, function(err, result){
    if(err){ return handleError(res, err);}
    return res.json(200, result);
  });
};

exports.generateCompetitionResultsReportFaiCSV = function(competitionId, callback){
  Competition.findById(competitionId)
    .populate('type')
    .exec(function(err, competition){
      if (err){ return callback(err, false);}
      if (!competition){ return callback(404, false);}
      _this.getResultsWithCompensation(competitionId, function(err, results) {
        competition.results = results;
        if(competition.type.name === 'Solo') {
          ReportCtrl.generateCompetitionSoloResultsFaiCSV(competition, function(err, result){
            if (err){ return callback(err, null);}
            return callback(null, result);
          });
        }
        else {
          ReportCtrl.generateCompetitionSyncResultsFaiCSV(competition, function (err, result) {
            if (err) {
              return callback(err, null);
            }
            return callback(null, result);
          });
        }
      });
    });
};


function handleError(res, err) {
  return res.send(500, err);
}
