'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var CompetitorSchema = new Schema({
  //pilot: {type: mongoose.Schema.Types.ObjectId, ref: 'Pilot'},
  //team: {type: mongoose.Schema.Types.ObjectId, ref: 'Team'},
  //Competitor
  _id: {type: mongoose.Schema.Types.ObjectId},
  name: String,
  //CIVL ranking
  civlRank: Number,
  //APWC ranking
  apwcRank: Number
});

var CompetitionSchema = new Schema({
  name: {type: String, unique: true},
  code: {type: String, unique: true},
  type: {type: mongoose.Schema.Types.ObjectId, ref: 'CompetitionType'},
  startTime: Date,
  endTime: Date,
  event: {type: mongoose.Schema.Types.ObjectId, ref: 'Event'},
  competitors:[CompetitorSchema],
  isValidAPWT: { type: Boolean, default: false}
});

module.exports = mongoose.model('Competition', CompetitionSchema);
