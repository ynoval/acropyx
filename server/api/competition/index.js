'use strict';

var express = require('express');
var controller = require('./competition.controller');

var router = express.Router();

router.get('/', controller.index);
router.get('/:id/markTypes', controller.markTypes);
router.get('/:id/runs', controller.runs);
router.get('/:id/allCompetitors', controller.allCompetitors);
router.get('/:id/results', controller.results);
router.get('/:id/resultsFAI', controller.resultsFAI);
router.get('/:id', controller.show);
router.post('/', controller.create);
router.put('/:id/generateResultsReportPDF', controller.generateResultsReportPDF);
router.put('/:id/generateResultsReportCSV', controller.generateResultsReportFaiCsv);
router.put('/:id/displayResults', controller.displayResults);
router.put('/:id/reset', controller.reset);
router.put('/:id/start', controller.start);
router.put('/:id/end', controller.end);
router.put('/:id/reopen', controller.reopen);
router.put('/:id', controller.update);
router.patch('/:id', controller.update);
router.delete('/:id', controller.destroy);

module.exports = router;
