'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var PilotSchema = new Schema({
  //Pilot name
  name: String,
  //Pilot picture
  picture: {data: Buffer, contentType: String},
  // Pilot Country
  country:{type: mongoose.Schema.Types.ObjectId, ref: 'Country'},
  // Female or Male
  sex: {type: String, default: 'Male'},
  // pilot date of the birth (obtain age)
  dateOfBirth: Date,
  //CIVL Identifier
  civlId: Number,
  //CIVL ranking (updated from FAI web service)
  civlRank: {type: Number, default: 1000},
  //CIVL Synchro ranking (updated from FAI web service)
  civlSyncRank: {type: Number, default: 1000},
  //APWC ranking (updated automatically)
  apwcRank: {type: Number, default: 1000},
  //APWC Synchro ranking (updated automatically)
  apwcSyncRank: {type: Number, default: 1000},
  //first flight year
  flyingSinceYear: Date,
  // Glide model
  glide: String,
  // has the FAI licence
  hasFaiLicence: {type: Boolean, default: true},
  //best results obtained by pilot
  bestResults: String,
  // Pilot Job
  job: String,
  // Pilot sponsors
  sponsors: [{type: mongoose.Schema.Types.ObjectId, ref: 'Sponsor'} ]
});

module.exports = mongoose.model('Pilot', PilotSchema);
