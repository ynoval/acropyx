/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var Pilot = require('./pilot.model');

exports.register = function(socket) {
  Pilot.schema.post('save', function (doc) {
    onSave(socket, doc);
  });
  Pilot.schema.post('remove', function (doc) {
    onRemove(socket, doc);
  });
};

function onSave(socket, doc, cb) {
  socket.emit('Pilot:save', doc);
}

function onRemove(socket, doc, cb) {
  socket.emit('Pilot:remove', doc);
}
