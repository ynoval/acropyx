'use strict';

var express = require('express');
var multiparty = require('connect-multiparty');
var multipartyMiddleware = multiparty();
var controller = require('./pilot.controller');

var router = express.Router();

router.get('/', controller.index);
router.get('/fai', controller.testFai);
router.get('/:id/picture', controller.getPicture);
router.get('/:id/picture/:cache', controller.getPicture);
router.get('/:id', controller.show);
router.post('/', controller.create);
router.put('/:id/updatePicture', multipartyMiddleware, controller.updatePicture);
router.put('/:id', controller.update);
router.patch('/:id', controller.update);
router.delete('/:id', controller.destroy);




module.exports = router;
