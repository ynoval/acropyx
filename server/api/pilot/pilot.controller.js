/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /pilots              ->  index
 * POST    /pilots              ->  create
 * GET     /pilots/:id          ->  show
 * PUT     /pilots/:id          ->  update
 * DELETE  /pilots/:id          ->  destroy
 */

'use strict';

var _ = require('lodash');
var Pilot = require('./pilot.model');

// Get list of pilots
exports.index = function(req, res) {
  Pilot.find({},{picture:0})
    .sort('name')
    .exec(function (err, pilots) {
    if(err) { return handleError(res, err); }
    return res.json(200, pilots);
  });
};

// Get a single pilot
exports.show = function(req, res) {
  Pilot
  .findById(req.params.id)
  .populate('sponsors', 'name')
  .exec(function (err, pilot) {
    if(err) { return handleError(res, err); }
    if(!pilot) { return res.send(404);}
    return res.json(pilot);
  });
};

// Creates a new pilot in the DB.
exports.create = function(req, res) {
  Pilot.create(req.body, function(err, pilot) {
    if(err) { return handleError(res, err); }
    return res.json(201, pilot);
  });
};

// Updates an existing pilot in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  Pilot.findById(req.params.id, function (err, pilot) {

    updateFromFai(pilot.civlId);

    if (err) { return handleError(res, err); }
    if(!pilot) { return res.send(404); }

    //TODO: FIX saving sponsors
    req.body.sponsors = req.body.sponsors.map(function(option) { return option._id; });
    pilot.sponsors = req.body.sponsors;
    req.body.sponsors = [];

    if(req.body.country && req.body.country._id){
      req.body.country = req.body.country._id;
    }



    var updated = _.merge(pilot, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.json(200, pilot);
    });
  });
};

// Delete a pilot from the DB.
exports.destroy = function(req, res) {
  Pilot.findById(req.params.id, function (err, pilot) {
    if(err) { return handleError(res, err); }
    if(!pilot) { return res.send(404); }
    pilot.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.send(204);
    });
  });
};

function updateFromFai(civlId){
  var soap = require('soap');

  var url = 'http://civlrankings.fai.org/FL.asmx?WSDL';

  soap.createClient(url, function(err, soapClient) {

    var args = {
     CIVLID: 16658,
     ladder_id: 6
    };

    soapClient.GetPilotAndLastRankingInLadder(args,
      function(err, result) {
        if (err) {
          console.log(err);
        } else {
         console.log(JSON.stringify(result));
          //var rank = result.GetPilotAndLastRankingInLadderResult.Rankings.Ranking[0].Rank;
          //console.log('Rank: ' + rank);

        }
      });
  });
}


exports.updatePicture = function(req, res){
  Pilot.findById(req.params.id, function(err, pilot){
    if (err) { return handleError(res, err); }
    if(!pilot) { return res.send(404); }
    var fs = require('node-fs');
    var file = req.files.file;

    fs.readFile(req.files.file.path, function (dataErr, data) {
      if(data) {
        pilot.picture = {};
        pilot.picture.data = data;
        pilot.picture.contentType = 'image/jpg';
        pilot.save(function (err, savedPilot) {
          if (err) {
            console.log('error updating the pilot image: ' + err);
          }
          return res.json(200, savedPilot);
        });
      }
    });
  });
};

exports.getPicture = function(req, res){
  Pilot.findById(req.params.id)
    .exec(function (err, pilot) {
      if(err) { return handleError(res, err); }
      if(!pilot) { return res.send(404); }
      if(pilot.picture && pilot.picture.contentType) {
        res.contentType(pilot.picture.contentType);
        return res.send(pilot.picture.data);
      }else{
        var fs = require('node-fs');
        var defaultNoImage = fs.readFileSync(__dirname +  '/../../public/pilot-no-image.jpg');
        //var defaultNoImage = fs.readFileSync('/tmp/Acro2015/sponsor-no-image.jpg');
        res.contentType('image/jpg');
        res.send(defaultNoImage);
      }
    });
};


exports.testFai = function(req, res){
  updateFromFai();
  return res.json(200, {result: 'OK'});
};

function handleError(res, err) {
  return res.send(500, err);
}
