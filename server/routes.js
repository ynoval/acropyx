/**
 * Main application routes
 */

'use strict';

var errors = require('./components/errors');

module.exports = function(app) {

  // Insert routes below
  app.use('/api/users', require('./api/user'));

  app.use('/api/reports', require('./api/report'));

  app.use('/api/seasons', require('./api/season'));
  app.use('/api/events', require('./api/event'));
  app.use('/api/competitions', require('./api/competition'));
  app.use('/api/competitionTypes', require('./api/competitionType'));
  app.use('/api/runs', require('./api/run'));
  app.use('/api/runTypes', require('./api/runType'));

  app.use('/api/pilots', require('./api/pilot'));
  app.use('/api/teams', require('./api/team'));
  app.use('/api/battles', require('./api/battle'));
  app.use('/api/flights', require('./api/flight'));

  app.use('/api/sponsors', require('./api/sponsor'));
  app.use('/api/judges', require('./api/judge'));
  app.use('/api/judgeTypes', require('./api/judgeType'));
  app.use('/api/markTypes', require('./api/markType'));

  app.use('/api/manoeuvres', require('./api/manoeuvre'));
  app.use('/api/settings', require('./api/setting'));

  app.use('/api/countries', require('./api/country'));





  // DUPLICATE and replace entity  by the entity name (ATTENTION: the url is plural)
  app.use('/api/entities', require('./api/entity'));



  app.use('/auth', require('./auth'));





  // All undefined asset or api routes should return a 404
  app.route('/:url(api|auth|components|app|bower_components|assets)/*')
   .get(errors[404]);

  // All other routes should redirect to the index.html
  app.route('/*')
    .get(function(req, res) {
      res.sendfile(app.get('appPath') + '/index.html');
    });
};
