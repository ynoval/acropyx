/**
 * Populate DB with sample data on server start
 * to disable, edit config/environment/index.js, and set `seedDB: false`
 */

'use strict';
var _ = require('lodash');

var countriesUrl = 'https://restcountries.eu/rest/v1/all';

//Entities
var User = require('../api/user/user.model');
var JudgeType = require('../api/judgeType/judgeType.model');
var Judge = require('../api/judge/judge.model');
var RunType = require('../api/runType/runType.model');
var CompetitionType = require('../api/competitionType/competitionType.model');
var Manoeuvre = require('../api/manoeuvre/manoeuvre.model');
var MarkType = require('../api/markType/markType.model');
var Report = require('../api/report/report.model');
var Setting = require('../api/setting/setting.model');
var Country = require('../api/country/country.model');
var Sponsor = require('../api/sponsor/sponsor.model');
var Pilot = require('../api/pilot/pilot.model');
var Team = require('../api/team/team.model');


var withoutPenaltyManoeuvres = [];

function importSponsors(callback){
  Sponsor.remove({}, function( err){
    var fs = require('fs');
    var csv = require('csv');

    var parser = csv.parse({delimiter: ','}, function(err, data){
      var Step = require('step');
      Step(
        function(){
          var group = this.group();
          _.each(data, function(sponsor){
            Sponsor.create({name: sponsor[0], website:sponsor[1]}, group());
          });
        },
        function(err, sponsors){
          console.log('finished importing sponsors');
          return callback(null, true);
        }
      );
    });
    fs.createReadStream(__dirname+'/csv/sponsors.csv').pipe(parser);
  });
}

function getBirthDate(pilotInfo){
   var date = pilotInfo[6];
   if(!date){
     console.log('ERROR: Invalid BirthDate --> ' + pilotInfo[1]);
     return null;
   }
   //remove time
   var dateInfo = date.split('/');
   var year = dateInfo[2].split(' ')[0];
   var month = dateInfo[1] - 1;
   var day = dateInfo[0];
   return new Date(year,month, day);
 }

function getPilotSponsors(sponsorsInfo, callback){
  if(sponsorsInfo.length === 0 ){
    return callback(null, []);
  }

  var step = require('step');
  step(
    function(){
      var sponsorList = sponsorsInfo.split('/');
      var group = this.group();
      _.each(sponsorList, function(sponsorName){
        if(sponsorName.length > 0) {
          Sponsor.findOne({name: sponsorName.trim()}, group());
        }
      });
    },
    function(err, sponsors){
      if(err){
        console.log('ERROR get pilot sponsors: ' + sponsorsInfo);
        return callback(err);
      }
      var result = [];
      if(sponsors.length > 0){
        _.each(sponsors, function(sponsor){
          if(sponsor){
            result.push(sponsor._id);
          }
        });
      }
      return callback(null, result);
    }
  );

}

function importPilot(pilotInfo, callback){
  var dateOfBirth = getBirthDate(pilotInfo);
  var pilot = {
    name : pilotInfo[1],
    sex: pilotInfo[11],
    civlId: pilotInfo[2],
    civlRank: (pilotInfo[3] > 0)?pilotInfo[3]:1000,
    civlSyncRank: (pilotInfo[4] > 0)?pilotInfo[4]:1000,
    dateOfBirth:dateOfBirth,
    flyingSinceYear: new Date(pilotInfo[7],1,1),
    glide: pilotInfo[8],
    hasFaiLicence: pilotInfo[10],
    job: pilotInfo[9],
    bestResults: pilotInfo[0],
    sponsors:[]
  };

  var step = require('step');
  step(
    function(){
      Country.findOne({ioc: pilotInfo[5].toUpperCase()},this);
    }, function(err, country) {
      if(err){
        console.log('ERROR Invalid pilot country:' +  pilotInfo[5].toUpperCase());
      }
      if(country) {
        pilot.country = country._id;
      } else {
        console.log('ERROR Invalid pilot country  (pilot -- country): ' + pilotInfo[1] + ' -- ' + pilotInfo[5].toUpperCase());
      }

      getPilotSponsors(pilotInfo[12], this);
    }, function(err, sponsors){
      if(err){
        console.log('ERROR invalid pilot sponsors --> ' + pilotInfo[1]);
        return callback(err, null);
      }
      pilot.sponsors = sponsors;
      Pilot.create(pilot, function(err, newPilot){
        if (err){
          return callback(err, null);
        }
        callback(null, true);
      })
    }
  );
}
function importPilots(callback){
  //0 - best results
  //1- name
  //2- civlId
  //3: civlRank
  //4: civlSyncRank
  //5- country code
  //6- date of the birth
  //7- flight since
  //8- glider
  //9: job
  //10: faiLicence
  //11: sex
  //12: sponsors
  Pilot.remove({}, function( err){
    var fs = require('fs');
    var csv = require('csv');

    var parser = csv.parse({delimiter: ','}, function(err, data) {
      var step = require('step');
      step(
        function () {
          var group = this.group();
          data.forEach(function (pilot) {
            importPilot(pilot, group());
          });
        },
        function(err, pilots){
          if (err) {
            return callback(err, false);
          }
          console.log('finished importing pilots');
          return callback(null, true);
        }
      );
    });

    fs.createReadStream(__dirname+'/csv/pilots.csv').pipe(parser);
  });
}

function importTeam(teamInfo, callback){
  var team = {
    name: teamInfo[1],
    bestResults: teamInfo[0],
    pilots: []
  };
  var pilot1Name =  teamInfo[2];
  Pilot.findOne({name: pilot1Name} , function(err, pilot1){
    if (err){
      console.log('ERROR importing team (' + teamInfo[1] + '): ' + err);
      return callback(err, null);
    }
    if (!pilot1){
      console.log('ERROR team:'+ teamInfo[1] + ', pilot ' + teamInfo[2] + ' not found');
      return callback('pilot ' + teamInfo[2] + ' not found', null);
    }
    team.pilots.push(pilot1._id);
    var pilot2Name = teamInfo[3];
    Pilot.findOne({name: pilot2Name}, function(err, pilot2){
      if (err){
        console.log('ERROR importing team (' + teamInfo[1] + '): ' + err);
        return callback(err, null);
      }
      if (!pilot2){
        console.log('ERROR team:'+ teamInfo[1] + ', pilot2 ' + teamInfo[3] + ' not found');
        return callback('pilot2 ' + teamInfo[3] + ' not found', null);
      }
      team.pilots.push(pilot2._id);
      Team.create(team, function(err, newTeam){
        if (err){
          console.log('ERROR importing team (' + teamInfo[1] + '): ' + err);
          return callback('ERROR importing team: '+ teamInfo[1], null);
        }
        return callback(null, newTeam);
      });
    });
  });
}
function importTeams(callback){
  //0 - best results
  //1- name
  //2- pilot1
  //3: pilot2
  Team.remove({}, function( err){
    var fs = require('fs');
    var csv = require('csv');

    var parser = csv.parse({delimiter: ','}, function(err, data){
      _.each(data, function(team){
        importTeam(team, function(err, newTeam){
          if(err){
            console.log('ERROR importing team --> ' + team[1]);
          }
        });
      });
      console.log('finished importing teams');
      return callback(null, true);

    });
    fs.createReadStream(__dirname+'/csv/teams.csv').pipe(parser);
  });
}

function importCountries(callback){
  var countryList  = require('country-data').countries;
  var countries = [];
  var lastCountry = 'TEST_COUNTRY';
  _.each(countryList,function(country) {

    if(country.name && country.ioc && country.name !== lastCountry){
      lastCountry = country.name;
      countries.push({name: country.name, ioc: country.ioc});
    }

  });
  Country.create(countries, function(err, countries) {
    if(err){
      console.log('ERROR importing countries: ', err);
      return callback(err);
    }
      console.log('finished importing countries');
      callback(null, true);
    }
  );


  //Country.find({}).remove(function() {
  //  var countryRest = require('restler');
  //  countryRest.get(countriesUrl).on('complete', function (result) {
  //    if (result instanceof Error) {
  //      callback('Error importing countries', null);
  //      return;
  //    }
  //     var countries = [];
  //    result.forEach(function(country){
  //      countries.push({name: country.name, col: country.alpha3Code});
  //    });
  //    Country.create(countries, function(err, countries) {
  //        console.log('finished importing countries');
  //        callback(null, true);
  //      }
  //    );
  //  });
  //});

}

function createUsers(callback){
  User.find({}).remove(function() {
    User.create({
        provider: 'local',
        role: 'admin',
        name: 'Admin',
        email: 'admin@acropyx.com',
        password: '123'
      },
      {
        provider: 'local',
        role: 'manager',
        name: 'Manager',
        email: 'manager@acropyx.com',
        password: '123'
      },
      function(err, users) {
        if(err){
          console.log('ERROR populating users: ' + err);
          return callback(err);
        }
        console.log('finished populating users');
        callback(null, true);
      }
    );
  });
}
function createJudgeTypes(callback){

  JudgeType.find({}).remove(function() {
    JudgeType.create({
        name: 'FAI'
      },{
        name: 'VIP'
      }, function(err, judgeTypes) {
      if(err){
        console.log('ERROR importing judgesTypes: ' + err);
        return callback(err);
      }
        console.log('finished populating judgeTypes');
        callback(null, true);
      }
    );
  });
}
function importJudges(callback){
  Judge.remove({}, function( err){
    var fs = require('fs');
    var csv = require('csv');

    var parser = csv.parse({delimiter: ','}, function(err, data){
      var Step = require('step');
      Step(
        function(){
          var group = this.group();
          _.each(data, function(judge){
            JudgeType.findOne({name: judge[1]},function(err,judgeType) {
              Judge.create({name: judge[0], type: judgeType._id}, group());
            });
          });
        },
        function(err, judges){
          if(err){
            console.log('ERROR importing judges: ' + err);
            return callback(err);
          }
          console.log('finished importing judges');
          return callback(null, true);
        }
      );
    });
    fs.createReadStream(__dirname+'/csv/judges.csv').pipe(parser);
  });
}
function createRunTypes(callback){
  RunType.find({}).remove(function() {
    RunType.create({
        name: 'Standard'
      },{
        name: 'Battle'
      }, function(err, judgeTypes) {
        if(err){
          console.log('ERROR importing runTypes: ' + err);
          return callback(err);
        }
        console.log('finished populating runTypes');
        callback(null, true);
      }
    );
  });
}
function createManoeuvres(callback){
  Manoeuvre.remove({}, function( err){
    var fs = require('fs');
    var csv = require('csv');

    var parser = csv.parse({delimiter: ','}, function(err, data){
      _.each(data, function(mano){
        if(mano[7] === "1"){
          withoutPenaltyManoeuvres.push(mano[0]);
        }
        Manoeuvre.create(
          {
            name: mano[0],
            coefficient: mano[1],
            hasSides: (mano[2] === "1")?true:false,
            onlySynchro: (mano[3]==="1")?true:false,
            twistedBonus: mano[4],
            reversedBonus: mano[5],
            flippedBonus: mano[6]
          }
        );
      });
      callback(null, true);
      console.log('finished populating manoeuvres');
    });
    fs.createReadStream(__dirname+'/csv/manoeuvres.csv').pipe(parser);
  });
}
function createMarkTypes(callback){
  //Technical expression, Choreography, Landing, Synchronization
  MarkType.remove({}, function(err){
    MarkType.create({
      name: 'Technical expression'
    },{
      name: 'Choreography'
    },{
      name: 'Landing'
    },{
      name: 'Synchronization'
    }, function(err, results){
      console.log('finished populating markTypes');
      callback(null, true);
    });
  });
}
function createCompetitionTypes(callback){
  var soloMarks = [];
  var syncMarks = [];
  MarkType.find({}, function(err, marks){
    _.each(marks, function(mark){
      if (mark.name !== 'Synchronization'){
        soloMarks.push(mark._id);
      }
      syncMarks.push(mark._id);
    });
    CompetitionType.remove({}, function(err){
      CompetitionType.create({
        name: 'Solo',
        markTypes: soloMarks
      },{
        name: 'Synchro',
        markTypes: syncMarks
      }, function(err, competitionTypes){
        console.log('finished populating competitionTypes');
        callback(null, true);
      });
    });
  });
}
function createReports(callback){
  Report.find({}).remove(function(){
    Report.create(
      {
        name: 'competitionResultsSolo',
        title: 'Competition Results',
        category: "COMPETITION",
        type: 'PDF'
      },
      {
        name: 'competitionResultsSync',
        title: 'Competition Results',
        category: "COMPETITION",
        type: 'PDF'
      },
      {
        name: 'competitionRuns',
        title: 'Runs',
        category: "COMPETITION",
        type: 'PDF'
      },
      {
        name: 'runResults',
        title: 'Run Results',
        category: "RUN",
        type: 'PDF'
      },
      {
        name: 'runResultsBattle',
        title: 'Run Battle Results',
        category: "RUN",
        type: 'PDF'
      },
      {
        name: 'runStartingOrder',
        title: 'Run Starting Order',
        category: "RUN",
        type: 'PDF'
      },
      {
        name: 'runStartingOrderBattle',
        title: 'Run Starting Order Battle',
        category: "RUN",
        type: 'PDF'
      },
      {
        name: 'runManoeuvres',
        title: 'Run Manoeuvres',
        category: "RUN",
        type: 'PDF'
      },

      function(){
        console.log('finished creating reports information');
        callback(null, true);
      }
    );
  });
}

function createSoloMarkCoefficients(defaultSettings, callback){
  CompetitionType.findOne({name: 'Solo'}).populate('markTypes').exec(function(err, soloCompetitionType) {
    var competitionMarkCoefficient = { competitionType: soloCompetitionType._id, markCoefficients:[]};
    _.each(soloCompetitionType.markTypes, function(markType) {
      var markCoefficient = { markType: markType._id};
      switch (markType.name) {
        case 'Technical expression':
        {
          markCoefficient.coefficient = 40;
          break;
        }
        case 'Choreography':
        {
          markCoefficient.coefficient = 40;
          break;
        }
        case 'Landing':
        {
          markCoefficient.coefficient = 20;
          break;
        }
      }
      competitionMarkCoefficient.markCoefficients.push(markCoefficient);
    });
    defaultSettings.competitionMarkCoefficients.push(competitionMarkCoefficient);
    callback(null, defaultSettings);
  });
}
function createSyncMarkCoefficients(defaultSettings, callback){
  CompetitionType.findOne({name: 'Synchro'}).populate('markTypes').exec(function(err, syncCompetitionType) {
    var competitionMarkCoefficient = { competitionType: syncCompetitionType._id, markCoefficients:[]};
    _.each(syncCompetitionType.markTypes, function(markType) {
      var markCoefficient = { markType: markType._id};
      switch (markType.name) {
        case 'Technical expression':
        {
          markCoefficient.coefficient = 30;
          break;
        }
        case 'Choreography':
        {
          markCoefficient.coefficient = 30;
          break;
        }
        case 'Landing':
        {
          markCoefficient.coefficient = 20;
          break;
        }
        case 'Synchronization':
        {
          markCoefficient.coefficient = 20;
          break;
        }
      }
      competitionMarkCoefficient.markCoefficients.push(markCoefficient);
    });
    defaultSettings.competitionMarkCoefficients.push(competitionMarkCoefficient);
    callback(null, defaultSettings);
  });
}

function createJudgeMean(defaultSettings, callback){
  JudgeType.find({}, function(err, judgeTypes) {
    judgeTypes.forEach(function(judgeType){
      var judgeTypeMean = {judgeType: judgeType._id};
      switch (judgeType.name){
        case 'FAI':
        {
          judgeTypeMean.mean = 80;
          break;
        }
        case 'VIP':
        {
          judgeTypeMean.mean = 20;
          break;
        }
      }
      defaultSettings.judgesMean.push(judgeTypeMean);
    });
    callback(null, defaultSettings);
  });
}

function addWithoutPenaltyManoeuvre(manoeuvreName, callback){
  Manoeuvre.findOne({name: manoeuvreName}, function(err, manoeuvre){
    if (err){
      callback(err, null);
    }
    callback(null, manoeuvre);
  });
}
function createWithoutPenaltyManoeuvres(defaultSettings, callback){
  //var manoeuvresNames = ['DYN. FULL STALL', 'FULL STALL', 'PITCH PENDULUM', 'TAIL SLIDE', 'WING OVER', 'NO MANEUVER'];
  var manoeuvresStep = require('step');
  manoeuvresStep(
    function () {
      var group = this.group();
      _.each(withoutPenaltyManoeuvres, function(manoeuvreName){
        addWithoutPenaltyManoeuvre(manoeuvreName, group());
      });
    },
    function (err , manoeuvres) {
      if (err) throw err;
      manoeuvres.forEach(function(manoeuvre){
        defaultSettings.withoutPenaltyManoeuvres.push(manoeuvre._id);
      });
      callback(null, defaultSettings);
    }
  );
}
function createDefaultSettings(callback){
  Setting.create(
    {
      name: 'Defaults',
      warningPoints: 0.5,
      warningIncremental: true,
      warningsToDSQ: 3,
      penalty: 13,
      bonusManoeuvres: 3,
      meanManoeuvres: 3,
      judgesMean: [],
//    competitionMarkCoefficients: [],
      withoutPenaltyManoeuvres: []
    }, function(err, defaultSettings){
      callback(null, defaultSettings);
  });
}
function createSettings(callback){
  Setting.find({}).remove(function(){
    var SettingsStep = require('step');
    SettingsStep(
      function(){
        createDefaultSettings(this);
      },
      function(err, defaultSettings){
        createWithoutPenaltyManoeuvres(defaultSettings, this);
      },
      function(err, defaultSettings){
        createJudgeMean(defaultSettings, this);
      },
      function(err, defaultSettings){
        createSoloMarkCoefficients(defaultSettings, this);
      },
      function(err, defaultSettings){
        createSyncMarkCoefficients(defaultSettings, this);
      },
      function (err, defaultSettings){
        defaultSettings.save();
        console.log('finished populating settings');
        callback(null, true);
      }
    );
  });
}

Setting.count({}, function( err, count){
  if(err){
    //TODO: abort the sever
    return;
  }
  if (count === 0){
    //FIRST TIME
    console.log("Welcome to Acropyx. Initializing the system... ");
    var Step = require('step');
    Step(
      function (){
        createUsers(this)
      },
      function(err, result){
        if(err){
          throw err;
        }
        importSponsors(this);
      },
      function(err, result){
        if(err){
          throw err;
        }
        importCountries(this);
      },
      function(err, result){
        if(err){
          throw err;
        }
        importPilots(this);
      },
      function(err, result){
        if(err){
          throw err;
        }
        importTeams(this);
      },
      function (err, result){
        if(err){
          throw err;
        }
        createJudgeTypes(this);
      },
      function (err, result){
        if(err){
          throw err;
        }
        importJudges(this)
      },
      function (err, result){
        if(err){
          throw err;
        }
        createRunTypes(this);
      },
      function (err, result){
        if(err){
          throw err;
        }
        createManoeuvres(this);
      },
      function (err, result){
        if(err){
          throw err;
        }
        createMarkTypes(this);
      },
      function(err, result){
        if (err){
          throw err;
        }
        createCompetitionTypes(this);
      },
      function(err, result){
        if (err){
          throw err;
        }
        createReports(this);
      },
      function(err, result){
        if (err){
          throw err;
        }
        createSettings(this);
      },
      function(err, result){
        if(err){
          throw err;
        }
        console.log("System initialized!!");
        //TODO: Remove
        //createFakeData(function(){
        //
        //});
      }
    );
  }
});


