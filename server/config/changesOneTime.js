/**
 * Changes DB with fix data (ex: csv files ...)
 */

'use strict';
var _ = require('lodash');

//Manoeuvres 2018
var Manoeuvre = require('../api/manoeuvre/manoeuvre.model');
function createManoeuvres(callback){
  Manoeuvre.remove({}, function( err){
    var fs = require('fs');
    var csv = require('csv');

    var parser = csv.parse({delimiter: ','}, function(err, data){
      _.each(data, function(mano){
        Manoeuvre.create(
          {
            name: mano[0],
            coefficient: mano[1],
            hasSides: (mano[2] === "1")?true:false,
            onlySynchro: (mano[3]=== "1")?true:false,
            twistedBonus: mano[4],
            reversedBonus: mano[5],
            flippedBonus: mano[6]
          }
        );
      });
      callback(null, true);
      console.log('');
    });
    fs.createReadStream(__dirname+'/csv/manoeuvres.csv').pipe(parser);
  });
}
Manoeuvre.count({}, function(err, count){
  if (count === 0){
    createManoeuvres(function(err, res){
      if(err){
        console.log('Error creating manoeuvres: ' + err);
        return;
      }
      console.log('Manoeuvres created successfully');
    });
  }
});


var Pilot = require('../api/pilot/pilot.model');

function getFaiService(callback){
  var soap = require('soap');
  var url = 'http://civlrankings.fai.org/FL.asmx?WSDL';
  soap.createClient(url, function(err, soapClient) {
    if (err || !soapClient) {
      console.log('Unable to connect to the FAI service, abort solo ranking update');
      return callback(err || 'FAI service unavailable');
    }
    return callback(null, soapClient);
  });
}
function getPilotSoloRank(faiService, civlId, name, callback){
    var args = {
      CIVLID: civlId,
      ladder_id: 6
    };
    faiService.GetPilotAndLastRankingInLadder(args, function (err, result) {
      if (err) {
        console.log('error getting FAI solo rank ' + name + '(' +  civlId + ')');
        return callback(err);
      }
      var rank = 1000;
      if (result.GetPilotAndLastRankingInLadderResult &&
          result.GetPilotAndLastRankingInLadderResult.Rankings) {
        var ranking = result.GetPilotAndLastRankingInLadderResult.Rankings.Ranking;
        if (ranking && ranking.length > 0) {
          rank = ranking[0].Rank;
        }else {
          // console.log(name+'(' + civlId + ')' + ': No Solo rankings');
        }
      } else {
        // console.log(name+'(' + civlId + ')' + ': No solo info');
      }
      return callback(null, rank);
    });
}
function getPilotSynchroRank(faiService, civlId, name, callback){
    var args = {
      CIVLID: civlId,
      ladder_id: 10
    };
    // console.log('updating pilot ' + name + ' sync ranking...');
    faiService.GetPilotAndLastRankingInLadder(args, function (err, result) {
      if (err) {
        console.log('error getting FAI synchro rank ' + name + '(' +  civlId + ')');
        return callback(err);
      }
      var rank = 1000;
      if (result.GetPilotAndLastRankingInLadderResult &&
        result.GetPilotAndLastRankingInLadderResult.Rankings) {
        var ranking = result.GetPilotAndLastRankingInLadderResult.Rankings.Ranking;
        if (ranking && ranking.length > 0) {
          rank = ranking[0].Rank;
        } else {
          // console.log(name+'(' + civlId + ')' + ': No synchro rankings');
        }
      } else {
        // console.log(name+'(' + civlId + ')' + ':  No Synchro info');
      }
      return callback(null, rank);
    });
}

function updatePilotRankings(pilot){
  if(pilot.civlId){
    console.log(pilot.name + '(' + pilot.civlId + '): ' + pilot.civlRank + ' ' + pilot.civlSyncRank);
    var step = require('step');
    step(
      function(){
        getFaiService(this);
      },
      function(err, faiService){
        if ( err ){
          throw err;
        }
        getPilotSoloRank(faiService, pilot.civlId, pilot.name, this.parallel());
        getPilotSynchroRank(faiService, pilot.civlId, pilot.name, this.parallel());
      },
      function(err, soloRank, synchroRank){
        if ( err ) {
         // console.log(err);
          return; //??
        }
        var updatePilot = false;
        if (soloRank && pilot.civlRank !== soloRank){
          updatePilot = true;
          pilot.civlRank = soloRank;
        }
        if (synchroRank && pilot.civlSyncRank !== synchroRank){
          updatePilot = true;
          pilot.civlSyncRank = synchroRank;
        }
        if ( updatePilot ){
          pilot.save(function(err){
            if ( err ){
              console.log('error saving pilot (updating ranking): ' + err);
            }
            console.log('Pilot ' + pilot.name + ' rankings updated!!');
          });
        }
      }
    );
  }
}

Pilot.find({},{picture:0}).exec(function (err, pilots) {
  console.log('Updating all rankings from FAI service...');
  if (err) {
    return 'unable to update pilots ranking';
  }
  pilots.forEach(function (pilot) {
    updatePilotRankings(pilot);
    // console.log(pilot.name + '(' + pilot.civlId + '): ' + pilot.civlRank + ' - ' + pilot.civlSyncRank);
  })
});
