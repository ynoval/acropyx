'use strict';

// Development specific configuration
// ==================================
module.exports = {
  // MongoDB connection options
  mongo: {
    uri: 'mongodb://localhost/acropyx2015' //-dev
  },
  displayDomain: 'localhost',//'acropyx.com',
  displayPort: '3000',
  seedDB: true
};
